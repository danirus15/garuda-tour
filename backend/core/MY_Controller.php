<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {

	function __construct() 
	{
		parent::__construct();

		$this->validate_access();	
		
	}

	/**
	 * validate login and access
	 * @return void
	 */
	private function validate_access()
	{

		$white_list_controller = array('login', 'logout');

		$controller = $this->uri->segment(1);
		if (!in_array($controller, $white_list_controller)) {

			if ($this->acl->logged_in()) {

				if (!is_null($controller)) {
					$method = (is_null($this->uri->segment(2))) ? 'index' : $this->uri->segment(2);

					if ($method == 'index_data') {
						$method = 'index';
					}
	
				} 	
			} else {
				
				redirect('login');
			}

		}

	}

}