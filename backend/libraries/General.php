<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class General
{
	public function __construct()
	{
		$this->CI =& get_instance();
		
	}

	/**
	 * create left menu
	 * @return string
	 */
	public function left_menu()
	{
		
		$this->CI->load->driver('cache');

		$menus = $this->CI->config->item('menus');
		$output = $this->set_menu_item($menus);
		
		return $output;
	}

	/**
	 * set left menu item
	 * @param object  $menus
	 * @return string 
	 */
	private function set_menu_item($menus)
	{
		$output = '';

		$active_menu = $this->CI->uri->segment(1,'');
		foreach ($menus as $menu) {
			$output .= '<li class="{{active}}">';
            $output .= '<a href="{{controller}}" class="auto">{{arrow}}<i class="i i-circle-sm text-info-dk"></i> <span>'.$menu['text'].'</span></a>';
            $arrow = '';
            $active = '';
            $controller = '';
            
            if (empty($menu['parent']) && $menu['controller'] == $active_menu) {

        		$active = 'active';
        	}

        	$controller = site_url($menu['controller']);

        	$output = str_replace(array(
        		'{{arrow}}', 
        		'{{active}}',
        		'{{controller}}'
        	), array(
        		$arrow, 
        		$active,
        		$controller,
        	), $output);

            $output .= '</li>';
		}
		return $output;
	}

	/**
	 * [json_encode description]
	 * @param  array $data
	 * @return void
	 */
	public function json_encode($data)
	{
		$this->CI->output->set_header("Pragma: no-cache");
        $this->CI->output->set_header("Cache-Control: no-store, no-cache");
        $this->CI->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	/**
	 * check folder upload
	 * @param  string $upload_type
	 * @return string full path upload folder
	 */
	public function check_upload_folder($folder="")
	{
		$upload_path = getenv("UPLOAD_DIR");

		if (!empty($upload_type)) {
			$upload_path .= "/".$upload_type;
			if (!file_exists($upload_path)) {
				mkdir($upload_path);
			}
		}
		
		$year = date('Y');
		$upload_path .= '/'.$year;
		if (!file_exists($upload_path)) {
			mkdir($upload_path);
		}

		$month = date('m');
		$upload_path .= '/'.$month;
		if (!file_exists($upload_path)) {
			mkdir($upload_path);
		}

		$day = date('d');
		$upload_path .= '/'.$day;
		if (!file_exists($upload_path)) {
			mkdir($upload_path);
		}

		return $upload_path;
	}

	/**
	 * upload file
	 * @param  string  $filename
	 * @param  integer $width
	 * @param  integer $height
	 * @return object
	 */
	public function upload_file($filename='userfile', $width=0, $height=0, $folder="")
    {
        
        $upload_path = $this->check_upload_folder($folder);

        $config['upload_path']      = $upload_path;
        $config['allowed_types']    = getenv("UPLOAD_TYPE");
        $config['encrypt_name']		= TRUE;

        $this->CI->load->library('upload', $config);

        if (!$this->CI->upload->do_upload($filename))
        {
        	$output = array(
        		'status' => 0,
        		'message' => $this->CI->upload->display_errors(),
        	);
            
        } else {
        	$data = $this->CI->upload->data();

        	if ($width > 0 && $height > 0) {
        		$this->resize_image($data['full_path'], $width, $height);	
        	}
        	

            $output = array(
            	'status' => 1,
            	'data' => $data
            );
        }

        return $output;
    }

    /**
     * resize image
     * @param  string $full_path
     * @param  int $width 
     * @param  int $height
     * @return object
     */
    public function resize_image($full_path, $width, $height)
    {
    	$config['image_library'] 	= 'gd2';
		$config['source_image'] 	= $full_path;
		$config['maintain_ratio'] 	= TRUE;
		$config['width']         	= $width;
		$config['height']       	= $height;

		$this->CI->load->library('image_lib', $config);

		if (!$this->CI->image_lib->resize()) {
			$output = array(
				'status' => 0,
				'message' => $this->CI->image_lib->display_errors()
			);
		} else {
			$output = array(
				'status' => 1
			);
		}

		return $output;
    }

    /**
	* option status
	* @param int $seleted status id
	* @return string
	*/
	public function option_status($selected = 1) {
		$output = "";
		$status = array('Inactive', 'Active');

		foreach ($status as $key => $value) {
			$output .= ($key == $selected) ? "<option value='".$key."' selected>".$value."</option>" : "<option value='".$key."'>".$value."</option>";
		}

		return $output;
	}
}