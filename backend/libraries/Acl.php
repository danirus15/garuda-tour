<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Acl
{
	public function __construct()
	{
		$this->CI =& get_instance();

		if( ! $this->logged_in() && get_cookie('identity') && get_cookie('remember_code') )
		{
			$this->login_remembered();
		}

	}

	/**
	 * login
	 * @param  string  $identity 
	 * @param  string  $password 
	 * @param  boolean $remember 
	 * @param  array   $session_data
	 * @return boolean              
	 */
	public function login($identity, $password, $remember = FALSE, $session_data = array())
	{
		$user = $this->CI->acl_model->get_user($identity);

		if (!$user OR !password_verify($password, $user->password)) {
			
			return FALSE;

		}

		

		$session = array(
			'user_id' => $user->id,
			'logged_in' => TRUE,
			'name' => $user->name,
			'user_'.getenv('IDENTITY_FIELD') => $user->{getenv('IDENTITY_FIELD')}
		);

		foreach ($session_data as $key) {
			
			$session['user_'.$key] = ($user->$key) ? $user->$key : NULL;

		}

		$this->CI->session->set_userdata($session);

		if ($remember) {

			$remember_code = password_hash(uniqid(), PASSWORD_BCRYPT);
			$this->CI->acl_model->set_user_remember_code($user->user_id, array('remember_code' => $remember_code));

			$expire = (60*60*24*365*2);
			
			set_cookie(array(
			    'name'   => 'identity',
			    'value'  => $identity,
			    'expire' => $expire
			));
			
			set_cookie(array(
			    'name'   => 'remember_code',
			    'value'  => $remember_code,
			    'expire' => $expire
			));

		}

		return TRUE;

	}

	/**
	 * [login_remembered description]
	 * @return void
	 */
	private function login_remembered()
	{
		$identity = get_cookie('identity');
		$code 	  = get_cookie('remember_code');

		$user = $this->CI->acl_model->get_user($identity);
		
		if( $user && $user->remember_code === $code )
		{
			$session = array(
				'user_id'	=> $user->id
				,'logged_in'=> TRUE
				,'user_'.getenv('IDENTITY_FIELD') => $user->{getenv('IDENTITY_FIELD')}
			);

			$this->CI->session->set_userdata($session);

		}
	}

	/**
	 * [logged_in description]
	 * @return boolean
	 */
	public function logged_in()
	{
		return (bool) $this->CI->session->userdata('logged_in');
	}

	/**
	 * [logout description]
	 * @return boolean
	 */
	public function logout()
	{
		$this->CI->session->sess_destroy();
		delete_cookie('identity');
		delete_cookie('remember_code');
		
		return( TRUE === $this->CI->session->userdata('logged_in') ) ? FALSE : TRUE;
	}

}