<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PdfGenerator
{
  public function generate($html,$filename)
  {
    
    $mpdf = new mPDF('utf-8', 'A4');
    // $mpdf->showImageErrors = true;
	$mpdf->WriteHTML($html);
	$mpdf->Output();

  }
}