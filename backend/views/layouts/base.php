<!DOCTYPE html>
<html lang="en" class="app">

<head>  
  <meta charset="utf-8" />
  <title>Ayo Liburan - Garuda Indonesia</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
  <link rel="stylesheet" href="<?php echo assets_url('css/bootstrap.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo assets_url('css/animate.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo assets_url('css/font-awesome.min.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo assets_url('css/icon.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo assets_url('js/datatables/datatables.css')?>" type="text/css"/>
  <link rel="stylesheet" href="<?php echo assets_url('css/font.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo assets_url('css/app.css')?>" type="text/css" />  
  <link rel="stylesheet" href="<?php echo assets_url('css/custom.css')?>" type="text/css" />  
  <link rel="stylesheet" href="<?php echo assets_url('js/calendar/bootstrap_calendar.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo assets_url('js/datepicker/datepicker.css')?>" type="text/css" />
  <!--[if lt IE 9]>
    <script src="<?php echo assets_url('js/ie/html5shiv.js')?>"></script>
    <script src="<?php echo assets_url('js/ie/respond.min.js')?>"></script>
    <script src="<?php echo assets_url('js/ie/excanvas.js')?>"></script>
  <![endif]-->
  <script src="<?php echo assets_url('js/jquery.min.js')?>"></script>
</head>
<body class="">
  <section class="vbox">
    <?php $this->load->view('partials/header')?>
    <section>
      <section class="hbox stretch">
        <?php $this->load->view('partials/left_menu')?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <?php
                if (isset($content) && !empty($content)) {
                  $this->load->view($content);
                }
                ?>
              </section>
            </section>
          </section>
        </section>
      </section>
    </section>
  </section>
<!-- Bootstrap -->
<script src="<?php echo assets_url('js/bootstrap.js')?>"></script>
<!-- App -->
<script src="<?php echo assets_url('js/app.js')?>"></script>  
<script src="<?php echo assets_url('js/slimscroll/jquery.slimscroll.min.js')?>"></script>
<script src="<?php echo assets_url('js/charts/easypiechart/jquery.easy-pie-chart.js')?>"></script>
<script src="<?php echo assets_url('js/charts/sparkline/jquery.sparkline.min.js')?>"></script>
<script src="<?php echo assets_url('js/charts/flot/jquery.flot.min.js')?>"></script>
<script src="<?php echo assets_url('js/charts/flot/jquery.flot.tooltip.min.js')?>"></script>
<script src="<?php echo assets_url('js/charts/flot/jquery.flot.spline.js')?>"></script>
<script src="<?php echo assets_url('js/charts/flot/jquery.flot.pie.min.js')?>"></script>
<script src="<?php echo assets_url('js/charts/flot/jquery.flot.resize.js')?>"></script>
<script src="<?php echo assets_url('js/charts/flot/jquery.flot.grow.js')?>"></script>

<script src="<?php echo assets_url('js/datatables/jquery.dataTables.min.js')?>"></script> 
<script src="<?php echo assets_url('js/datatables/jquery.csv-0.71.min.js')?>"></script>

<script src="<?php echo assets_url('js/calendar/bootstrap_calendar.js')?>"></script>
<script src="<?php echo assets_url('js/datepicker/bootstrap-datepicker.js')?>"></script>
<script src="<?php echo assets_url('js/sortable/jquery.sortable.js')?>"></script>
<script src="<?php echo assets_url('js/app.plugin.js')?>"></script>
<script src="<?php echo assets_url('js/alertify.js')?>"></script>
<script type="text/javascript" language="javascript" src="<?php echo assets_url('js/tinymce/tinymce.min.js')?>"></script>
<script src="<?php echo assets_url('js/controller.js')?>"></script>

<!-- Bootbox -->
<script src="<?php echo assets_url('js/bootbox/bootbox.min.js')?>"></script>
<script src="<?php echo assets_url('js/custom.js')?>"></script>

</body>
</html>