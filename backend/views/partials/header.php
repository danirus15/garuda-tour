<header class="bg-white header header-md navbar navbar-fixed-top-xs box-shadow">
  <div class="navbar-header aside-md dk">
    <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav">
      <i class="fa fa-bars"></i>
    </a>
    <a href="home.php" class="navbar-brand">
      <img src="<?php echo assets_url('images/logo2.png')?>" class="m-r-sm" alt="garuda indonesia" class="fl">
    </a>
    <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user">
      <i class="fa fa-cog"></i>
    </a>
  </div>
  <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <?php echo $this->session->userdata('name')?> <b class="caret"></b>
      </a>
      <ul class="dropdown-menu animated fadeInRight">            
        <li>
          <a href="<?php echo site_url('change_password')?>">Change Password</a>
        </li>
        
        <li class="divider"></li>
        <li>
          <a href="<?php echo site_url('logout')?>" >Logout</a>
        </li>
      </ul>
    </li>
  </ul>      
</header>