<?php if ($this->session->flashdata('success')) { ?>
<div class="alert alert-success alert-dismissable">
  <h4>  <i class="icon fa fa-check"></i> Success!</h4>
  <?php echo $this->session->flashdata('success')?>
</div>
<?php } ?>
<?php if ($this->session->flashdata('error')) { ?>
<div class="alert alert-danger alert-dismissable">
  <h4><i class="icon fa fa-ban"></i> Error!</h4>
  <?php echo $this->session->flashdata('error')?>
</div>
<?php } ?>