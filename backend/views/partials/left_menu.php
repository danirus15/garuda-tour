<!-- .aside -->
<aside class="bg-black aside-md hidden-print" id="nav">          
  <section class="vbox">
    <section class="w-f scrollable">
      <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
                 


        <!-- nav -->                 
        <nav class="nav-primary hidden-xs">
          <ul class="nav">
            <?php echo $this->general->left_menu()?>
          </ul>
          
        </nav>
        <!-- / nav -->
      </div>
    </section>
    
    
  </section>
</aside>