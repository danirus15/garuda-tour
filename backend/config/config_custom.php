<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['menus'] = array(
	array(
		'icon'			=> 'i-statistics',
		'text'			=> 'Dashboard',
		'controller'	=> '',
		'parent' 		=> '',
		'child' 		=> array(),
	),
	array(
		'icon'			=> 'i-statistics',
		'text'			=> 'Cover Images',
		'controller'	=> 'coverimages',
		'parent' 		=> '',
		'child' => array(
			
		)
	),
	array(
		'icon'			=> 'i-statistics',
		'text'			=> 'Static Wording',
		'controller'	=> 'staticwording',
		'parent' 		=> '',
		'child' => array(
			
		)
	),
	array(
		'icon'			=> 'i-statistics',
		'text'			=> 'Paket Liburan',
		'controller'	=> 'paketliburan',
		'parent' 		=> '',
		'child' => array(
			
		)
	),
	array(
		'icon'			=> 'i-statistics',
		'text'			=> 'Daftar Pemesanan',
		'controller'	=> 'daftarpemesanan',
		'parent' 		=> '',
		'child' => array(
			
		)
	),
	array(
		'icon'			=> 'i-statistics',
		'text'			=> 'User',
		'controller'	=> 'user',
		'parent' 		=> '',
		'child' => array(
			
		)
	),
	array(
		'icon'			=> 'i-statistics',
		'text'			=> 'Admin Member',
		'controller'	=> 'adminmember',
		'parent' 		=> '',
		'child' => array(
			
		)
	),
	array(
		'icon'			=> 'i-statistics',
		'text'			=> 'Logout',
		'controller'	=> 'logout',
		'parent' 		=> '',
		'child' => array(
			
		)
	),
);
