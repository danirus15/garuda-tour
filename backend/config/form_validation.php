<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * config error delimiter
 */
$config['error_prefix'] = '<label style="has-error">';
$config['error_suffix'] = '</label>';