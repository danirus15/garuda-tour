<?php
function assets_url($path, $witch_assets='assets/backend')
{
	$asset = ($witch_assets == 'bower')?'bower_components':$witch_assets;
	return base_url($asset.'/'.$path);
} 

function date_to_path($datetime)
{
	$date = explode(' ', $datetime);
	$date_split = explode('-', $date[0]);
	return implode('/', $date_split);
}

function image_url($path)
{
	$output = "";

	if (!empty($path)) {

		$output = getenv('BASE_IMAGE_URL').$path;
	}

	return $output;
}

function price_format($number) 
{
	return number_format($number , 0, ',', '.');
}