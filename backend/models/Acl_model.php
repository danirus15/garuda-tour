<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Acl_model extends MY_Model
{
	
    public function __construct()
	{   
		parent::__construct();

	}

    /**
     * get user data
     * @param  string $identity
     * @return object
     */
	public function get_user($identity)
    {
        $query = $this->db->select('admin_member.*')
            ->from('admin_member')
            ->where(getenv('IDENTITY_FIELD'), $identity)
            ->get();

        return $query->row();
    }

    /**
     * update user remember code
     * @param int $user_id 
     * @param array  $data 
     * @return bool
     */
    public function set_user_remember_code($user_id, $data = array())
    {
        $output = FALSE;

        if (!empty($data)) {
            $output = $this->db->update('admin_member', array('remember_code' => $remember_code), array('user_id' => $user_id));    
        }
        
        return $output;
    }
}
/* End of file '/Acl_model.php' */
/* Location: ./application/models/Aclmodel.php */