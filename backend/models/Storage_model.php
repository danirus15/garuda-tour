<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Storage_model extends MY_Model implements DatatableModel
{
    public $table = 'storages';
    public $primary_key = 'storage_id';
    public $fillable = array();
    public $protected = array('storage_id');
    public $timestamps = TRUE;
    public $before_create = array();
    public $before_update = array();
	
    public function __construct()
	{   
		parent::__construct();
	}

    public function appendToSelectStr() {
        return array(
            'action_id' => $this->primary_key
        );
    }

    public function fromTableStr() {
        return $this->table;
    }

    public function joinArray(){
        return NULL;
    }

    public function whereClauseArray(){
        return NULL;
    }
	
}
/* End of file '/Storage_model.php' */
/* Location: ./application/models/Storage_model.php */