<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Coverimages_model extends MY_Model implements DatatableModel
{
    public $table = 'cover_images';
    public $primary_key = 'id';
    public $fillable = array();
    public $protected = array('id');
    public $timestamps = TRUE;
    public $before_create = array('upload_photo', 'create_by');
    public $before_update = array('upload_photo', 'modify_by');
	
    public function __construct()
	{   

		parent::__construct();
        $this->load->library('general');

	}

    /**
     * log user create data
     * @param  array $data
     * @return array     
     */
    public function create_by($data)
    {
        
        $data['created_by'] = $this->session->userdata('user_id');
        
        return $data;
    }

    /**
     * log user modify data
     * @param  array $data
     * @return array     
     */
    public function modify_by($data)
    {
        
        $data['updated_by'] = $this->session->userdata('user_id');
        
        return $data;
    }

    /**
     * upload photo
     * @param  array $data
     * @return array
     */
    public function upload_photo($data)
    {

        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $data_upload = $this->general->upload_file('userfile', 1400, 800);

            if ($data_upload['status']) {
                $full_path = explode('uploads', $data_upload['data']['file_path'])[1];
                $file_name = $data_upload['data']['file_name'];
                $data['photo'] = $full_path.$file_name;
            }
        }

        return $data;
    }

    /**
     * rules validate insert/update admin_members
     * @var array
     */
    public $rules = array(
        'insert' => array(
            'is_logo_exist' => array(
                'field' => 'is_logo_exist',
                'label' => 'Logo',
            ),
            'link' => array(
                'field' => 'link',
                'label' => 'Link',
            ),
            'status' => array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required'
            ),
        ),
        'update' => array(
            'is_logo_exist' => array(
                'field' => 'is_logo_exist',
                'label' => 'Logo',
            ),
            'link' => array(
                'field' => 'link',
                'label' => 'Link',
            ),
            'status' => array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required'
            ),
        )  
    );

    public function appendToSelectStr() {
        return array(
            'action_id' => $this->primary_key
        );
    }

    public function fromTableStr() {
        return $this->table;
    }

    public function joinArray(){
        return NULL;
    }

    public function whereClauseArray(){
        return array('1' => '1');
    }
	
}
/* End of file '/admin_members_model.php' */
/* Location: ./application/modules/admin_members/models/coverimages_model.php */