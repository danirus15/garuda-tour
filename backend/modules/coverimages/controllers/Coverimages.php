<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coverimages extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('datatable', array('model' => 'coverimages_model', 'rowIdCol' => 'id'));
		$this->load->model(array('coverimages_model'));
	}

	/**
	 * index list
	 * @return void
	 */
	public function index()
	{
		$data = array(
			'content' => 'index',
		);
		$this->load->view('layouts/base', $data);
	}

	/**
	 * index list data
	 * @return json
	 */
	public function index_data()
	{
		$this->datatable->setPreResultCallback(
            function(&$json) {
            	$rows =& $json['data'];
                foreach($rows as &$r) {
                    $r['is_logo_exist'] = ($r['is_logo_exist']) ? 'Ada' : 'Tidak';
                    $r['status'] = ($r['status']) ? 'Active' : 'Inactive';
                    $r['photo'] = ($r['photo']) ? '<img src="'.image_url($r['photo']).'" width="150"/>' : '';  
                }
            }
        );

		$json = $this->datatable->datatableJson();
		$json['csrfName'] = $this->security->get_csrf_token_name();
		$json['csrfHash'] = $this->security->get_csrf_hash();
		$this->general->json_encode($json);
	}

	/**
	 * add new coverimages
	 */
	public function add()
	{
		if ($this->input->post()) {
			$id = $this->coverimages_model->from_form()->insert();
			if ($id) {
				$this->session->set_flashdata('success', 'Data success save');
				redirect('coverimages');
			}
		}

		$data = array(
			'content' => 'add',
			'option_status' => $this->general->option_status()
		);
		$this->load->view('layouts/base', $data);
	}

	/**
	 * edit coverimages
	 * @param  int $id coverimages id
	 * @return void
	 */
	public function edit($id)
	{
		
		if (!is_numeric($id)) {
			$this->session->set_flashdata('error', 'ID data must numeric');
			redirect('coverimages');
		}

		$record = $this->coverimages_model->get($id);
		
		if (!$record) {
			$this->session->set_flashdata('error', 'Data not found');
			redirect('coverimages');
		}

		if ($this->input->post()) {

			// delete previous file
			if ($_FILES['userfile'] && !empty($_FILES['userfile']['name'])) {

				if ($record->photo) {
					@unlink(getenv('UPLOAD_DIR').$record->photo);	
				}
			}

			//update data
			$data_update = $this->input->post();
			if (!$this->input->post('is_logo_exist')) {
				$data_update['is_logo_exist'] = '0';
			}
			$result = $this->coverimages_model->update($data_update, $id);
			
			if ($result) {

				$this->session->set_flashdata('success', 'Update data success');				
				redirect('coverimages');
			}
		}

		$data = array(
			'record' => $record,
			'option_status' => $this->general->option_status($record->status),
			'content' => 'edit',
		);

		$this->load->view('layouts/base', $data);
	}

	/**
	 * delete coverimages
	 * @param  int $id coverimages id
	 * @return void
	 */
	public function delete($id)
	{
		if (!is_numeric($id)) {
			$this->session->set_flashdata('error', 'ID data must numeric');
		}

		$record = $this->coverimages_model->get($id);
		if (!$record) {
			$this->session->set_flashdata('error', 'Data not found');
		}

		if ($this->coverimages_model->delete($id)) {
			if ($record->photo) {
				@unlink(getenv('UPLOAD_DIR').$record->photo);	
			}
			$this->session->set_flashdata('success', 'Delete data success');
		} else {
			$this->session->set_flashdata('error', 'Delete data failed');
		}

		redirect('coverimages');

	}

}
