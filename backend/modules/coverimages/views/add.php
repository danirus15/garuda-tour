<section class="scrollable padder">              
  <section class="row m-b-md">
    <div class="col-sm-6">
      <h3 class="m-b-xs text-black">Cover Image Add</h3>
    </div>
  </section>
  <!-- s:content --> 
  <section class="panel panel-default">
    <header class="panel-heading font-bold">
      
    </header>
    <div class="panel-body">
      <?php echo form_open_multipart('',array('class'=>'form-horizontal'))?>
        <div class="form-group <?php echo (form_error('name')) ? 'has-error' : '' ?>">
          <label class="col-sm-2 control-label">Foto</label>
          <div class="col-sm-10">
            <label class="foto-form">
              <div id="imagePreview"></div>
              <?php echo form_upload('userfile','', array('class'=>'img', 'id' => 'uploadFile'))?>
              <?php echo form_error('userfile');?>
              <span>Upload Foto</span>
            </label>
            *Ukuran 1400x800px
          </div>
        </div>
        <div class="form-group <?php echo (form_error('name')) ? 'has-error' : '' ?>">
          <label class="col-sm-2 control-label">Link</label>
          <div class="col-md-10">
            <?php echo form_input('link', set_value('link'), array('class'=>'input-sm form-control'))?>
            <?php echo form_error('link');?>
          </div>
        </div>
        <div class="form-group <?php echo (form_error('name')) ? 'has-error' : '' ?>">
          <label class="col-sm-2 control-label">Logo Ayo Liburan</label>
          <div class="col-md-10">
            <div class="checkbox i-checks col-sm-3">
              <label>
                <input type="checkbox" name="is_logo_exist" value="1">
                <i></i>
                Ada
              </label>
            </div>
          </div>
        </div>
        <div class="form-group <?php echo (form_error('name')) ? 'has-error' : '' ?>">
          <label class="col-sm-2 control-label">Status <a class="mandatory tip" title="" data-original-title="Wajib diisi">*</a></label>
          <div class="col-md-3">
            <select name="status" class="form-control" required="">
              <?php echo $option_status?>
            </select>
          </div>
        </div>
        
        <div class="form-group">
          <div class="col-sm-4 col-sm-offset-2">
            <a href="<?php echo site_url('coverimages')?>" class="btn btn-default">Batal</a>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </section>
  <!-- e:content -->
  <div class="clearfix"></div>
  

</section>