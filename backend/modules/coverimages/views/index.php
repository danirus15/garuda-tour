<section class="scrollable padder">              
  <section class="row m-b-md">
    <div class="col-sm-6">
      <h3 class="m-b-xs text-black">Cover Images</h3>
    </div>
    <a href="<?php echo site_url('coverimages/add')?>" class="btn btn-success fr m15">Tambah</a>
  </section>
  
  <div class="clearfix"></div>
  <?php echo $this->load->view('partials/flash_messages')?>
  <table class="table table-striped m-b-none" id="my-datatable">
    <thead>
      <tr>
        <th width="30%">Image</th>
        <th width="30%">Link</th>
        <th width="20%">Logo Ayo Liburan</th>
        <th width="10%">Status</th>
        <th width="10%">Action</th>
      </tr>
    </thead>
  </table>
</section>

<script type="text/javascript">
var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
    csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
$(function () {
  var table = $('#my-datatable').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url":"<?php echo site_url('coverimages/index_data')?>",
        "type":"POST",
        "data": function(d){
          eval('d.'+csrfName+'=\''+csrfHash+'\'');
        },
        "dataSrc": function(response) {
          csrfName = response.csrfName;
          csrfHash = response.csrfHash;
          return response.data;
        }
      },
      "columns": [
        {
          "data":"photo",
          "orderable" : false,
        },
        {"data":"link"},
        {"data":"is_logo_exist"},
        {"data":"status"},
        {   
            "data" : "$.action_id",
            "searchable" : false,
            "orderable" : false,
            "sClass" : "action",
            mRender: function(data, type, row) {
            return '<a href="<?php echo site_url('coverimages/edit')?>/' + data + '" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="<?php echo assets_url('images/ico_edit.png')?>" alt=""></a><a href="javascript:void(0);" onclick="confirm_delete(\'<?php echo site_url('coverimages/delete')?>/'  + data +  '\');" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="<?php echo assets_url('images/ico_del.png')?>" alt=""></a>';
        }

      }],
      "order":[
        [0, "asc"]
      ],
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
  });
});
</script>