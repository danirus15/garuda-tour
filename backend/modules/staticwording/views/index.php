<section class="scrollable padder">              
  <section class="row m-b-md">
    <div class="col-sm-6">
      <h3 class="m-b-xs text-black">Static Wording</h3>
    </div>
  </section>
  <!-- s:content --> 
  <section class="panel panel-default">
    <header class="panel-heading font-bold">
      
    </header>
    <div class="panel-body">
      <?php echo $this->load->view('partials/flash_messages')?>
      <?php echo form_open('staticwording/index/wording', array('class'=>'form-horizontal'))?>
        <section class="panel panel-default">
          <header class="panel-heading bg-light">
            <ul class="nav nav-tabs nav-justified">
              <li class="active"><a href="#indonesia" data-toggle="tab">Indonesia</a></li>
              <li><a href="#english" data-toggle="tab">English</a></li>
            </ul>
          </header>
          <div class="panel-body">
            <div class="tab-content">
              <!-- s:english -->
              <div class="tab-pane active" id="indonesia">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Judul Intro</label>
                  <div class="col-sm-10">
                    <?php echo form_input('title_id',$record->title_id, array('class'=>'form-control'))?>
                    <?php echo form_error('title_id');?>
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Deskripsi</label>
                  <div class="col-md-8">
                    <?php echo form_textarea('content_id',$record->content_id, array('class'=>'form-control', 'cols' => 30, 'rows' => 10))?>
                    <?php echo form_error('content_id');?>
                  </div>
                </div>
              </div>
              <!-- s:english -->
              <!-- s:arab -->
              <div class="tab-pane" id="english">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Intro Title</label>
                  <div class="col-sm-10">
                    <?php echo form_input('title_en',$record->title_en, array('class'=>'form-control'))?>
                    <?php echo form_error('title_en');?>
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Description</label>
                  <div class="col-md-8">
                    <?php echo form_textarea('content_en',$record->content_en, array('class'=>'form-control', 'cols' => 30, 'rows' => 10))?>
                    <?php echo form_error('content_en');?>
                    
                  </div>
                </div>
              </div>
              <!-- s:arab -->
            </div>
          </div>
        </section>
        <div class="form-group">
          <div class="col-sm-4 col-sm-offset-2">
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </section>
  <!-- e:content -->
  <div class="clearfix"></div>
  

</section>