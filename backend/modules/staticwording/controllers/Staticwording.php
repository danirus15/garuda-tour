<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staticwording extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('datatable', array('model' => 'staticwording_model', 'rowIdCol' => 'id'));
		$this->load->model(array('staticwording_model'));
	}

	/**
	 * edit staticwording
	 * @param  int $id staticwording id
	 * @return void
	 */
	public function index()
	{	
		
		$record = $this->staticwording_model->get(array('type' => 'wording'));
		
		if (!$record) {
			$this->session->set_flashdata('error', 'Data not found');
			redirect('staticwording');
		}

		if ($this->input->post()) {
			//update data
			$data_update = $this->input->post();
			
			$result = $this->staticwording_model->update($data_update, $id);
			
			if ($result) {

				$this->session->set_flashdata('success', 'Update data success');				
				redirect('staticwording');
			}
		}

		$data = array(
			'record' => $record,
			'content' => 'index',
		);

		$this->load->view('layouts/base', $data);
	}

}
