<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Staticwording_model extends MY_Model implements DatatableModel
{
    public $table = 'static_content';
    public $primary_key = 'id';
    public $fillable = array();
    public $protected = array('id');
    public $timestamps = TRUE;
    public $before_create = array( 'create_by');
    public $before_update = array( 'modify_by');
	
    public function __construct()
	{   

		parent::__construct();
        $this->load->library('general');

	}

    /**
     * log user create data
     * @param  array $data
     * @return array     
     */
    public function create_by($data)
    {
        
        $data['created_by'] = $this->session->userdata('user_id');
        
        return $data;
    }

    /**
     * log user modify data
     * @param  array $data
     * @return array     
     */
    public function modify_by($data)
    {
        
        $data['updated_by'] = $this->session->userdata('user_id');
        
        return $data;
    }

    /**
     * rules validate insert/update admin_members
     * @var array
     */
    public $rules = array(
        'insert' => array(
            'title_id' => array(
                'field' => 'title_id',
                'label' => 'Title Indonesia',
                'rules' => 'trim|required'
            ),
            'title_en' => array(
                'field' => 'title_en',
                'label' => 'Title English',
                'rules' => 'trim|required'
            ),
            'content_id' => array(
                'field' => 'content_id',
                'label' => 'Description Indonesia',
                'rules' => 'trim|required'
            ),
            'content_en' => array(
                'field' => 'content_en',
                'label' => 'Description English',
                'rules' => 'trim|required'
            ),
        ),
        'update' => array(
            'title_id' => array(
                'field' => 'title_id',
                'label' => 'Title Indonesia',
                'rules' => 'trim|required'
            ),
            'title_en' => array(
                'field' => 'title_en',
                'label' => 'Title English',
                'rules' => 'trim|required'
            ),
            'content_id' => array(
                'field' => 'content_id',
                'label' => 'Description Indonesia',
                'rules' => 'trim|required'
            ),
            'content_en' => array(
                'field' => 'content_en',
                'label' => 'Description English',
                'rules' => 'trim|required'
            ),
        )  
    );

    public function appendToSelectStr() {
        return array(
            'action_id' => $this->primary_key
        );
    }

    public function fromTableStr() {
        return $this->table;
    }

    public function joinArray(){
        return NULL;
    }

    public function whereClauseArray(){
        return NULL;
    }
	
}
/* End of file '/admin_members_model.php' */
/* LocStion: ./application/modules/admin_members/models/staticwording_model.php */