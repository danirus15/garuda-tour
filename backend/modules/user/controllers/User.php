<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('datatable', array('model' => 'user_model', 'rowIdCol' => 'id'));
		$this->load->model(array('user_model'));
	}

	/**
	 * index list
	 * @return void
	 */
	public function index()
	{
		$data = array(
			'content' => 'index',
		);
		$this->load->view('layouts/base', $data);
	}

	/**
	 * index list data
	 * @return json
	 */
	public function index_data()
	{
		$this->datatable->setPreResultCallback(
            function(&$json) {
            	$rows =& $json['data'];
                foreach($rows as &$r) {
                    $r['status'] = ($r['status']) ? 'Active' : 'Inactive';
                }
            }
        );

		$json = $this->datatable->datatableJson();
		$json['csrfName'] = $this->security->get_csrf_token_name();
		$json['csrfHash'] = $this->security->get_csrf_hash();
		$this->general->json_encode($json);
	}

	/**
	 * add new user
	 */
	public function add()
	{

		if ($this->input->post()) {

			$id = $this->user_model->from_form()->insert();
			if ($id) {
				$this->session->set_flashdata('success', 'Data success save');
				redirect('user');
			}
		}

		$data = array(
			'content' => 'add',
		);
		$this->load->view('layouts/base', $data);
	}

	/**
	 * edit user
	 * @param  int $id user id
	 * @return void
	 */
	public function edit($id)
	{
		
		if (!is_numeric($id)) {
			$this->session->set_flashdata('error', 'ID data must numeric');
			redirect('user');
		}

		$record = $this->user_model->get($id);
		
		if (!$record) {
			$this->session->set_flashdata('error', 'Data not found');
			redirect('user');
		}

		if ($this->input->post()) {

			//update data
			$data_update = $this->input->post();
			$result = $this->user_model->update($data_update, $id);
			
			if ($result) {

				$this->session->set_flashdata('success', 'Update data success');				
				redirect('user');
			}
		}

		$data = array(
			'record' => $record,
			'content' => 'edit',
		);

		$this->load->view('layouts/base', $data);
	}

	/**
	 * delete user
	 * @param  int $id user id
	 * @return void
	 */
	public function delete($id)
	{
		if (!is_numeric($id)) {
			$this->session->set_flashdata('error', 'ID data must numeric');
		}

		$user = $this->user_model->get($id);
		if (!$user) {
			$this->session->set_flashdata('error', 'Data not found');
		}

		if ($this->user_model->delete($id)) {
			$this->session->set_flashdata('success', 'Delete data success');
		} else {
			$this->session->set_flashdata('error', 'Delete data failed');
		}

		redirect('user');

	}

	/**
	 * [login description]
	 * @return [type] [description]
	 */
	public function login()
	{

		if ($this->input->post()) {
			
			$this->acl->login($this->input->post(getenv('IDENTITY_FIELD')), $this->input->post('password'), FALSE);
		}

		if ($this->acl->logged_in()) {
			redirect('/');
		}

		$this->load->view('login');
	}

	public function logout()
	{
		$this->acl->logout();
		redirect('login');
	}

}
