<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends MY_Model implements DatatableModel
{
    public $table = 'user';
    public $primary_key = 'id';
    public $fillable = array();
    public $protected = array('id');
    public $timestamps = TRUE;
    public $before_create = array( 'hash_password', 'create_by');
    public $before_update = array( 'hash_password', 'modify_by');
	
    public function __construct()
	{   

		parent::__construct();
        $this->load->library('general');

	}

    /**
     * log user create data
     * @param  array $data
     * @return array     
     */
    public function create_by($data)
    {
        
        $data['created_by'] = $this->session->userdata('user_id');
        
        return $data;
    }

    /**
     * log user modify data
     * @param  array $data
     * @return array     
     */
    public function modify_by($data)
    {
        
        $data['updated_by'] = $this->session->userdata('user_id');
        
        return $data;
    }

    /**
     * hash password
     * @param  array $data
     * @return array     
     */
    public function hash_password($data)
    {
        
        if (!empty($data['password'])) {

            $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);

        } else {
            
            unset($data['password']);
            
        }
        
        return $data;
    }

    /**
     * rules validate insert/update admin_members
     * @var array
     */
    public $rules = array(
        'insert' => array(
            'name' => array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required'
            ),
            'email' => array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|valid_email|required'
            ),
            'password' => array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required'
            ),
        ),
        'update' => array(
            'name' => array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required'
            ),
            'email' => array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|valid_email|required'
            ),
            'password' => array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required'
            ),
        )  
    );

    public function appendToSelectStr() {
        return array(
            'action_id' => $this->primary_key
        );
    }

    public function fromTableStr() {
        return $this->table;
    }

    public function joinArray(){
        return NULL;
    }

    public function whereClauseArray(){
        return array('1' => '1');
    }
	
}
/* End of file '/admin_members_model.php' */
/* Location: ./application/modules/admin_members/models/user_model.php */