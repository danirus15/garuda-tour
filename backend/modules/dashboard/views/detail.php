<section class="scrollable padder">              
  <section class="row m-b-md">
    <div class="col-sm-6">
      <h3 class="m-b-xs text-black">Detail Pemesanan</h3>
    </div>
  </section>

  <div class="clearfix"></div>
  <section class="panel">
    <div class="box_ box_2">
      <div class="text">
        <div class="title">
        Paket <?php echo $order->destination_name?>
          <span class="code">
            KODE PESANAN
            <strong><?php echo strtoupper($order->code)?></strong>
          </span>
        </div>
        <div class="clearfix"></div>
        <div class="info_box">
          <div class="group-input info_time">
            <div class="ico">
              <img src="<?php echo assets_url('images/ico_time.png')?>" alt="">
            </div>
            <label class="input-date">
              <span>Berangkat</span>
              <?php echo date('d-m-Y', strtotime($order->depart_date))?>
            </label>
            <label class="input-date">
              <span>Kembali</span>
              <?php echo date('d-m-Y', strtotime($order->return_date))?>
            </label>
            <div class="clearfix"></div>
          </div>
          <div class="group-input info_wis">
            <div class="ico">
              <img src="<?php echo assets_url('images/ico_dewasa.png')?>" alt="">
            </div>
            <div class="info"> <?php echo $order->qty + $order->qty_promo?> Wisatawan</div>
            <div class="clearfix"></div>
          </div>
          <div class="info_total">
            TOTAL
            <strong>Rp.<?php echo number_format((($order->qty * $order->price) + ($order->qty_promo * $order->price_promo) + $order->suplement) - (($order->qty + $order->qty_promo) * $order->cashback ))?>,-</strong>
            <!-- <span>(<?php echo $order->qty?> orang x Rp.<?php echo number_format($order->price)?> <?php if ($order->suplement) { echo ' + '.number_format($order->suplement).' suplement'; } ?>)</span> -->
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="infowisatawan">
          <h3>Info Pemesan</h3>
          <div class="iw">
            <div class="isi">
              <div class="jdl">Nama</div>
              <div class="isi2"><?php echo $order->user_name?></div>
              <div class="clearfix"></div>
              <div class="jdl">Email</div>
              <div class="isi2"><?php echo $order->user_email?></div>
              <div class="clearfix"></div>
              <div class="jdl">No. Telp</div>
              <div class="isi2"><?php echo $order->contact_phone?></div>
            </div>
            <div class="clearfix"></div>
          </div>  
          <br><br>
          <h3>Info Wisatawan</h3>
          <?php 
          $i = 1;
          foreach ($order_detail->result_array() as $row) { 
          ?>
            <div class="iw">
              <div class="no"><?php echo $i?></div>
              <div class="isi">
                <div class="jdl">Nama</div>
                <div class="isi2"><?php echo $row['title']?>. <?php echo $row['first_name']?> <?php echo $row['last_name']?></div>
                <div class="clearfix"></div>
                <div class="jdl">Tanggal Lahir</div>
                <div class="isi2"><?php echo date('d M Y', strtotime($row['birthdate']))?></div>
              </div>
              <div class="clearfix"></div>
            </div>  
          <?php 
            $i++;
          } 
          ?>
        </div>
        <br>
        <div class="clearfix"></div>
        <div class="line"></div>
        <div align="center">
          <!-- <a href="<?php echo site_url('dashboard/pdf/'.$order->id)?>" class="btn">DOWNLOAD PDF</a> -->
          <a href="#" class="btn" onclick="window.print();">PRINT</a>
        </div>
      </div>
    </div>
  </section>

</section>