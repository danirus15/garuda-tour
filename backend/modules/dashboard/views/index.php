<section class="scrollable padder">              
  <section class="row m-b-md">
    <div class="col-sm-6">
      <h3 class="m-b-xs text-black">Dashboard</h3>
      <small>Halo <?php echo ucfirst($this->session->userdata('name'))?></small>
    </div>
    
  </section>
  <div class="row">
    <div class="col-xs-12">
      <div class="panel b-a">
        <div class="row m-n">
          <div class="col-md-6 b-b b-r">
            <a href="#" class="block padder-v hover">
              <span class="i-s i-s-2x pull-left m-r-sm">
                <i class="i i-hexagon2 i-s-base text-danger hover-rotate"></i>
                <i class="i i-plus2 i-1x text-white"></i>
              </span>
              <span class="clear">
                <span class="h3 block m-t-xs text-danger"><?php echo $total_new_order?></span>
                <small class="text-muted text-u-c">Pemesanan Baru</small>
              </span>
            </a>
          </div>
          <div class="col-md-6 b-b">
            <a href="#" class="block padder-v hover">
              <span class="i-s i-s-2x pull-left m-r-sm">
                <i class="i i-hexagon2 i-s-base text-success-lt hover-rotate"></i>
                <i class="i i-users2 i-sm text-white"></i>
              </span>
              <span class="clear">
                <span class="h3 block m-t-xs text-success"><?php echo $total_member?></span>
                <small class="text-muted text-u-c">User Terdaftar</small>
              </span>
            </a>
          </div>
          
        </div>
      </div>
    </div>
  </div>  
  <section class="row m-b-md">
    <div class="col-sm-6">
      <h3 class="m-b-xs text-black">10 Pemesanan Terakhir</h3>
    </div>
  </section>
  
  <div class="clearfix"></div>
  
  <table class="table table-striped m-b-none">
    <thead>
      <tr>
        <th></th>
        <th>Kode Pesanan</th>
        <th>Destinasi</th>
        <!-- <th>Kota Berangkat</th> -->
        <th>Tanggal</th>
        <th>Orang</th>
        <th width="150">Nama Pemesan</th>
        <th>Total</th>
        <th>Status</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <?php if ($new_order->num_rows()) { ?>
        <?php foreach ($new_order->result_array() as $row) { ?>
          <tr>
            <td><?php echo $row['created_at']?></td>
            <td><?php echo strtoupper($row['code'])?></td>
            <td><?php echo $row['destination_name']?></td>
            <!-- <td><?php echo $row['city']?></td> -->
            <td><?php echo date('d M Y', strtotime($row['depart_date']))?> - <?php echo date('d M Y', strtotime($row['return_date']))?></td>
            <td><?php echo $row['qty'] + $row['qty_promo']?></td>
            <td>
              <b><?php echo $row['user_name']?></b><br>
              <?php echo $row['user_email']?><br>
              <?php echo $row['contact_phone']?>
            </td>
            <td>Rp.<?php echo number_format((($row['qty'] * $row['price']) + ($row['qty_promo'] * $row['price_promo']) + $row['suplement']) - (($row['qty'] + $row['qty_promo']) * $row['cashback']))?>,-</td>
            <td>
              <?php 
              if ($row['status'] == '0') {
                echo 'Unpaid';
              } elseif ($row['status'] == '2') {
                echo 'Expire';
              } else {
                echo 'Paid';
              }
              ?>
            </td>
            <td><a href="<?php echo site_url('dashboard/detail/'.$row['id'])?>" class="btn_small">Lihat</a></td>
          </tr>  
        <?php } ?>
      <?php } else { ?>
          <tr>
            <td colspan="9" align="center">No Data</td>
          </tr>
      <?php }?>
    </tbody>
  </table>         
</section>