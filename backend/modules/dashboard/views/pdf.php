<!doctype html>	
<html>
<div style="width: 900px; margin: 0 auto; border: 1px solid #eee; padding: 20px; font-family: helvetica, arial; font-size: 13px; background: #fff;">
	<div>
		<div>
			<div >
				<span style="float: left; font-size: 22px; font-weight: bold; margin-top: 10px;"><?php echo $order->destination_name?></span>
				<span style="float: right; text-align: left;">
					KODE PESANAN
					<strong style="display: block; color: #019198; font-size: 24px;"><?php echo strtoupper($order->code)?></strong>
				</span>
			</div>
			<div style="clear: both; border-bottom: 1px solid #eee; padding-top: 20px; margin-bottom: 20px;"></div>
			<div>
				<div>
					<b>Pembayaran:</b> <?php echo ($order->installment) ? $order->installment.' bln cicilan' : 'Full Payment' ?><br>
					<b>Suplement:</b> <?php echo ($order->suplement) ? 'Yes' : 'No' ?>
				</div>
			</div>
			<div class="info_box">
				<div style="float: left; width: 45%;">
					<div style="float: left; margin-right: 20px;">
						<img src="<?php echo assets_url('images/ico_time.png')?>" alt="" height="60">
					</div>
					<label style="font-size: 20px;
					font-weight: bold; float: left; width: 40%; margin-top: 10px;">
						<div style="color: #9B9B9B; font-size: 13px; font-weight: normal;">Berangkat</div>
						<?php echo date('d-m-Y', strtotime($order->depart_date))?>
					</label>
					<label style="font-size: 20px;
					font-weight: bold;  margin-top: 10px; float: left;">
						<div style="color: #9B9B9B; font-size: 13px; font-weight: normal;">Kembali</div>
						<?php echo date('d-m-Y', strtotime($order->return_date))?>
					</label>
					<div class="clearfix"></div>
				</div>
				<div  style="float: left; width: 25%; margin-right: 30px;">
					<div style="float: left; margin-right: 20px;">
						<img src="<?php echo assets_url('images/ico_dewasa.png')?>" alt=""  height="60">
					</div>
					<div style="font-size: 20px; font-weight: bold; margin-top: 20px;"> <?php echo $order->qty + $order->qty_promo?> Wisatawan</div>
					<div class="clearfix"></div>
				</div>
				<div  style="float: left; width: 20%;">
					<?php 
					$total = (($order->qty_promo * $order->price_promo) + ($order->qty * $order->price) + $order->suplement)  - (($order->qty + $order->qty_promo) * $order->cashback);
					?>
					<div style="font-size: 13px;">TOTAL</div>
					<strong style="font-size: 20px;">Rp.<?php echo price_format($total)?>,-</strong>
				</div>
				<div style="clear: both;"></div>
			</div>
			<div style="clear: both; border-bottom: 1px solid #eee; padding-top: 20px; margin-bottom: 20px;"></div>
			<div class="infowisatawan">
				<h2>Info Pemesan</h2>
				<div style="font-size: 15px; line-height: 150%;">
					<div style="float: left; width: 450px;">
						<div style="float: left; width: 150px;">Nama</div>
						<div style="float: left; width: 250px;"><b><?php echo $order->user_name?></b></div>
						<div style="clear: both;"></div>
						<div style="float: left; width: 150px;">Email</div>
						<div style="float: left; width: 250px;"><b><?php echo $order->user_email?></b></div>
						<div style="clear: both;"></div>
						<div style="float: left; width: 150px;">No. Telepon</div>
						<div style="float: left; width: 250px;"><b><?php echo $order->contact_phone?></b></div>
					</div>
					<div style="clear: both; padding-top: 20px;"></div>
				</div>
				<br><br>
				<h2>Info Wisatawan</h2>
				<?php $i = 1;?>
				<?php foreach ($order_detail->result_array() as $detail) { ?>
					<div style="font-size: 15px; line-height: 150%;">
						<div style="float: left; width: 40px"><?php echo $i?></div>
						<div style="float: left; width: 450px;">
							<div style="float: left; width: 150px;">Nama</div>
							<div style="float: left; width: 250px;"><b><?php echo ucfirst($detail['title'])?>. <?php echo ucwords($detail['first_name'].' '.$detail['last_name'])?></b></div>
							<div style="clear: both;"></div>
							<div style="float: left; width: 150px;">Tanggal Lahir</div>
							<div style="float: left; width: 250px;"><b><?php echo date('d M Y', strtotime($detail['birthdate']))?></b></div>
							<div style="clear: both;"></div>
							<div style="float: left; width: 150px;">ID Number</div>
							<div style="float: left; width: 250px;"><b><?php echo $detail['id_number']?></b></div>
							<div style="clear: both;"></div>
							<div style="float: left; width: 150px;">No. Telepon</div>
							<div style="float: left; width: 250px;"><b><?php echo $detail['phone']?></b></div>
						</div>
						<div style="clear: both; padding-top: 20px;"></div>
					</div>
				<?php } ?>
				
			</div>
			<br>
			<div style="clear: both;"></div>
		</div>
	</div>
</div>
</body>
</html>