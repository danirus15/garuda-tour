<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->model(array(
			'dashboard_model', 
		));
	}

	/**
	 * index dashboard
	 * @return [type] [description]
	 */
	public function index()
	{
		$data = array(
			'content' => 'index',
			'total_new_order' => $this->dashboard_model->get_total_new_order(),
			'total_member' => $this->dashboard_model->get_total_register_member(),
			'new_order' => $this->dashboard_model->get_new_order(10),
		);
		$this->load->view('layouts/base', $data);
	}

	public function detail($id)
	{
		if (!is_numeric($id)) {
			redirect('/');
		}

		$data = array(
			'content' => 'detail',
			'order' => $this->dashboard_model->get_order($id)->row(),
			'order_detail' => $this->dashboard_model->get_order_detail($id),
		);
		$this->load->view('layouts/base', $data);	
	}

	public function pdf($order_id)
	{
		$this->load->library('pdfgenerator');
		$data = array(
			'order' => $this->dashboard_model->get_order($order_id)->row(),
			'order_detail' => $this->dashboard_model->get_order_detail($order_id)
		);
 
	    $html = $this->load->view('pdf', $data, true);
	    // dd($html);
	    $this->pdfgenerator->generate($html,'contoh');
	    // $this->load->view('payment_pdf', $data);
	}
}
