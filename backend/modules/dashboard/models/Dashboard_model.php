<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard_model extends MY_Model
{
    	
    public function __construct()
	{   
		parent::__construct();

	}

    /**
     * get order 
     * @param  integer $id order id
     * @return object data order   
     */
    public function get_order($id)
    {
        return $this->db->select()
            ->from('order')
            ->where('id', $id)
            ->get();
    }

    /**
     * get order detail
     * @param  integer $order_id order id
     * @return object  object order detail
     */
    public function get_order_detail($order_id)
    {
        return $this->db->select()
            ->from('order_detail')
            ->where('order_id', $order_id)
            ->get();
    }

    /**
     * get new order
     * @param  integer $limit limit data want to get
     * @return object    object data order from database
     */
    public function get_new_order($limit=10)
    {
        return $this->db->select()
            ->from('order')
            ->order_by('created_at', 'DESC')
            ->limit($limit)
            ->get();
    }

    /**
     * get total register member
     * @return int return total register member
     */
    public function get_total_register_member()
    {
        return $this->db->select()
            ->from('user')
            ->where('status', '1')
            ->count_all_results();
    }

    /**
     * get total new order
     * @return int return total all new order
     */
    public function get_total_new_order()
    {
        return $this->db->select()
            ->from('order')
            ->where('status', '0')
            ->count_all_results();
    }

}
/* End of file '/Acl_model.php' */
/* Location: ./application/models/Aclmodel.php */