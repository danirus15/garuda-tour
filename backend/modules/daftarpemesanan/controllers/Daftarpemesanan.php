<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftarpemesanan extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('datatable', array('model' => 'daftarpemesanan_model', 'rowIdCol' => 'id'));
		$this->load->model(array('daftarpemesanan_model'));
	}

	/**
	 * index list
	 * @return void
	 */
	public function index()
	{
		$data = array(
			'content' => 'index',
		);
		$this->load->view('layouts/base', $data);
	}

	/**
	 * index list data
	 * @return json
	 */
	public function index_data()
	{
		$this->datatable->setPreResultCallback(
            function(&$json) {
            	$rows =& $json['data'];
                foreach($rows as &$r) {
                    $r['$']['date'] = date('d-m-Y', strtotime($r['depart_date'])).' - '.date('d-m-Y', strtotime($r['return_date']));
                    $r['$']['user_order'] = '<b>'.$r['user_name'].'</b><br/>'.$r['user_email'].'<br/>'.$r['contact_phone'];
                    $r['$']['total'] = 'Rp.'.number_format((($r['qty'] * $r['price']) + ($r['qty_promo'] * $r['price_promo']) + $r['suplement']) - (($r['qty'] + $r['qty_promo']) * $r['cashback']));
                    if ($r['status'] == '0') {
                    	$r['status'] = 'Unpaid';
                    } elseif ($r['status'] == '2') {
                    	$r['status'] = 'Expire';
                    } else {
                    	$r['status'] = 'Paid';
                    }

                    $r['qty'] = $r['qty'] + $r['qty_promo'];
                }


            }
        );

		$json = $this->datatable->datatableJson();
		$json['csrfName'] = $this->security->get_csrf_token_name();
		$json['csrfHash'] = $this->security->get_csrf_hash();
		$this->general->json_encode($json);
	}

}
