<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Daftarpemesanan_model extends MY_Model implements DatatableModel
{
    public $table = 'order';
    public $primary_key = 'id';
    public $fillable = array();
    public $protected = array('id');
    public $timestamps = TRUE;
	
    public function __construct()
	{   

		parent::__construct();
        $this->load->library('general');

	}

    public function appendToSelectStr() {
        return array(
            'action_id' => $this->primary_key,
            'date' => $this->primary_key,
            'user_order' => $this->primary_key,
            'total' => $this->primary_key,
        );
    }

    public function fromTableStr() {
        return $this->table;
    }

    public function joinArray(){
        return NULL;
    }

    public function whereClauseArray(){
        return NULL;
    }
	
}
/* End of file '/admin_members_model.php' */
/* Location: ./application/modules/admin_members/models/daftarpemesanan_model.php */