<section class="scrollable padder">              
  <section class="row m-b-md">
    <div class="col-sm-6">
      <h3 class="m-b-xs text-black">Daftar Pemesanan</h3>
    </div>
    
  </section>
  
  <div class="clearfix"></div>
  <?php echo $this->load->view('partials/flash_messages')?>
  <table class="table table-striped m-b-none" id="my-datatable">
    <thead>
      <tr>
        <th>Contact Phone</th>
        <th>Cashback</th>
        <th>Qty Promo</th>
        <th>Price Promo</th>
        <th>Created At</th>
        <th>Tgl Berangkat</th>
        <th>Tgl Balik</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Price</th>
        <th>Suplement</th>
        <th>Kode Pesanan</th>
        <th>Destinasi</th>
        <th>Kota Berangkat</th>
        <th>Tanggal</th>
        <th>Orang</th>
        <th width="150">Nama Pemesan</th>
        <th>Total</th>
        <th>Status</th>
        <th></th>
      </tr>
    </thead>
  </table>
</section>

<script type="text/javascript">
var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
    csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
$(function () {
  var table = $('#my-datatable').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url":"<?php echo site_url('daftarpemesanan/index_data')?>",
        "type":"POST",
        "data": function(d){
          eval('d.'+csrfName+'=\''+csrfHash+'\'');
        },
        "dataSrc": function(response) {
          csrfName = response.csrfName;
          csrfHash = response.csrfHash;
          return response.data;
        }
      },
      "columns": [
        {"data":"contact_phone", "contact_phone": false, "orderable": false, "visible": false},
        {"data":"cashback", "searchable": false, "orderable": false, "visible": false},
        {"data":"qty_promo", "searchable": false, "orderable": false, "visible": false},
        {"data":"price_promo", "searchable": false, "orderable": false, "visible": false},
        {"data":"created_at", "searchable": false, "orderable": false},
        {"data":"depart_date", "searchable": false, "orderable": false, "visible": false},
        {"data":"return_date", "searchable": false, "orderable": false, "visible": false},
        {"data":"user_name", "orderable": false, "visible": false},
        {"data":"user_email", "orderable": false, "visible": false},
        {"data":"price", "searchable": false, "orderable": false, "visible": false},
        {"data":"suplement", "searchable": false, "orderable": false, "visible": false},
        {"data":"code", "searchable": false, "orderable": false},
        {"data":"destination_name", "orderable": false},
        {"data":"city", "searchable": false, "orderable": false, "visible": false},
        {"data":"$.date", "searchable": false, "orderable": false},
        {"data":"qty", "searchable": false, "orderable": false},
        {"data":"$.user_order", "searchable": false, "orderable": false},
        {"data":"$.total", "searchable": false, "orderable": false},
        {"data":"status", "searchable": false, "orderable": false},
        {   
            "data" : "$.action_id",
            "searchable" : false,
            "orderable" : false,
            "sClass" : "action",
            mRender: function(data, type, row) {
            return '<a href="<?php echo site_url('dashboard/detail/')?>' + data + '" class="btn_small">Lihat</a>';
        }

      }],
      "order":[
        [0, "desc"]
      ],
      "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
  });
});
</script>