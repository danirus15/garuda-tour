<section class="scrollable padder">              
  <section class="row m-b-md">
    <div class="col-sm-6">
      <h3 class="m-b-xs text-black">Tanggal Paket Liburan Add</h3>
    </div>
  </section>
  <!-- s:content --> 
  <?php echo form_open('',array('class'=>'form-horizontal'))?>
  <input type="hidden" name="destination_id" value="<?php echo $destination_id?>" />
  <input type="hidden" name="total_days" value="<?php echo $total_days?>" />
  <section class="panel panel-default">
    
    <header class="panel-heading font-bold">
      Paket
    </header>
    <div class="panel-body">
      <div class="form-group">
          <label class="col-sm-2 control-label">Kota Keberangkatan</label>
          <div class="col-sm-10 no-padding">
            <?php foreach($cities as $city) { ?>
                <div class="col-sm-10">
                  <div class="checkbox i-checks col-sm-10">
                    <label>
                      <input type="checkbox" name="city_<?php echo $city->id?>" value="<?php echo $city->id?>">
                      <i></i>
                      <?php echo $city->name?>
                    </label>
                  </div>
                </div>
                <div class="clearfix" style="padding-top: 5px;"></div>
                <div class="col-sm-4">
                  Harga Luar Paket<br>
                  <input name="city_price_normal_<?php echo $city->id?>" class="input-sm form-control" type="text">
                </div>
                <div class="col-sm-4">
                  Jumlah Kursi Potongan<br>
                  <input name="city_promo_seat_max_<?php echo $city->id?>" class="input-sm form-control" type="text">
                </div>
                <div class="col-sm-4">
                  Harga Potongan<br>
                  <input name="city_promo_seat_discount_<?php echo $city->id?>" class="input-sm form-control" type="text">
                </div>
                <div class="col-sm-4">
                  Harga Paket<br>
                  <input name="city_price_<?php echo $city->id?>" class="input-sm form-control" type="text">
                </div>
                <div class="col-sm-4">
                  Suplement Harga<br>
                  <input name="city_suplement_<?php echo $city->id?>" class="input-sm form-control" size="16" type="text">
                </div>
                <div class="clearfix" style="padding-top: 10px;"></div>
            <?php } ?>
          </div>
      </div>
      <div class="form-group <?php echo (form_error('depart_date')) ? 'has-error' : '' ?>">
          <label class="col-sm-2 control-label">Tanggal Berangkat</label>
          <div class="col-sm-10">
            <?php echo form_input('depart_date', set_value('depart_date'), array('class'=>'input-s form-control datepicker-input', 'data-date-format' => 'yyyy-mm-dd'))?>
            <?php echo form_error('depart_date');?>
          </div>
      </div>
      <div class="form-group <?php echo (form_error('return_date')) ? 'has-error' : '' ?>">
          <label class="col-sm-2 control-label">Total Hari</label>
          <div class="col-sm-10">
            <?php echo $total_days ?> Hari
          </div>
      </div>
      <!-- <div class="form-group <?php echo (form_error('return_date')) ? 'has-error' : '' ?>">
          <label class="col-sm-2 control-label">Tanggal Pulang</label>
          <div class="col-sm-10">
            <?php echo form_input('return_date', set_value('return_date'), array('class'=>'input-s form-control datepicker-input', 'data-date-format' => 'yyyy-mm-dd'))?>
            <?php echo form_error('return_date');?>
          </div>
      </div> -->
      <div class="form-group <?php echo (form_error('end_date_order')) ? 'has-error' : '' ?>">
          <label class="col-sm-2 control-label">Tanggal Batas Pesan Akhir</label>
          <div class="col-sm-10">
            <?php echo form_input('end_date_order', set_value('end_date_order'), array('class'=>'input-s form-control datepicker-input', 'data-date-format' => 'yyyy-mm-dd'))?>
            <?php echo form_error('end_date_order');?>
          </div>
      </div>
      <div class="form-group <?php echo (form_error('total_seat')) ? 'has-error' : '' ?>">
          <label class="col-sm-2 control-label">Jumlah Kursi</label>
          <div class="col-sm-10">
            <?php echo form_input('total_seat', set_value('total_seat'), array('class'=>'input-s form-control '))?>
            <?php echo form_error('total_seat');?>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-2 control-label">Status <a class="mandatory tip" title="" data-original-title="Wajib diisi">*</a></label>
          <div class="col-md-3">
            <select name="status" class="form-control" required="">
              <?php echo $option_status?>
            </select>
          </div>
        </div>
    </div> 
    <div class="panel-body">
     <div class="form-group">
          <label class="col-sm-2 control-label">&nbsp;</label>
          <div class="col-sm-4">
            <a href="<?php echo site_url('paketliburan')?>" class="btn btn-default">Batal</a>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </div>
      </div>
  </section>
 
  </form>
  <!-- e:content -->
  <div class="clearfix"></div>
  

</section>