<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('datatable', array('model' => 'schedule_model', 'rowIdCol' => 'id'));
		$this->load->model(array('schedule_model', 'cities_model', 'scheduleprice_model', 'paketliburan/paketliburan_model'));
	}

	/**
	 * add new schedule
	 */
	public function add($destination_id)
	{
		if ($this->input->post()) {
			$id = $this->schedule_model->from_form()->insert();
			if ($id) {

				$this->add_schedule_price($id);

				$this->session->set_flashdata('success', 'Data success save');
				redirect('paketliburan');
			}
		}

		$destination = $this->paketliburan_model->get($destination_id);

		$data = array(
			'content' => 'add',
			'cities' => $this->cities_model->where('status', '1')->get_all(),
			'option_status' => $this->general->option_status(),
			'destination_id' => $destination_id,
			'total_days' => $destination->total_days,
		);
		$this->load->view('layouts/base', $data);
	}

	private function add_schedule_price($schedule_id) {

		$cities = $this->cities_model->where('status', '1')->get_all();
		foreach ($cities as $city) {
			if (isset($_POST['city_'.$city->id])) {
				$this->scheduleprice_model->insert(array(
					'schedule_id' => $schedule_id,
					'city_id' => $city->id,
					'city_name' => $city->name,
					'price' => $this->input->post('city_price_'.$city->id),
					'price_normal' => $this->input->post('city_price_normal_'.$city->id),
					'suplement' => $this->input->post('city_suplement_'.$city->id),
					'promo_seat_max' => $this->input->post('city_promo_seat_max_'.$city->id),
					'promo_seat_discount' => $this->input->post('city_promo_seat_discount_'.$city->id),
				));
			}
		}
	}

	/**
	 * edit schedule
	 * @param  int $id schedule id
	 * @return void
	 */
	public function edit($id)
	{
		$cities = $this->cities_model->where('status', '1')->get_all();
		if (!is_numeric($id)) {
			$this->session->set_flashdata('error', 'ID data must numeric');
			redirect('paketliburan');
		}

		$record = $this->schedule_model->get($id);

		$prices = array();
		$prices_normal = array();
		$total_seats = array();
		$total_seats_discount = array();
		$suplement = array();
		$schedule_price_query = $this->scheduleprice_model->get_all(array('schedule_id' => $record->id));
		
		foreach ($schedule_price_query as $price) {
			$prices[$price->city_id] = $price->price;
			$prices_normal[$price->city_id] = $price->price_normal;
			$promo_seats_max[$price->city_id] = $price->promo_seat_max;
			$promo_seats_discount[$price->city_id] = $price->promo_seat_discount;
			$suplement[$price->city_id] = $price->suplement;

		}
		if (!$record) {
			$this->session->set_flashdata('error', 'Data not found');
			redirect('paketliburan');
		}

		if ($this->input->post()) {

			//update data
			$data_update = $this->input->post();
			$result = $this->schedule_model->update($data_update, $id);
			
			if ($result) {

				$this->scheduleprice_model->delete(array('schedule_id' => $id));

				$this->add_schedule_price($id);
				$this->session->set_flashdata('success', 'Update data success');				
				redirect('paketliburan');
			}
		}

		$destination = $this->paketliburan_model->get($record->destination_id);

		$data = array(
			'record' => $record,
			'content' => 'edit',
			'cities' => $cities,
			'prices' => $prices,
			'prices_normal' => $prices_normal,
			'promo_seats_max' => $promo_seats_max,
			'promo_seats_discount' => $promo_seats_discount,
			'suplement' => $suplement,
			'option_status' => $this->general->option_status($record->status),
			'total_days' => $destination->total_days,
		);

		$this->load->view('layouts/base', $data);
	}

	/**
	 * delete schedule
	 * @param  int $id schedule id
	 * @return void
	 */
	public function delete($id)
	{
		if (!is_numeric($id)) {
			$this->session->set_flashdata('error', 'ID data must numeric');
		}

		$schedule = $this->schedule_model->get($id);
		if (!$schedule) {
			$this->session->set_flashdata('error', 'Data not found');
		}

		if ($this->schedule_model->delete($id)) {

			$this->scheduleprice_model->delete(array('schedule_id' => $schedule->id));
			$this->session->set_flashdata('success', 'Delete data success');
		} else {
			$this->session->set_flashdata('error', 'Delete data failed');
		}

		redirect('paketliburan');

	}

}
