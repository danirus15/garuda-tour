<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Scheduleprice_model extends MY_Model implements DatatableModel
{
    public $table = 'schedule_price';
    public $primary_key = 'id';
    public $fillable = array();
    public $protected = array('id');
    public $timestamps = TRUE;
    public $before_create = array('create_by');
    public $before_update = array('modify_by');
	
    public function __construct()
	{   

		parent::__construct();
        $this->has_one['city'] = array('foreign_model'=>'Cities_model','foreign_table'=>'cities','foreign_key'=>'id','local_key'=>'city_id');
        $this->has_one['schedule'] = array('foreign_model'=>'Schedule_model','foreign_table'=>'schedule','foreign_key'=>'id','local_key'=>'schedule_id');
        $this->load->library('general');

	}

    /**
     * log scheduleprice create data
     * @param  array $data
     * @return array     
     */
    public function create_by($data)
    {
        
        $data['created_by'] = $this->session->userdata('user_id');
        
        return $data;
    }

    /**
     * log scheduleprice modify data
     * @param  array $data
     * @return array     
     */
    public function modify_by($data)
    {
        
        $data['updated_by'] = $this->session->userdata('user_id');
        
        return $data;
    }

    /**
     * rules validate insert/update admin_members
     * @var array
     */
    public $rules = array(
        'insert' => array(
            'schedule_id' => array(
                'field' => 'schedule_id',
                'label' => 'Schedule',
                'rules' => 'trim|required'
            ),
            'city_id' => array(
                'field' => 'city_id',
                'label' => 'City',
                'rules' => 'trim|required'
            ),
            'price' => array(
                'field' => 'price',
                'label' => 'Price',
                'rules' => 'trim|required'
            ),
            'suplement' => array(
                'field' => 'suplement',
                'label' => 'Suplement',
                'rules' => 'trim|required'
            ),
        ),
        'update' => array(
            'schedule_id' => array(
                'field' => 'schedule_id',
                'label' => 'Schedule',
                'rules' => 'trim|required'
            ),
            'city_id' => array(
                'field' => 'city_id',
                'label' => 'City',
                'rules' => 'trim|required'
            ),
            'price' => array(
                'field' => 'price',
                'label' => 'Price',
                'rules' => 'trim|required'
            ),
            'suplement' => array(
                'field' => 'suplement',
                'label' => 'Suplement',
                'rules' => 'trim|required'
            ),
        )  
    );

    public function appendToSelectStr() {
        return array(
            'action_id' => $this->primary_key
        );
    }

    public function fromTableStr() {
        return $this->table;
    }

    public function joinArray(){
        return NULL;
    }

    public function whereClauseArray(){
        return array('1' => '1');
    }
	
}
/* End of file '/admin_members_model.php' */
/* Location: ./application/modules/admin_members/models/scheduleprice_model.php */