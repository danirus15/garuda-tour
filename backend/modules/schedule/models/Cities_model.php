<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Cities_model extends MY_Model implements DatatableModel
{
    public $table = 'cities';
    public $primary_key = 'id';
    public $fillable = array();
    public $protected = array('id');
    public $timestamps = FALSE;
	
    public function __construct()
	{   

		parent::__construct();
        $this->has_many['scheduleprice'] = array('foreign_model'=>'Scheduleprice_model','foreign_table'=>'schedule_price','foreign_key'=>'city_id','local_key'=>'id');
        $this->load->library('general');

	}

    /**
     * rules validate insert/update admin_members
     * @var array
     */
    public $rules = array(
        'insert' => array(
            'name' => array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required'
            ),
        ),
        'update' => array(
            'name' => array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required'
            ),
        )  
    );

    public function appendToSelectStr() {
        return array(
            'action_id' => $this->primary_key
        );
    }

    public function fromTableStr() {
        return $this->table;
    }

    public function joinArray(){
        return NULL;
    }

    public function whereClauseArray(){
        return array('1' => '1');
    }
	
}
/* End of file '/admin_members_model.php' */
/* Location: ./application/modules/admin_members/models/cities_model.php */