<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Schedule_model extends MY_Model implements DatatableModel
{
    public $table = 'schedule';
    public $primary_key = 'id';
    public $fillable = array();
    public $protected = array('id');
    public $timestamps = TRUE;
    public $before_create = array('create_by');
    public $before_update = array('modify_by');
	
    public function __construct()
	{   

		parent::__construct();

        $this->has_one['paketliburan'] = array('foreign_model'=>'Paketliburan_model','foreign_table'=>'destination','foreign_key'=>'id','local_key'=>'destination_id');
        $this->has_many['scheduleprice'] = array('foreign_model'=>'Scheduleprice_model','foreign_table'=>'schedule_price','foreign_key'=>'schedule_id','local_key'=>'id');

        $this->load->library('general');

	}

    /**
     * log schedule create data
     * @param  array $data
     * @return array     
     */
    public function create_by($data)
    {
        if (isset($data['depart_date'])) {
            $data['return_date'] = date('Y-m-d', strtotime($data['depart_date']." +".$this->input->post('total_days')." days"));    
        }
        
        $data['created_by'] = $this->session->userdata('user_id');
        
        return $data;
    }

    /**
     * log schedule modify data
     * @param  array $data
     * @return array     
     */
    public function modify_by($data)
    {   
        if (isset($data['depart_date'])) {
            $data['return_date'] = date('Y-m-d', strtotime($data['depart_date']." +".$this->input->post('total_days')." days"));    
        }
        
        $data['updated_by'] = $this->session->userdata('user_id');
        
        return $data;
    }

    /**
     * rules validate insert/update admin_members
     * @var array
     */
    public $rules = array(
        'insert' => array(
            'destination_id' => array(
                'field' => 'destination_id',
                'label' => 'Destination',
                'rules' => 'trim|required'
            ),
            'depart_date' => array(
                'field' => 'depart_date',
                'label' => 'Tanggal Berangkat',
                'rules' => 'trim|required'
            ),
            'end_date_order' => array(
                'field' => 'end_date_order',
                'label' => 'Tanggal Batas Akhir Pesan',
                'rules' => 'trim|required'
            ),
            'total_seat' => array(
                'field' => 'total_seat',
                'label' => 'Jumlah Kursi',
                'rules' => 'trim|required|numeric'
            ),
            'status' => array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required'
            ),
        ),
        'update' => array(
            'destination_id' => array(
                'field' => 'destination_id',
                'label' => 'Destination',
                'rules' => 'trim|required'
            ),
            'depart_date' => array(
                'field' => 'depart_date',
                'label' => 'Tanggal Berangkat',
                'rules' => 'trim|required'
            ),
            'end_date_order' => array(
                'field' => 'end_date_order',
                'label' => 'Tanggal Batas Akhir Pesan',
                'rules' => 'trim|required'
            ),
            'total_seat' => array(
                'field' => 'total_seat',
                'label' => 'Jumlah Kursi',
                'rules' => 'trim|required|numeric'
            ),
            'status' => array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required'
            ),
        )  
    );

    public function appendToSelectStr() {
        return array(
            'action_id' => $this->primary_key
        );
    }

    public function fromTableStr() {
        return $this->table;
    }

    public function joinArray(){
        return NULL;
    }

    public function whereClauseArray(){
        return array('1' => '1');
    }
	
}
/* End of file '/admin_members_model.php' */
/* Location: ./application/modules/admin_members/models/schedule_model.php */