<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Paketliburan_model extends MY_Model implements DatatableModel
{
    public $table = 'destination';
    public $primary_key = 'id';
    public $fillable = array();
    public $protected = array('id');
    public $timestamps = TRUE;
    public $before_create = array('upload_photo', 'create_by');
    public $before_update = array('upload_photo', 'modify_by');
	
    public function __construct()
	{   

		parent::__construct();

        $this->has_many['gallery'] = array('foreign_model'=>'Gallery_model','foreign_table'=>'gallery','foreign_key'=>'destination_id','local_key'=>'id');
        $this->has_many['schedule'] = array('foreign_model'=>'Schedule_model','foreign_table'=>'schedule','foreign_key'=>'destination_id','local_key'=>'id');

        $this->load->library('general');

	}

    public function delete_all_destination_attribute($destination_id)
    {
        $this->db->query('
            DELETE FROM schedule_price WHERE schedule_id IN (
                SELECT id FROM schedule WHERE destination_id = '.$destination_id.'
            )
        ');

        $this->db->query('
            DELETE FROM schedule WHERE destination_id = '.$destination_id.'
        ');
    }

    public function get_avaliable_seat($schedule_id, $city_id) 
    {
        $remind = 0;
        $query = $this->db->query('
            SELECT SUM(qty) total_normal, SUM(qty_promo) total_promo FROM `order` WHERE schedule_id = '.$schedule_id.' AND city_id = '.$city_id.' AND status != \'2\'
        ');
        if ($query->num_rows()) {
            $result = $query->row();
            $remind = $result->total_normal + $result->total_promo;
        }

        return $remind;
    }

    /**
     * upload photo
     * @param  array $data
     * @return array
     */
    public function upload_photo($data)
    {

        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $data_upload = $this->general->upload_file('userfile', 1400, 800);

            if ($data_upload['status']) {
                $full_path = explode('uploads', $data_upload['data']['file_path'])[1];
                $file_name = $data_upload['data']['file_name'];
                $data['photo'] = $full_path.$file_name;
            }
        }

        return $data;
    }

    /**
     * log paketliburan create data
     * @param  array $data
     * @return array     
     */
    public function create_by($data)
    {
        
        $data['created_by'] = $this->session->userdata('paketliburan_id');
        
        return $data;
    }

    /**
     * log paketliburan modify data
     * @param  array $data
     * @return array     
     */
    public function modify_by($data)
    {
        
        $data['updated_by'] = $this->session->userdata('paketliburan_id');
        
        return $data;
    }

    /**
     * rules validate insert/update admin_members
     * @var array
     */
    public $rules = array(
        'insert' => array(
            'destination_id' => array(
                'field' => 'destination_id',
                'label' => 'Destinasi Kota',
                'rules' => 'trim|required'
            ),
            'destination_en' => array(
                'field' => 'destination_en',
                'label' => 'Destination City',
                'rules' => 'trim|required'
            ),
            'title_id' => array(
                'field' => 'title_id',
                'label' => 'Judul',
                'rules' => 'trim|required'
            ),
            'title_en' => array(
                'field' => 'title_en',
                'label' => 'Title',
                'rules' => 'trim|required'
            ),
            'summary_id' => array(
                'field' => 'summary_id',
                'label' => 'Ringkasan',
                'rules' => 'trim|required'
            ),
            'summary_en' => array(
                'field' => 'summary_en',
                'label' => 'Summary',
                'rules' => 'trim|required'
            ),
            'description_id' => array(
                'field' => 'description_id',
                'label' => 'Deskripsi',
                'rules' => 'trim|required'
            ),
            'description_en' => array(
                'field' => 'description_en',
                'label' => 'Description',
                'rules' => 'trim|required'
            ),
            'trip_plan_id' => array(
                'field' => 'trip_plan_id',
                'label' => 'Rencana Perjalanan',
                'rules' => 'trim|required'
            ),
            'trip_plan_en' => array(
                'field' => 'trip_plan_en',
                'label' => 'Trip Plan',
                'rules' => 'trim|required'
            ),
            'total_days' => array(
                'field' => 'total_days',
                'label' => 'Total Hari',
                'rules' => 'trim|numeric|required|greater_than[0]'
            ),
        ),
        'update' => array(
            'title_id' => array(
                'field' => 'title_id',
                'label' => 'Judul',
                'rules' => 'trim|required'
            ),
            'title_en' => array(
                'field' => 'title_en',
                'label' => 'Title',
                'rules' => 'trim|required'
            ),
            'summary_id' => array(
                'field' => 'summary_id',
                'label' => 'Ringkasan',
                'rules' => 'trim|required'
            ),
            'summary_en' => array(
                'field' => 'summary_en',
                'label' => 'Summary',
                'rules' => 'trim|required'
            ),
            'description_id' => array(
                'field' => 'description_id',
                'label' => 'Deskripsi',
                'rules' => 'trim|required'
            ),
            'description_en' => array(
                'field' => 'description_en',
                'label' => 'Description',
                'rules' => 'trim|required'
            ),
            'trip_plan_id' => array(
                'field' => 'trip_plan_id',
                'label' => 'Rencana Perjalanan',
                'rules' => 'trim|required'
            ),
            'trip_plan_en' => array(
                'field' => 'trip_plan_en',
                'label' => 'Trip Plan',
                'rules' => 'trim|required'
            ),
            'total_days' => array(
                'field' => 'total_days',
                'label' => 'Total Hari',
                'rules' => 'trim|numeric|required|greater_than[0]'
            ),
            'is_sold_out' => array(
                'field' => 'is_sold_out',
                'label' => 'Sold Out',
                'rules' => 'trim|numeric'
            ),
            'order_number' => array(
                'field' => 'order_number',
                'label' => 'Order Number',
                'rules' => 'trim|numeric'
            ),
        )  
    );

    public function appendToSelectStr() {
        return array(
            'action_id' => $this->primary_key
        );
    }

    public function fromTableStr() {
        return $this->table;
    }

    public function joinArray(){
        return NULL;
    }

    public function whereClauseArray(){
        return array('1' => '1');
    }
	
}
/* End of file '/admin_members_model.php' */
/* Location: ./application/modules/admin_members/models/paketliburan_model.php */