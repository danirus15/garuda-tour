<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');
class Gallery_model extends MY_Model implements DatatableModel
{
    public $table = 'gallery';
    public $primary_key = 'id';
    public $fillable = array();
    public $protected = array('id');
    public $timestamps = FALSE;
    public $before_create = array('upload_photo');
    public $before_update = array('upload_photo');
	
    public function __construct()
	{   

		parent::__construct();

        $this->has_one['paketliburan'] = array('foreign_model'=>'Paketliburan_model','foreign_table'=>'destination','foreign_key'=>'id','local_key'=>'destination_id');

        $this->load->library('general');

	}

    /**
     * upload photo
     * @param  array $data
     * @return array
     */
    public function upload_photo($data)
    {
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $data_upload = $this->general->upload_file('userfile', 1400, 800);

            if ($data_upload['status']) {
                $full_path = explode('uploads', $data_upload['data']['file_path'])[1];
                $file_name = $data_upload['data']['file_name'];
                $data['photo'] = $full_path.$file_name;
            }
        }

        return $data;
    }

    /**
     * rules validate insert/update admin_members
     * @var array
     */
    public $rules = array(
        'insert' => array(
            'destination_id' => array(
                'field' => 'destination_id',
                'label' => 'Destination',
                'rules' => 'trim'
            ),
            'caption' => array(
                'field' => 'caption',
                'label' => 'Caption',
                'rules' => 'trim'
            ),
        ),
        'update' => array(
            'destination_id' => array(
                'field' => 'destination_id',
                'label' => 'Destination',
                'rules' => 'trim'
            ),
            'caption' => array(
                'field' => 'caption',
                'label' => 'Caption',
                'rules' => 'trim'
            ),
        )  
    );

    public function appendToSelectStr() {
        return array(
            'action_id' => $this->primary_key
        );
    }

    public function fromTableStr() {
        return $this->table;
    }

    public function joinArray(){
        return NULL;
    }

    public function whereClauseArray(){
        return array('1' => '1');
    }
	
}
/* End of file '/admin_members_model.php' */
/* Location: ./application/modules/admin_members/models/paketliburan_model.php */