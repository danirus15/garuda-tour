<section class="scrollable padder">              
  <section class="row m-b-md">
    <div class="col-sm-6">
      <h3 class="m-b-xs text-black">Paket Liburan</h3>
    </div>
    <a href="<?php echo site_url('paketliburan/add')?>" class="btn btn-success fr m15">Tambah</a>
  </section>
  
  <div class="clearfix"></div>
  
  <table class="table table-striped m-b-none">
    <thead>
      <tr>
        <th width="80%" colspan="6">Destinasi</th>
        <th width="10%">Urutan</th>
        <th width="10%">Action</th>
      </tr>
    </thead>
    <tbody>
      <?php if ($records) { ?>
        <?php foreach ($records as $record) { ?>
          <tr class="destinasi">
            <td colspan="5">
              <b><?php echo $record->title_id?></b>
            </td>
            <td>
              <a href="<?php echo site_url('schedule/add/'.$record->id)?>" class="btn_small">Tambah Tanggal</a>
            </td>
            <td>
              <select name="order_number" class="order_number">
                <?php for ($i=0; $i<=count($records);$i++) { ?>
                  <?php if ($i == $record->order_number) {?>
                    <option value="<?php echo $record->order_number?>|<?php echo $i?>|<?php echo $record->id?>" selected><?php echo $i?></option>
                  <?php } else { ?>
                    <option value="<?php echo $record->order_number?>|<?php echo $i?>|<?php echo $record->id?>"><?php echo $i?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </td>
            <td class="action">
              
              <a href="<?php echo site_url('paketliburan/edit/'.$record->id)?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="<?php echo assets_url('images/ico_edit.png')?>" alt=""></a>
              <a href="javascript:void(0);" onclick="confirm_delete('<?php echo site_url('paketliburan/delete/'.$record->id)?>');"><img src="<?php echo assets_url('images/ico_del.png')?>" alt=""></a>
            </td>
          </tr>
          <?php if (isset($record->schedule)) { ?>
            <?php foreach ($record->schedule as $schedule) { ?>
              <tr class="tanggal">
                <td>
                  <?php echo date('d F Y', strtotime($schedule->depart_date))?> - <?php echo date('d F Y', strtotime($schedule->return_date))?><br>
                  Batas pesan:  <?php echo date('d F Y', strtotime($schedule->end_date_order))?>
                </td>
                <td colspan="2">
                  <?php $taken_seat = 0?>
                  <?php if (isset($schedule->scheduleprice)) { ?>
                    <?php foreach ($schedule->scheduleprice as $price) { ?>
                      <span class="kota"><?php echo $price->city_name?></span> Rp. <?php echo number_format($price->price)?>,-<br>
                      <?php $taken_seat += $this->paketliburan_model->get_avaliable_seat($schedule->id, $price->city_id);?>
                    <?php } ?>
                  <?php } ?>
                </td>
                <td><?php echo $schedule->total_seat?> Kursi</td>
                <td>Sisa <?php echo ($schedule->total_seat - $taken_seat)?> Kursi</td>
                <td>Status: <?php echo ($schedule->status == '1') ? 'Active' : 'Inactive'?></td>
                <td>
                </td>
                <td class="action">
                  <a href="<?php echo site_url('schedule/edit/'.$schedule->id)?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="<?php echo assets_url('images/ico_edit.png')?>" alt=""></a>
                  <a href="javascript:void(0);" onclick="confirm_delete('<?php echo site_url('schedule/delete/'.$schedule->id)?>');"><img src="<?php echo assets_url('images/ico_del.png')?>" alt=""></a>
                </td>
              </tr>
            <?php } ?>
          <?php } ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>

</section>
<script type="text/javascript">
  var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
      csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
  $(document).ready(function() {
    $('.order_number').on('change', function() {
      var selected_value = $(this).val();
      var data_part = selected_value.split('|');
      var prev_order = data_part[0];
      var selected_order = data_part[1];
      var destination_id = data_part[2];
      console.log(selected_value);
      $.post('<?php echo site_url('paketliburan/reorder')?>', {
        prev_order: prev_order,
        selected_order: selected_order,
        destination_id: destination_id,
        [csrfName]: csrfHash,
      })
      .done(function(response) {
        console.log(response);
        csrfName = response.csrfName;
        csrfHash = response.csrfHash;
      });
    });
  });
</script>