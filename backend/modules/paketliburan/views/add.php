
<section class="scrollable padder">              
  <?php echo form_open_multipart('',array('class'=>'form-horizontal'))?>
  <section class="row m-b-md">
    <div class="col-sm-6">
      <h3 class="m-b-xs text-black">Destinasi Paket Liburan Add</h3>
    </div>
  </section>
  <!-- s:content --> 
  

  <section class="panel panel-default">
    <header class="panel-heading font-bold">
      Content
    </header>
    <div class="panel-body">
        
        <section class="panel panel-default">
          <header class="panel-heading bg-light">
            <ul class="nav nav-tabs nav-justified">
              <li class="active"><a href="#indonesia" data-toggle="tab">Indonesia</a></li>
              <li><a href="#english" data-toggle="tab">English</a></li>
            </ul>
          </header>
          <div class="panel-body">
            <div class="tab-content">
              <!-- s:english -->
              <div class="tab-pane active" id="indonesia">
                <div class="form-group <?php echo (form_error('destination_id')) ? 'has-error' : '' ?>">
                  <label class="col-sm-2 control-label">Destinasi Kota</label>
                  <div class="col-sm-10">
                    <?php echo form_input('destination_id',set_value('destination_id'), array('class'=>'form-control', 'placeholder' => 'Destinasi Kota'))?>
                    <?php echo form_error('destination_id');?>
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group <?php echo (form_error('title_id')) ? 'has-error' : '' ?>">
                  <label class="col-sm-2 control-label">Judul</label>
                  <div class="col-sm-10">
                    <?php echo form_input('title_id',set_value('title_id'), array('class'=>'form-control', 'placeholder' => 'Judul'))?>
                    <?php echo form_error('title_id');?>
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group <?php echo (form_error('summary_id')) ? 'has-error' : '' ?>">
                  <label class="col-sm-2 control-label">Ringkasan</label>
                  <div class="col-md-10">
                    <?php echo form_textarea('summary_id', set_value('summary_id', '', false), array('class'=>'input-sm form-control', 'cols' => 30, 'rows' => 10))?>
                    <?php echo form_error('summary_id');?>
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group <?php echo (form_error('description_id')) ? 'has-error' : '' ?>">
                  <label class="col-sm-2 control-label">Deskripsi</label>
                  <div class="col-md-10">
                    <?php echo form_textarea('description_id', set_value('description_id', '', false), array('class'=>'input-sm form-control', 'cols' => 30, 'rows' => 10))?>
                    <?php echo form_error('description_id');?>
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group <?php echo (form_error('trip_plan_id')) ? 'has-error' : '' ?>">
                  <label class="col-sm-2 control-label">Rencana Perjalanan</label>
                  <div class="col-md-10">
                    <?php echo form_textarea('trip_plan_id', set_value('trip_plan_id', '', false), array('class'=>'input-sm form-control', 'cols' => 30, 'rows' => 10))?>
                    <?php echo form_error('trip_plan_id');?>
                  </div>
                </div>
              </div>
              <!-- s:english -->
              <!-- s:arab -->
              <div class="tab-pane" id="english">
                <div class="form-group <?php echo (form_error('destination_en')) ? 'has-error' : '' ?>">
                  <label class="col-sm-2 control-label">Destination City</label>
                  <div class="col-sm-10">
                    <?php echo form_input('destination_en',set_value('destination_id'), array('class'=>'form-control', 'placeholder' => 'Destination City'))?>
                    <?php echo form_error('destination_en');?>
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group <?php echo (form_error('title_en')) ? 'has-error' : '' ?>">
                  <label class="col-sm-2 control-label">Title</label>
                  <div class="col-sm-10">
                    <?php echo form_input('title_en',set_value('title_en', '', false), array('class'=>'form-control', 'placeholder' => 'Title'))?>
                    <?php echo form_error('title_en');?>
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group <?php echo (form_error('summary_en')) ? 'has-error' : '' ?>">
                  <label class="col-sm-2 control-label">Highlight</label>
                  <div class="col-md-10">
                    <?php echo form_textarea('summary_en', set_value('summary_en', '', false), array('class'=>'input-sm form-control', 'cols' => 30, 'rows' => 10))?>
                    <?php echo form_error('summary_en');?>
                    
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group <?php echo (form_error('description_en')) ? 'has-error' : '' ?>">
                  <label class="col-sm-2 control-label">Decription</label>
                  <div class="col-md-10">
                    <?php echo form_textarea('description_en', set_value('description_en', '', false), array('class'=>'input-sm form-control', 'cols' => 30, 'rows' => 10))?>
                    <?php echo form_error('description_en');?>
                  </div>
                </div>
                <div class="line line-dashed b-b line-lg pull-in"></div>
                <div class="form-group <?php echo (form_error('trip_plan_en')) ? 'has-error' : '' ?>">
                  <label class="col-sm-2 control-label">Itenerary</label>
                  <div class="col-md-10">
                    <?php echo form_textarea('trip_plan_en', set_value('trip_plan_en', '', false), array('class'=>'input-sm form-control', 'cols' => 30, 'rows' => 10))?>
                    <?php echo form_error('trip_plan_en');?>
                    
                  </div>
                </div>
              </div>
              <!-- s:arab -->
            </div>
          </div>
    <header class="panel-heading font-bold">
      Schedule
    </header>
    <div class="panel-body">
      <div class="form-group">
        <label class="col-sm-2 control-label">Total Hari</label>
        <div class="col-sm-10">
          <?php echo form_input('total_days',set_value('total_days'), array('class'=>'form-control', 'placeholder' => 'Total hari'))?>
          <?php echo form_error('total_days');?>
        </div>   
      </div>
    </div>
        <header class="panel-heading font-bold">
      Image Cover
    </header>
    <div class="panel-body">
      <div class="form-group">
          <label class="col-sm-2 control-label">Foto</label>
          <div class="col-sm-10">
            <label class="foto-form">
              <div id="imagePreview"></div>
              <?php echo form_upload('userfile','', array('class'=>'img', 'id' => 'uploadFile'))?>
              <?php echo form_error('userfile');?>
              <span>Upload Foto</span>
            </label>
            *Ukuran 1400x800px
          </div>
        </div>
    </div>
        <header class="panel-heading font-bold">
          Galeri
        </header>
        <div class="panel-body">
            <div class="form-group">
              <div class="group-img">
                <div class="img-area">
                    <div class="img-wrap">
                        <div class="img-loc">
                            <div class="style-input">
                                <input type="file" name="files[]" id="file1" class="inputfile" onchange="PreviewImage(1);" />
                                <label class="file" for="file1"><span>Upload Foto</span></label>
                            </div>
                            <div class="clearfix"></div>
                            <div id="appendImg1" class="appImg"></div>
                        </div>
                        <div class="img-cap">
                            <h5>Caption</h5>
                            <input type="text" name="captions[]">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <input type="button" id="addImg" class="btn_add" value="Tambah Foto">
            </div>
        </div>
      </section>
    </div>
    <section class="panel panel-default">
    <div class="panel-body">
     <div class="form-group">
          <div class="col-sm-4">
            <a href="<?php echo site_url('paketliburan')?>" class="btn btn-default">Batal</a>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </div>
      </div>
  </section>
 
  </form>
  <!-- e:content -->
  <div class="clearfix"></div>
  

</section>
<script type="text/javascript">
  var baseUrl = '<?php echo base_url()?>';
</script>