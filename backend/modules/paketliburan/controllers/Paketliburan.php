<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paketliburan extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('datatable', array('model' => 'paketliburan_model', 'rowIdCol' => 'id'));
		$this->load->model(array(
			'paketliburan_model', 
			'gallery_model', 
			'schedule/schedule_model', 
			'schedule/scheduleprice_model', 
			'schedule/cities_model'
		));
	}

	/**
	 * index list
	 * @return void
	 */
	public function index()
	{
		$data = array(	
			'content' => 'index',
			'records' => $this->paketliburan_model->with_schedule(array('with' => array('relation' => 'scheduleprice')))->get_all(),
			'option_status' => $this->general->option_status(),
		);
		$this->load->view('layouts/base', $data);
	}

	/**
	 * add new paketliburan
	 */
	public function add()
	{

		if ($this->input->post()) {
			$id = $this->paketliburan_model->from_form()->insert();
			if ($id) {

				$this->upload_gallery($id);
				$this->session->set_flashdata('success', 'Data success save');
				redirect('paketliburan');
			}
		}

		$data = array(
			'content' => 'add',
		);
		$this->load->view('layouts/base', $data);
	}

	private function upload_gallery($destination_id) 
	{
		if (isset($_FILES['files']) && !empty($_FILES['files']['name'])) {
            $total_file = count($_FILES['files']['name']);
            for ($i=0; $i < $total_file; $i++) { 
                $_FILES['userfile']['name'] = $_FILES['files']['name'][$i];
                $_FILES['userfile']['type'] = $_FILES['files']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['userfile']['error'] = $_FILES['files']['error'][$i];
                $_FILES['userfile']['size'] = $_FILES['files']['size'][$i];

                $_POST['destination_id'] = $destination_id;
                $_POST['caption'] = $_POST['captions'][$i];

                if (!empty($_FILES['userfile']['name'])) {
                	$this->gallery_model->from_form()->insert();		
                }
                
            }
        }
	}

	/**
	 * edit paketliburan
	 * @param  int $id paketliburan id
	 * @return void
	 */
	public function edit($id)
	{
		
		if (!is_numeric($id)) {
			$this->session->set_flashdata('error', 'ID data must numeric');
			redirect('paketliburan');
		}

		$record = $this->paketliburan_model->with_gallery()->get($id);
		
		if (!$record) {
			$this->session->set_flashdata('error', 'Data not found');
			redirect('paketliburan');
		}

		if ($this->input->post()) {

			//update data
			$data_update = $this->input->post();
			if (!isset($_POST['is_sold_out'])) {
				$data_update['is_sold_out'] = 0;
			}
			if (!isset($_POST['status'])) {
				$data_update['status'] = 0;
			}
			unset($data_update['captions']);
			$result = $this->paketliburan_model->update($data_update, $id);
			if ($result) {
				$this->upload_gallery($id);

				$schedules = $this->schedule_model->get_all(array('destination_id' => $id));
				
				if ($schedules) {
					foreach ($schedules as $schedule) {
						$return_date = date('Y-m-d', strtotime($schedule->depart_date." +".$this->input->post('total_days')." days"));
						$this->schedule_model->update(array('return_date' => $return_date), $schedule->id);
					}
				}

				$this->session->set_flashdata('success', 'Update data success');				
				redirect('paketliburan');
			}
		}

		$data = array(
			'record' => $record,
			'content' => 'edit',
		);

		$this->load->view('layouts/base', $data);
	}

	/**
	 * delete paketliburan
	 * @param  int $id paketliburan id
	 * @return void
	 */
	public function delete($id)
	{
		if (!is_numeric($id)) {
			$this->session->set_flashdata('error', 'ID data must numeric');
		}

		$paketliburan = $this->paketliburan_model->get($id);
		if (!$paketliburan) {
			$this->session->set_flashdata('error', 'Data not found');
		}

		if ($this->paketliburan_model->delete($id)) {

			$gallery = $this->gallery_model->get_all(array('destination_id' => $id));
			foreach ($gallery as $image) {
				@unlink(getenv('UPLOAD_DIR').$image->photo);
			}

			$this->paketliburan_model->delete_all_destination_attribute($id);

			$this->session->set_flashdata('success', 'Delete data success');
		} else {
			$this->session->set_flashdata('error', 'Delete data failed');
		}

		redirect('paketliburan');

	}

	/**
	 * Get gallery delegate
	 * @param  int $gallery_id id of gallery
	 * @return json
	 */
	public function gallerydelete($gallery_id) 
	{
		$record = $this->gallery_model->get($gallery_id);
		if (!$record) {
			die('no record found');
		}

		if ($this->gallery_model->delete($gallery_id)) {
			@unlink(getenv('UPLOAD_DIR').$record->photo);	
		}

		echo $this->general->json_encode(array(
			'csrfName' => $this->security->get_csrf_token_name(),
			'csrfHash' => $this->security->get_csrf_hash())
		);
	}

	public function reorder() {
		if ($this->input->post()) {
			$json['csrfName'] = $this->security->get_csrf_token_name();
			$json['csrfHash'] = $this->security->get_csrf_hash();
			if (isset($_POST['prev_order']) && isset($_POST['selected_order']) && isset($_POST['destination_id'])) {
				$prev_order = $this->input->post('prev_order');
				$selected_order = $this->input->post('selected_order');
				$destination_id = $this->input->post('destination_id');
				
				$this->paketliburan_model->update(array('order1_number' => $prev_order), array('order_number' => $selected_order));
				$this->paketliburan_model->update(array('order_number' => $selected_order), array('id' => $destination_id));
			}
			$this->general->json_encode($json);
		}
		
	}

}
