<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminmember extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('datatable', array('model' => 'adminmember_model', 'rowIdCol' => 'id'));
		$this->load->model(array('adminmember_model'));
	}

	/**
	 * index list
	 * @return void
	 */
	public function index()
	{
		$data = array(
			'content' => 'index',
		);
		$this->load->view('layouts/base', $data);
	}

	/**
	 * index list data
	 * @return json
	 */
	public function index_data()
	{
		$json = $this->datatable->datatableJson();
		$json['csrfName'] = $this->security->get_csrf_token_name();
		$json['csrfHash'] = $this->security->get_csrf_hash();
		$this->general->json_encode($json);
	}

	/**
	 * add new adminmember
	 */
	public function add()
	{

		if ($this->input->post()) {

			$id = $this->adminmember_model->from_form()->insert();
			if ($id) {
				$this->session->set_flashdata('success', 'Data success save');
				redirect('adminmember');
			}
		}

		$data = array(
			'content' => 'add',
		);
		$this->load->view('layouts/base', $data);
	}

	/**
	 * edit adminmember
	 * @param  int $id adminmember id
	 * @return void
	 */
	public function edit($id)
	{
		
		if (!is_numeric($id)) {
			$this->session->set_flashdata('error', 'ID data must numeric');
			redirect('adminmember');
		}

		$record = $this->adminmember_model->get($id);
		
		if (!$record) {
			$this->session->set_flashdata('error', 'Data not found');
			redirect('adminmember');
		}

		if ($this->input->post()) {

			//update data
			$data_update = $this->input->post();
			$result = $this->adminmember_model->update($data_update, $id);
			
			if ($result) {

				$this->session->set_flashdata('success', 'Update data success');				
				redirect('adminmember');
			}
		}

		$data = array(
			'record' => $record,
			'content' => 'edit',
		);

		$this->load->view('layouts/base', $data);
	}

	/**
	 * delete adminmember
	 * @param  int $id adminmember id
	 * @return void
	 */
	public function delete($id)
	{
		if (!is_numeric($id)) {
			$this->session->set_flashdata('error', 'ID data must numeric');
		}

		$adminmember = $this->adminmember_model->get($id);
		if (!$adminmember) {
			$this->session->set_flashdata('error', 'Data not found');
		}

		if ($this->adminmember_model->delete($id)) {
			$this->session->set_flashdata('success', 'Delete data success');
		} else {
			$this->session->set_flashdata('error', 'Delete data failed');
		}

		redirect('adminmember');

	}

	public function change_password() {
		if (!$this->acl->logged_in()) {
			redirect('login');
		}
		$record = $this->adminmember_model->get($this->session->userdata('id'));
		
		if (!$record) {
			$this->session->set_flashdata('error', 'Data not found');
			redirect('change_password');
		}

		if ($this->input->post()) {

			//update data
			$data_update = $this->input->post();
			$result = $this->adminmember_model->update($data_update, $this->session->userdata('id'));
			
			if ($result) {

				$this->session->set_flashdata('success', 'Change password success');				
				redirect('change_password');
			}
		}

		$data = array(
			'record' => $record,
			'content' => 'change_password',
		);

		$this->load->view('layouts/base', $data);
	}

	/**
	 * [login description]
	 * @return [type] [description]
	 */
	public function login()
	{

		if ($this->input->post()) {
			
			$this->acl->login($this->input->post(getenv('IDENTITY_FIELD')), $this->input->post('password'), FALSE);
		}

		if ($this->acl->logged_in()) {
			redirect('/');
		}

		$this->load->view('login');
	}

	public function logout()
	{
		$this->acl->logout();
		redirect('login');
	}

}
