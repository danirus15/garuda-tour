<section class="scrollable padder">              
  <section class="row m-b-md">
    <div class="col-sm-6">
      <h3 class="m-b-xs text-black">Change Password</h3>
    </div>
  </section>
  <!-- s:content --> 
  <section class="panel panel-default">
    <header class="panel-heading font-bold">
      <?php echo $this->load->view('partials/flash_messages')?>
    </header>
    <div class="panel-body">
      <?php echo form_open('change_password', array('class'=>'form-horizontal'))?>
        <div class="form-group" <?php echo (form_error('name')) ? 'has-error' : '' ?>>
          <label class="col-sm-2 control-label">Nama</label>
          <div class="col-sm-10">
            <?php echo $record->name ?>
          </div>
        </div>
        <div class="line line-dashed b-b line-lg pull-in"></div>
        <div class="form-group" <?php echo (form_error('email')) ? 'has-error' : '' ?>>
          <label class="col-sm-2 control-label">Email</label>
          <div class="col-sm-10">
            <?php echo $record->email ?>
          </div>
        </div>
        <div class="line line-dashed b-b line-lg pull-in"></div>
        <div class="form-group" <?php echo (form_error('email')) ? 'has-error' : '' ?>>
          <label class="col-sm-2 control-label">Password</label>
          <div class="col-sm-10">
            <?php echo form_password('password','', array('class'=>'form-control', 'placeholder' => 'Password'))?>
            <?php echo form_error('password');?>
          </div>
        </div>
        <div class="line line-dashed b-b line-lg pull-in"></div>

        
        <div class="form-group">
          <div class="col-sm-4 col-sm-offset-2">
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </section>
  <!-- e:content -->
  <div class="clearfix"></div>
  

</section>