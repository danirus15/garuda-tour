<!DOCTYPE html>
<html lang="en" class="app">
<head>  
  <meta charset="utf-8" />
  <title>Ayo Liburan - Garuda Indonesia</title>
  <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
  <link rel="stylesheet" href="<?php echo assets_url('css/bootstrap.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo assets_url('css/animate.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo assets_url('css/font-awesome.min.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo assets_url('css/icon.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo assets_url('css/font.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo assets_url('css/app.css')?>" type="text/css" />  
    <!--[if lt IE 9]>
    <script src="<?php echo assets_url('js/ie/html5shiv.js')?>"></script>
    <script src="<?php echo assets_url('js/ie/respond.min.js')?>"></script>
    <script src="<?php echo assets_url('js/ie/excanvas.js')?>"></script>
  <![endif]-->
</head>
<body class="">
  <section id="content" class="m-t-lg wrapper-md animated fadeInUp">    
    <div class="container aside-xl">
      <div align="center">
        <a class="logo_login" href="<?php echo site_url('/')?>">
          <img src="<?php echo assets_url('images/logo_ayo.png')?>" height="150" alt="">
        </a>
      </div>
      <br>
      <section class="m-b-lg">
        <?php echo form_open(site_url('login'))?>
          <div class="list-group">
            <div class="list-group-item">
              <input name="<?php echo getenv('IDENTITY_FIELD')?>" type="text" class="form-control no-border" placeholder="<?php echo ucfirst(getenv('IDENTITY_FIELD'))?>">
            </div>
            <div class="list-group-item">
               <input name="password" type="password" class="form-control no-border" placeholder="Password">
            </div>
          </div>
          <button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
        </form>
      </section>
      <div class="logo_login_foot" align="center">
        <img src="<?php echo assets_url('images/logo2.png')?>" alt="" height="30"><br><br>
        <div style="font-size: 11px">Copyright of Garuda Indonesia @2017 - Allright reserved</div>

      </div>
    </div>
  </section>
  <!-- footer -->
  <script src="<?php echo assets_url('js/jquery.min.js')?>"></script>
  <!-- Bootstrap -->
  <script src="<?php echo assets_url('js/bootstrap.js')?>"></script>
  <!-- App -->
  <script src="<?php echo assets_url('js/app.js')?>"></script>  
  <script src="<?php echo assets_url('js/slimscroll/jquery.slimscroll.min.js')?>"></script>
  <script src="<?php echo assets_url('js/app.plugin.js')?>"></script>
  <!-- / footer -->
  
</body>
</html>