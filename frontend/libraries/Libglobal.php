<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Libglobal{
	public function __construct()
    {
		$this->CI = &get_instance();
	}

	/**
	 * conver array to json format
	 * @param  array $data array data
	 * @return string       json encode
	 */
	public function jsonEncode($data) {
		header('Access-Control-Allow-Origin: *');
    	header("Content-Type: application/json");
    	return json_encode($data);
	}

	/**
	 * get total day in month
	 * @param  int $month month
	 * @param  int $year  year
	 * @return int        total day in month
	 */
	public function dayInMonth($month, $year) {
		return cal_days_in_month(CAL_GREGORIAN, $month, $year);
	}

	public function monthString($index) {
		if ($index < 1 || $index > 12) {
			return "None";
		}
		
		$month = array(
			1 => "Januari", 
			2 => "Februari", 
			3 => "Maret", 
			4 => "April", 
			5 => "Mei", 
			6 => "Juni", 
			7 => "Juli", 
			8 => "Agustus", 
			9 => "September", 
			10 => "Oktober", 
			11 => "November", 
			12 => "Desember"
		);

		return $month[$index];
	}

	public function code() 
	{
		while (true) {
			$string = $this->generate();
			$check = $this->CI->modglobal->find('order', array('code' => $string));
			if (!$check->num_rows()) {
				break;
			}
		}

		return $string;
	}

	public function generate($length=5)
	{
		$random_string_length = $length;
		$characters = 'abcdefghijklmnopqrstuvwxyz01234567890';
		$string = '';
		$max = strlen($characters) - 1;
		for ($i = 0; $i < $random_string_length; $i++) {
		    $string .= $characters[mt_rand(0, $max)];
		}

		return $string.getenv('PREFIX_CODE');
	}

	/**
	 * validate order
	 * @return list
	 */
	public function validateOrder() 
	{
		if (empty($_SERVER['QUERY_STRING'])) {
			redirect('/');
		}

		if (!$this->CI->session->userdata('logged_in')) {
			redirect('login?callback='.urlencode(uri_string().'?'.$_SERVER['QUERY_STRING']));
		}

		$destination_id = $this->CI->input->get('destination_id');
		$city_id = $this->CI->input->get('city_id');
		$schedule_id = $this->CI->input->get('schedule_id');
		$quantity = $this->CI->input->get('quantity');

		if (!is_numeric($destination_id) && !is_numeric($city_id) && !is_numeric($quantity) && !is_numeric($schedule_id)) {
			redirect('/');
		}

		return array($destination_id, $city_id, $schedule_id, $quantity);

	}

	/**
	 * get data order
	 * @return list
	 */
	public function getDataOrder() 
	{

		$destination_id = $this->CI->input->get('destination_id');
		$city_id = $this->CI->input->get('city_id');
		$schedule_id = $this->CI->input->get('schedule_id');

		$destination = $this->CI->modglobal->find('destination', array('id' => $destination_id));
		if (!$destination->num_rows()) {
			show_error('Destination not found');
		}

		$schedule = $this->CI->modglobal->find('schedule', array('id' => $schedule_id, 'destination_id' => $destination_id, 'status' => '1'));
		if (!$schedule->num_rows()) {
			show_error('Schedule not found');
		}

		$schedule_price = $this->CI->modglobal->find('schedule_price', array('schedule_id' => $schedule_id, 'city_id' => $city_id));
		if (!$schedule_price->num_rows()) {
			show_error('Price not found');
		}

		return array($destination, $schedule, $schedule_price);
	}

}