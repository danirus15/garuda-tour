<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
   	{
        parent::__construct();
        
        if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
   	}

   	/**
   	 * dashboard page
   	 * @return void
   	 */
	public function index()
	{
    $order = $this->modglobal->find('order', array('user_id' => $this->session->userdata('id')), 'created_at DESC', array(), 8, 0);
		$data = array(
			'content' => 'dashboard',
			'order' => $order,
		);
    
		$this->load->view('layouts/base', $data);
	}
}
