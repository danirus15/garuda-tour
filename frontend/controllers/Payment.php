<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Controller {
	public function __construct()
    {
		parent::__construct();
		$this->load->library('midtrans');

		
	}

	/**
	 * payment form
	 * @return void
	 */
	public function index() 
	{

		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }

		if (!$this->input->get('code')) {
			show_404();
		}

		$code = $this->input->get('code');
		$order_check = $this->modglobal->find('order', array(
				'code' => $code, 
				'user_id' => $this->session->userdata('id')
			)
		);

		if (!$order_check->num_rows()) {
			show_404();
		}

		$order = $order_check->row();
		$order_detail = $this->modglobal->find('order_detail', array('order_id' => $order->id));
		$destination = $this->modglobal->find('destination', array('id' => $order->destination_id));
		$schedule = $this->modglobal->find('schedule', array('id' => $order->schedule_id));
		$schedule_price = $this->modglobal->find('schedule_price', array('schedule_id' => $order->schedule_id, 'city_id' => $order->city_id));

		if ($this->input->post()) {

			if ($this->input->post('card_type')) {
				$cashback = 0;
				if ($this->input->post('card_type') == 'TITANIUM') {
					$cashback = getenv('MIDTRANS_BINS_PLATINUM_CASHBACK');
				} elseif ($this->input->post('card_type') == 'MASTERCARD') {
					$cashback = getenv('MIDTRANS_BINS_NONE_PLATINUM_CASHBACK');
				} elseif ($this->input->post('card_type') == 'VISA') {
					$cashback = getenv('MIDTRANS_BINS_BNI_JBC_VISA_CASHBACK');
				}
				$this->modglobal->update('order', array(
					'cashback' => $cashback,
					'installment' => $this->input->post('installment'),
					'card_type' => $this->input->post('card_type')
				), array('id' => $order->id));

				redirect('payment/confirmation?code='.$order->code);
			} else {
				$this->session->set_flashdata('error', 'Please select bni card type');
			}

		} else {
			
		}

		$data = array(
			'content' => 'payment',
			'destination' => $destination->row_array(),
			'schedule' => $schedule->row(),
			'schedule_price' => $schedule_price->row(),
			'order' => $order,
			'order_detail' => $order_detail->result_array(),
		);
		
		$this->load->view('layouts/base', $data);
	}

	public function confirmation() 
	{
		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }

		if (!$this->input->get('code')) {
			show_404();
		}

		$code = $this->input->get('code');
		$order_check = $this->modglobal->find('order', array(
				'code' => $code, 
				'user_id' => $this->session->userdata('id')
			)
		);

		if (!$order_check->num_rows()) {
			show_404();
		}

		$order = $order_check->row();
		$order_detail = $this->modglobal->find('order_detail', array('order_id' => $order->id));
		$destination = $this->modglobal->find('destination', array('id' => $order->destination_id));
		$schedule = $this->modglobal->find('schedule', array('id' => $order->schedule_id));
		$schedule_price = $this->modglobal->find('schedule_price', array('schedule_id' => $order->schedule_id, 'city_id' => $order->city_id));
		$response = $this->processPayment($order, $order_detail, $destination, $schedule, $schedule_price);
		
		if (isset($response->token)) {
			$snap_token = $response->token;
		} else {

			if (is_array($response->error_messages)) {
				foreach ($response->error_messages as $message) {
					$messages .= ($messages) ? $message : '<br>'.$message ;
				}
			} else {
				$messages = $response->error_messages;
			}
			$this->session->set_flashdata('error', $messages);
		  	redirect('payment/fail');
		}


		$data = array(
			'content' => 'payment_confirmation',
			'destination' => $destination->row_array(),
			'schedule' => $schedule->row(),
			'schedule_price' => $schedule_price->row(),
			'order' => $order,
			'order_detail' => $order_detail->result_array(),
			'snap_token' => $snap_token,
		);

		$this->load->view('layouts/base', $data);
	}

	private function processPayment($order, $order_detail, $destination, $schedule, $schedule_price) 
	{
		$this->midtrans->config(array(
			'server_key' => getenv('MIDTRANS_SERVER_KEY'),
			'production' => (getenv('MIDTRANS_IS_PRODUCTION') == '1') ? true : false
		));

		if ($order->card_type == 'TITANIUM') { 
			$list_bins = explode(",",getenv('MIDTRANS_BINS_PLATINUM_CODE')); //platinum
		} elseif ($order->card_type == 'MASTERCARD') { 
			$list_bins = explode(",",getenv('MIDTRANS_BINS_NONE_PLATINUM_CODE')); //none platinum
		} else { 
			$list_bins = explode(",",getenv('MIDTRANS_BINS_BNI_JBC_VISA_CODE')); //visa
		}

		$order_id = $order->id;
		$installment = $this->input->post('installment');

		$quantity = $order->qty_promo + $order->qty;

		$total = (($order->qty_promo * $order->price_promo) + ($order->qty * $order->price) + $order->suplement) - ($quantity * $order->cashback);
		$transaction_details = array(
			'order_id' => $order_id,
			'gross_amount' => $total
		);

		$credit_card = array(
			'bank' => 'bni',
			'whitelist_bins' => $list_bins,
		);

		if ($order->installment) {
			$credit_card['installment'] =  array(
				'required' => true,
				'terms' => array(
					'bni' => array((int)$order->installment)
				),
			);
		}

		if (getenv('MIDTRANS_IS_3DS') == '1') {
			$credit_card['secure'] = true;
		}

		$item_details = ($order->suplement) ? array(array(
			'id' => '00',
			'price' => $order->suplement,
			'quantity' => 1,
			'name' => 'Suplement',
			'category' => 'Normal price' 
		)) : array();
		foreach ($order_detail->result_array() as $value) {
			array_push($item_details, array(
				'id' => $value['id'],
				'price' => ($value['price'] - $order->cashback),
				'quantity' => 1,
				'name' => $value['first_name'].' '.$value['last_name'],
				'category' => ($value['is_promo'] == '1') ? 'Promo price' : 'Normal price' 
			));
		}

		$customer_details = array(
			'first_name' => $this->session->userdata('name'),
			'email' => $this->session->userdata('email'),
			// 'billing_address' => array(
			// 	'first_name' => $this->input->post('name'),
			// 	'email' => $this->input->post('email'),
			// 	'phone' => $this->input->post('phone')
			// )
		);

		$transaction_data = array(
			'enabled_payments' => array("credit_card"),
			'credit_card' => $credit_card,
			'transaction_details' => $transaction_details,
			'item_details' => $item_details,
			'customer_details' => $customer_details,
		);
		// dd($transaction_data);

		return $this->midtrans->getSnapToken($transaction_data);
	}


	private function send_email_order($data) {
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->load->helper('file');
		$email_template = read_file('order.html');

		$email_body = str_replace(
			array(
				'{logo}',
				'{name}',
				'{pdf_url}',
				'{year}'
			), 
			array(
				base_url('assets/mail/logo.png'),
				ucfirst($data['name']),
				$data['pdf_url'],
				date('Y')
			), 
			$email_template
		);

		$this->email->from(getenv('ADMIN_EMAIL'), getenv('ADMIN_EMAIL_NAME'));
		$this->email->to($data['email']);
		$this->email->bcc('ayoliburan@garuda-indonesia.com,ayoliburangaruda@gmail.com');
		$this->email->subject('AyoLiburan - Order Confirmation');
		$this->email->message($email_body);
		return $this->email->send();
	}

	private function getOrder($order_id)
	{
		
		if (!is_numeric($order_id)) {
			show_404();
		}

		$order = $this->modglobal->find('order', array('id' => $order_id, 'user_id' => $this->session->userdata('id'), 'status' => '1'));

		if (!$order->num_rows()) {
			show_404();
		}

		$order_detail = $this->modglobal->find('order_detail', array('order_id' => $order_id));

		if (!$order_detail->num_rows()) {
			show_404();
		}

		return array($order, $order_detail);
	}

	public function status() 
	{
		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }

		if ($this->input->post()) {
			$order_id = $this->input->post('order_id');
			if (!is_numeric($order_id)) {
				show_404();
			}

			$this->modglobal->update('order', array('status' => '1'), array(
				'id' => $order_id,
				'user_id' => $this->session->userdata('id')
			));

			$this->send_email_order(array(
				'name' => $this->session->userdata('name'),
				'email' => $this->session->userdata('email'),
				'pdf_url' => site_url('payment/pdf/'.$order_id)
			));
		}
	}

	public function success()
	{
		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }

		$order_id = $this->input->get('order_id');

		if (!is_numeric($order_id)) {
			show_404();
		}

		list($order, $order_detail) = $this->getOrder($order_id);

		$data = array(
			'content' => 'payment_success',
			'order' => $order->row(),
			'order_detail' => $order_detail->result_array()
		);
		
		$this->load->view('layouts/base', $data);
	}

	public function fail() 
	{
		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }

		$data = array(
			'content' => 'payment_fail'
		);
		$this->load->view('layouts/base', $data);	
	}

	public function printing($order_id)
	{
		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }

		if (!is_numeric($order_id)) {
			show_404();
		}

		list($order, $order_detail) = $this->getOrder($order_id);

		$data = array(
			'order' => $order->row(),
			'order_detail' => $order_detail->result_array()
		);

		$this->load->view('payment_print', $data);	
	}

	public function pdf($order_id)
	{
		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }

		if (!is_numeric($order_id)) {
			show_404();
		}

		list($order, $order_detail) = $this->getOrder($order_id);
		$this->load->library('pdfgenerator');
		$data = array(
			'order' => $order->row(),
			'order_detail' => $order_detail->result_array()
		);
 
	    $html = $this->load->view('payment_pdf', $data, true);
	    // dd($html);
	    $this->pdfgenerator->generate($html,'contoh');
	    // $this->load->view('payment_pdf', $data);
	}

	public function notification()
	{
		// echo "notification";
		$inputJSON = file_get_contents('php://input');
		$input = json_decode($inputJSON, TRUE);
		// dd($input);
	}

	public function finish()
	{
		echo "finish";
	}

	public function unfinish()
	{
		echo "unfinish";
	}

	public function error()
	{
		echo "error";
	}
}
