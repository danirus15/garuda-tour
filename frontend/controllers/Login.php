<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct()
   	{
        parent::__construct();
        $this->table = "user";
   	}

   	/**
   	 * login page
   	 * @return void
   	 */
	public function index()
	{
		$callback = ($this->input->get('callback')) ? '?callback='.urlencode($this->input->get('callback')) : '';
		if ($this->input->post()) {
			$this->set_validation_form_login();  
			if ($this->form_validation->run() == TRUE) {
				$data = $this->collect_data_form_login();
				$check_user = $this->modglobal->find('user', array('email' => $data['email'], 'status' => '1'));
				if ($check_user->num_rows()) {
					$user = $check_user->row();
					if (password_verify($data['password'], $user->password)) {
						$session_data = array(
							'id' => $user->id,
							'name' => $user->name,
							'email' => $user->email,
							'logged_in' => 1,
						);
						$this->session->set_userdata($session_data);
						
						if ($this->input->get('callback')) {
							redirect(site_url($this->input->get('callback')));
						} else {
							redirect('/');
						}

					} else {
						$this->session->set_flashdata('error', 'Wrong email or password');
						redirect('login'.$callback);	
					}
				} else {
					$this->session->set_flashdata('error', 'User not found');
					redirect('login'.$callback);
				}
	            
			} 

		}

		$data = array(
			'callback' => $callback
		);

		$this->load->view('login', $data);
	}

	/**
	 * validation form login
	 * @param void
	 */
	private function set_validation_form_login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');
    }

    /**
     * collect data form login
     * @return array
     */
    private function collect_data_form_login()
    {
        $output = array(
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password'),
        );
        return $output;
    }

    /**
     * logout
     * @return void
     */
    public function logout() {
    	$session_data = array(
    		'id',
    		'name',
    		'logged_in'
    	);
    	$this->session->unset_userdata($session_data);
		redirect('/');
    }

    /**
     * activation page
     * @param  string $code code ativation
     * @return void
     */
	public function activation($code) 
	{
		$data = array(
			'title' => 'Aktivasi Akun ',
			'message' => '<br><br>Akunmu telah berhasil di aktivasi',
			'success' => false,
			'content' => 'activation'
		);

		$user = $this->modglobal->find('user', array('status' => '0', 'code' => $code));
		
		if ($user->num_rows()) {

			$data_update = array(
				'code' => '',
				'status' => '1'
			);

			if ($this->modglobal->update('user', $data_update, array('status' => '0', 'code' => $code))) {
				$data['title'] = 'Aktivasi Akun Berhasil';
			}
		}

		$this->load->view('layouts/base', $data);
	}
	
	/**
	 * register user
	 * @return void
	 */
	public function register()
	{
		if ($this->input->post()) {
			$this->set_validation_form_register();  
			if ($this->form_validation->run() == TRUE) {
				$data = $this->collect_data_form_register();
				$check_email = $this->modglobal->find('user', array('email' => $data['email']));
				if ($check_email->num_rows()) {
					$this->session->set_flashdata('error', 'Email already exist');
				} else {
					if($this->modglobal->insert($this->table, $data)){
						//send email activation
						$this->send_email_activation($data);
		                $this->session->set_flashdata('success', 'Save data success');

		                redirect('register_success');

		            }else{
		                $this->session->set_flashdata('error', 'Register failed');
		                redirect('register');
		            }
				}
	            
			}
			
		}

		$this->load->view('register');
	}

	/**
	 * send email activation 
	 * @param  array $data data post user
	 * @return bool
	 */
	public function send_email_activation($data) {
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->load->helper('file');
		$email_template = read_file('mail.html');

		$link_actiovation = site_url('a/'.$data['code']);
		//$link_actiovation = 'ayoliburan.garuda-indonesia.com/activation/'.$data['code'];

		$email_body = str_replace(
			array(
				'{logo}',
				'{name}',
				'{activation_url}',
				'{year}'
			), 
			array(
				base_url('assets/mail/logo.png'),
				ucfirst($data['name']),
				$link_actiovation,
				date('Y')
			), 
			$email_template
		);

		$this->email->from(getenv('ADMIN_EMAIL'), getenv('ADMIN_EMAIL_NAME'));
		$this->email->to($this->input->post('email'));
		$this->email->subject('AyoLiburan - activated your account');
		
		$this->email->message($email_body);
		return $this->email->send();
	}

	/**
	 * register success page
	 * @return void
	 */
	public function register_success() 
	{
		$this->load->view('register_success');
	}

	/**
	 * validation form register
	 * @param void
	 */
	private function set_validation_form_register()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');
    }

    /**
     * collect data form register
     * @return array
     */
    private function collect_data_form_register()
    {
        $output = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
            'code' => md5($this->input->post('name').$this->input->post('email').date('Y-m-d H:i:s')),
            'created_at' => date('Y-m-d h:i:s'),
            'created_by' => $this->input->post('name'),
        );
        return $output;
    }

    /**
     * forgot password page
     * @return void
     */
	public function forgotpassword()
	{

		if ($this->input->post()) {
			$this->load->library('form_validation');
        	$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			if ($this->form_validation->run() == TRUE) {

				$check_email = $this->modglobal->find('user', array('email' => $this->input->post('email')));
				if ($check_email->num_rows()) {
					$user = $check_email->row();
					
					$password = $this->libglobal->generate(4);
					$password_hash = password_hash($password, PASSWORD_BCRYPT);

					if ($this->modglobal->update('user', array('password' => $password_hash), array('id' => $user->id))) {
						$this->send_email_reset_password(array(
							'name' => $user->name,
							'password' => $password
						));

		                $this->session->set_flashdata('success', 'Reset password success');

		                redirect('forgotpassword_success');
					} else {
						$this->session->set_flashdata('error', 'Reset password failed');
		                redirect('forgot_password');
					}

				} else {
					$this->session->set_flashdata('error', 'Email not found');
				}
	            
			}
			
		}

		$this->load->view('forgotpassword');
	}

	/**
	 * register success page
	 * @return void
	 */
	public function forgotpassword_success() 
	{
		$this->load->view('forgotpassword_success');
	}

	/**
	 * send email activation 
	 * @param  array $data data post user
	 * @return bool
	 */
	public function send_email_reset_password($data) {
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->load->helper('file');
		$email_template = read_file('reset_password_mail.html');

		$email_body = str_replace(
			array(
				'{logo}',
				'{name}',
				'{password}',
				'{year}'
			), 
			array(
				base_url('assets/mail/logo.png'),
				ucfirst($data['name']),
				$data['password'],
				date('Y')
			), 
			$email_template
		);

		$this->email->from(getenv('ADMIN_EMAIL'), getenv('ADMIN_EMAIL_NAME'));
		$this->email->to($this->input->post('email'));
		// $this->email->to('awalletter@gmail.com');
		$this->email->subject('AyoLiburan - Forgot Password');
		
		$this->email->message($email_body);
		return $this->email->send();
	}

	/**
	 * edit profile
	 * @return void
	 */
	public function edit() 
	{
		if (!$this->session->userdata('logged_in')) {
			redirect('login');
		}

		$query = $this->modglobal->find('user', array('id' => $this->session->userdata('id'), 'status' => '1'));

		$user = $query->row();

		if (!$query->num_rows()) {
			redirect('/');
		}

		if ($this->input->post()) {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('name', 'Name', 'required');
        	$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        	if ($this->form_validation->run() == TRUE) {

        		if ($this->input->post('email') != $user->email) {
        			$query_check_email = $this->modglobal->find('user', array('email' => $this->input->post('email')));
        			if (!$query_check_email->num_rows()) {
        				$this->update_profile();
        				$this->session->set_flashdata('success', 'Edit data success');
        				redirect('profile_success');
        			} else {
        				$this->session->set_flashdata('error', 'Email sudah terdaftar');
        				redirect('profile');
        			}
        		} else {
        			$this->update_profile();
        			$this->session->set_flashdata('success', 'Edit data success');
        			redirect('profile_success');
        		}
        	}

		}

		$data = array(
			'row' => $user
		);

		$this->load->view('edit_profile', $data);
	}

	/**
	 * edit profile success page
	 * @return void 
	 */
	public function edit_success()
	{
		$this->load->view('edit_profile_success');
	}

	/**
	 * update profile to database
	 * @return void
	 */
	private function update_profile() 
	{
		$this->modglobal->update('user', array(
        			'name' => $this->input->post('name'),
        			'email' => $this->input->post('email'),
        			'mobile' => $this->input->post('mobile'),
        			'phone' => $this->input->post('phone'),
        		), array('id' => $this->session->userdata('id')));
	}

	/**
	 * edit profile
	 * @return void
	 */
	public function change_password() 
	{
		if (!$this->session->userdata('logged_in')) {
			redirect('login');
		}

		$query = $this->modglobal->find('user', array('id' => $this->session->userdata('id'), 'status' => '1'));

		$user = $query->row();

		if (!$query->num_rows()) {
			redirect('/');
		}

		if ($this->input->post()) {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('current_password', $this->lang->line('current_password'), 'required');
			$this->form_validation->set_rules('new_password', $this->lang->line('new_password'), 'required');
			$this->form_validation->set_rules('re_password', $this->lang->line('re_password'), 'required|matches[new_password]');
        	if ($this->form_validation->run() == TRUE) {
				$check_user = $this->modglobal->find('user', array('id' => $this->session->userdata('id')));
				if ($check_user->num_rows()) {
					$user = $check_user->row();
					if (password_verify($this->input->post('current_password'), $user->password)) {
						$this->modglobal->update('user', array(
							'password' => password_hash($this->input->post('new_password'), PASSWORD_BCRYPT),
						), array('id' => $this->session->userdata('id')));
						redirect('change_password_success');
					} else {
						$this->session->set_flashdata('error', 'Current password not match');
						redirect('change_password');
					}
				} else {
					$this->session->set_flashdata('error', 'Data not found');
					redirect('change_password');
				}
        	}

		}

		$data = array(
			'row' => $user
		);

		$this->load->view('change_password', $data);
	}

	/**
	 * edit profile success page
	 * @return void 
	 */
	public function change_password_success()
	{
		$this->load->view('change_password_success');
	}
}
