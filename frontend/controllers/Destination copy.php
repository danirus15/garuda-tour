<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Destination extends MY_Controller {
	public function __construct()
    {
		parent::__construct();
	}

	/**
	 * home page
	 * @return void
	 */
	public function index()
	{

		redirect('/');
	}

	/**
	 * detail destination
	 * @param  int $id    id destination
	 * @param  string $title name of destination
	 * @return void
	 */
	public function detail($id, $title='') 
	{
		if (!is_numeric($id)) {
			redirect('/');
		}

		$destination = $this->modglobal->find('destination', array('id' => $id));

		if (!$destination->num_rows()) {
			redirect('/');
		}

		$this->modglobal->setExpireOrder();

		$gallery = $this->modglobal->find('gallery', array('destination_id' => $id));

		$schedules = $this->modglobal->find('schedule', array('destination_id' => $id, 'status' => '1', 'end_date_order >=' => date('Y-m-d')));
		$cities = $this->modglobal->find('cities', array('status' => '1'));
		$data = array(
			'content' => 'detail',
			'destination' => $destination->row_array(),
			'gallery' => $gallery->result_array(),
			'schedules' => $schedules->result_array(),
			'cities' => $cities->result_array(),
		);
		
		$this->load->view('layouts/base', $data);
	}

	/**
	 * get schedule for form order
	 * @param int $destination_id post data type int destination id
	 * @param int $city_id post data type int city id
	 * @return json
	 */
	public function schedules() 
	{
		$destination_id = $this->input->post('destination_id');
		$city_id = $this->input->post('city_id');

		if (!is_numeric($destination_id) && !is_numeric($city_id)) {
			die('Please input integer type'); 
		}

		$destination = $this->modglobal->find('destination', array('id' => $destination_id));
		if (!$destination->num_rows()) {
			die('Destination not found');
		}

		$city = $this->modglobal->find('cities', array('id' => $city_id, 'status' => '1'));
		if (!$city->num_rows()) {
			die('City not found');
		}

		$schedules = $this->modglobal->getScheduleCity($destination_id, $city_id);
		if (!$schedules->num_rows()) {
			die('Schedule not found');
		}

		$data = array();
		foreach ($schedules->result_array() as $schedule) {

			$taken_seat = $this->modglobal->getTotalSeatOrdered($schedule['id'], $city_id);

			$promo_seat_avaliable = $schedule['promo_seat_max'] - $taken_seat;
			$total_seat_all_avaliable = ($schedule['total_seat'] - $schedule['promo_seat_max']);
			$total_seat_normal_avaliable = (($schedule['promo_seat_max'] - $taken_seat) < 0) ? $total_seat_all_avaliable + ($schedule['promo_seat_max'] - $taken_seat) : $total_seat_all_avaliable ;

			array_push($data, array(
				'id' => $schedule['id'],
				'depart_date' => date('d M Y', strtotime($schedule['depart_date'])),
				'return_date' => date('d M Y', strtotime($schedule['return_date'])),
				'total_seat' => $schedule['total_seat'],
				'promo_seat_avaliable' => ($promo_seat_avaliable > 0) ? $promo_seat_avaliable : 0,  
				'total_seat_normal_avaliable' => $total_seat_normal_avaliable,  
				'price' => $schedule['price'],
				'price_normal' => $schedule['price_normal'],
				'promo_seat_max' => $schedule['promo_seat_max'],
				'promo_seat_discount' => $schedule['promo_seat_discount'],
			));
		}

		echo $this->libglobal->jsonEncode(array('data' => $data));

	}

	/**
	 * order proses 
	 * @return void
	 */
	public function order() 
	{

		list($destination_id, $city_id, $schedule_id, $quantity) =  $this->libglobal->validateOrder();

		list($destination, $schedule, $schedule_price) = $this->libglobal->getDataOrder();

		if ($this->input->post()) {



			$code = $this->libglobal->code();
			$user_id = $this->session->userdata('id');
			$user_name = $this->session->userdata('name');
			$user_email = $this->session->userdata('email');

			$destination_result = $destination->row_array();
			$destination_id = $destination_result['id'];
			$destination_name = $destination_result['title_'.active_language()];
			
			$schedule_result = $schedule->row();
			$schedule_id = $schedule_result->id;
			$depart_date = $schedule_result->depart_date;
			$return_date = $schedule_result->return_date;

			$schedule_price_result = $schedule_price->row();
			$price = $schedule_price_result->price;
			$city_id = $schedule_price_result->city_id;
			$city = $schedule_price_result->city_name;

			$promo_seat_max = $schedule_price_result->promo_seat_max;
			$promo_seat_discount = $schedule_price_result->promo_seat_discount;

			$taken_seat = $this->modglobal->getTotalSeatOrdered($schedule_id, $city_id);

			$user_get_promo_seat = 0;
			$user_get_normal_seat = $quantity;

			$promo_seat_avaliable = $promo_seat_max - $taken_seat;
			// $total_seat_all_avaliable = $schedule_result->total_seat - $promo_seat_max;
			// $total_seat_normal_avaliable = (($promo_seat_max - $taken_seat) < 0) ? $total_seat_all_avaliable + ($promo_seat_max - $taken_seat) : $total_seat_all_avaliable;

			if ($promo_seat_avaliable > 0) {
				if ($promo_seat_avaliable >= $quantity) {
					$user_get_promo_seat = $quantity;
					$user_get_normal_seat = 0;
				} else {
					$user_get_promo_seat = $promo_seat_avaliable;
					$user_get_normal_seat = $quantity - $promo_seat_avaliable;
				}
			}


			$order = array(
				'code' => $code,
				'user_id' => $user_id,
				'user_name' => $user_name,
				'user_email' => $user_email,
				'contact_email' => $this->input->post('email_c'),
				'contact_phone' => $this->input->post('phone_c'),
				'contact_phone_bisnis' => $this->input->post('phone_bc'),
				'emergency_name' => $this->input->post('name_ec'),
				'emergency_phone' => $this->input->post('phone_ec'),
				'destination_id' => $destination_id,
				'destination_name' => $destination_name,
				'schedule_id' => $schedule_id,
				'depart_date' => $depart_date,
				'return_date' => $return_date,
				'qty_promo' => $user_get_promo_seat,
				'price_promo' => $promo_seat_discount,
				'qty' => $user_get_normal_seat,
				'price' => $price,
				'suplement' => ($quantity % 2 && $this->input->post('suplement') > 0) ? $this->input->post('suplement') : 0,
				'city_id' => $city_id,
				'city' => $city,
				'created_at' => date('Y-m-d H:i:s')
			);
			
			$tourist = '';
			$order_datails = array();

			for ($i=0; $i < $quantity; $i++) { 
				$title = $this->input->post('title')[$i];
				$first_name = $this->input->post('first_name')[$i];
				$last_name = $this->input->post('last_name')[$i];
				$id_number = $this->input->post('id_number')[$i];
				$email = $this->input->post('email')[$i];
				$phone = $this->input->post('phone')[$i];
				$birthdate = $this->input->post('birth_year')[$i].'-'.$this->input->post('birth_month')[$i].'-'.$this->input->post('birth_day')[$i];

				$order_detail = array(
					'title' => $title,
					'first_name' => $first_name,
					'last_name' => $last_name,
					'id_number' => $id_number,
					'email' => $email,
					'phone' => $phone,
					'birthdate' => $birthdate,
					'created_at' => date('Y-m-d H:i:s')
				);

				array_push($order_datails, $order_detail);
				//'.($i + 1).'
				$tourist .= '<tr>
								<td colspan="4">
								Nama: <b>'.$title.'. '.$first_name.' '.$last_name.'</b><br>
								Tanggal Lahir: <b>'.date('d M Y', strtotime($birthdate)).'</b><br>
								Email: <b>'.$email.'</b><br>
								No. Telepon: <b>'.$phone.'</b><br>
								ID Number: <b>'.$id_number.'</b><br>
								</td>
							</tr>';

			}

			$order['tourist'] = $tourist;
			$order['total_tourist'] = count($order_datails);
			$order['order_details'] = $order_datails;
			$order['total'] = ($order['qty_promo'] * $order['price_promo']) + ($order['qty'] * $order['price']) + $order['suplement'];
			$this->send_email_order_offline($order);

			$data = array(
				'content' => 'payment_offline',
				'order' => $order,
			);

		} else {
			$data = array(
				'content' => 'order',
				'destination' => $destination->row_array(),
				'schedule' => $schedule->row(),
				'schedule_price' => $schedule_price->row(),
				'quantity' => $quantity
			);	
		}

		
		
		$this->load->view('layouts/base', $data);
	}

	private function send_email_order_offline($data) {
		$this->load->library('email');
		$this->email->set_newline("\r\n");
		$this->load->helper('file');
		$email_template = read_file('order_offline.html');

		$email_body = str_replace(
			array(
				'{logo}',
				'{name}',
				'{destination_name}',
				'{code}',
				'{suplement}',
				'{depart_date}',
				'{return_date}',
				'{total_tourist}',
				'{total}',
				'{user_name}',
				'{user_email}',
				'{contact_phone}',
				'{tourist}',
				'{year}'
			), 
			array(
				base_url('assets/mail/logo.png'),
				ucfirst($data['user_name']),
				$data['destination_name'],
				strtoupper($data['code']),
				($data['suplement']) ? 'Yes' : 'No',
				$data['depart_date'],
				$data['return_date'],
				$data['total_tourist'],
				price_format($data['total']),
				$data['user_name'],
				$data['user_email'],
				$data['contact_phone'],
				$data['tourist'],
				date('Y')
			), 
			$email_template
		);
		
		$this->email->from(getenv('ADMIN_EMAIL'), getenv('ADMIN_EMAIL_NAME'));
		$this->email->to($data['user_email']);
		$this->email->bcc('ayoliburan@garuda-indonesia.com,helopiloindonesia@gmail.com,ayoliburangaruda@gmail.com');
		$this->email->subject('AyoLiburan - Order Confirmation');
		$this->email->message($email_body);

		return $this->email->send();
	}

	/**
	 * order proses 
	 * @return void
	 */
	public function order_online() 
	{

		list($destination_id, $city_id, $schedule_id, $quantity) =  $this->libglobal->validateOrder();

		list($destination, $schedule, $schedule_price) = $this->libglobal->getDataOrder();

		if ($this->input->post()) {



			$code = $this->libglobal->code();
			$user_id = $this->session->userdata('id');
			$user_name = $this->session->userdata('name');
			$user_email = $this->session->userdata('email');

			$destination_result = $destination->row_array();
			$destination_id = $destination_result['id'];
			$destination_name = $destination_result['title_'.active_language()];
			
			$schedule_result = $schedule->row();
			$schedule_id = $schedule_result->id;
			$depart_date = $schedule_result->depart_date;
			$return_date = $schedule_result->return_date;

			$schedule_price_result = $schedule_price->row();
			$price = $schedule_price_result->price;
			$city_id = $schedule_price_result->city_id;
			$city = $schedule_price_result->city_name;

			$promo_seat_max = $schedule_price_result->promo_seat_max;
			$promo_seat_discount = $schedule_price_result->promo_seat_discount;

			$taken_seat = $this->modglobal->getTotalSeatOrdered($schedule_id, $city_id);

			$user_get_promo_seat = 0;
			$user_get_normal_seat = $quantity;

			$promo_seat_avaliable = $promo_seat_max - $taken_seat;
			// $total_seat_all_avaliable = $schedule_result->total_seat - $promo_seat_max;
			// $total_seat_normal_avaliable = (($promo_seat_max - $taken_seat) < 0) ? $total_seat_all_avaliable + ($promo_seat_max - $taken_seat) : $total_seat_all_avaliable;

			if ($promo_seat_avaliable > 0) {
				if ($promo_seat_avaliable >= $quantity) {
					$user_get_promo_seat = $quantity;
					$user_get_normal_seat = 0;
				} else {
					$user_get_promo_seat = $promo_seat_avaliable;
					$user_get_normal_seat = $quantity - $promo_seat_avaliable;
				}
			}


			$order = array(
				'code' => $code,
				'user_id' => $user_id,
				'user_name' => $user_name,
				'user_email' => $user_email,
				'contact_email' => $this->input->post('email_c'),
				'contact_phone' => $this->input->post('phone_c'),
				'contact_phone_bisnis' => $this->input->post('phone_bc'),
				'emergency_name' => $this->input->post('name_ec'),
				'emergency_phone' => $this->input->post('phone_ec'),
				'destination_id' => $destination_id,
				'destination_name' => $destination_name,
				'schedule_id' => $schedule_id,
				'depart_date' => $depart_date,
				'return_date' => $return_date,
				'qty_promo' => $user_get_promo_seat,
				'price_promo' => $promo_seat_discount,
				'qty' => $user_get_normal_seat,
				'price' => $price,
				'suplement' => ($quantity % 2 && $this->input->post('suplement') > 0) ? $this->input->post('suplement') : 0,
				'city_id' => $city_id,
				'city' => $city,
				'created_at' => date('Y-m-d H:i:s')
			);
			
			if ($this->modglobal->insert('order', $order)) {
				$order_id = $this->modglobal->insert_id();

				$item_details = array();

				for ($i=0; $i < $quantity; $i++) { 
					$title = $this->input->post('title')[$i];
					$first_name = $this->input->post('first_name')[$i];
					$last_name = $this->input->post('last_name')[$i];
					$id_number = $this->input->post('id_number')[$i];
					$email = $this->input->post('email')[$i];
					$phone = $this->input->post('phone')[$i];
					$birthdate = $this->input->post('birth_year')[$i].'-'.$this->input->post('birth_month')[$i].'-'.$this->input->post('birth_day')[$i];

					$price_order = $price;
					$is_promo_order = '0';

					if ($i < $user_get_promo_seat) {
						$price_order = $promo_seat_discount;
						$is_promo_order = '1';
					}

					$order_detail = array(
						'order_id' => $order_id,
						'title' => $title,
						'first_name' => $first_name,
						'last_name' => $last_name,
						'id_number' => $id_number,
						'email' => $email,
						'phone' => $phone,
						'birthdate' => $birthdate,
						'price' => $price_order,
						'is_promo' => $is_promo_order,
						'created_at' => date('Y-m-d H:i:s')
					);

					$this->modglobal->insert('order_detail', $order_detail);

					$order_detail_id = $this->modglobal->insert_id();
					array_push($item_details, array(
						'id' => $order_detail_id,
						'name' => ucwords($title.'. '.$first_name.' '.$last_name),
						'quantity' => 1,
						'price' => $price,
					));

				}

				redirect('payment?code='.$code);
			}
		}

		$data = array(
			'content' => 'order',
			'destination' => $destination->row_array(),
			'schedule' => $schedule->row(),
			'schedule_price' => $schedule_price->row(),
			'quantity' => $quantity
		);
		
		$this->load->view('layouts/base', $data);
	}
}
