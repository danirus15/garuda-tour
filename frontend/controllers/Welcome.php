<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
	public function __construct()
    {
		parent::__construct();
	}

	public function under()
	{
		$this->load->view('maintenis');
	}

	/**
	 * home page
	 * @return void
	 */
	public function index()
	{

		$query_cover_images = $this->modglobal->find('cover_images', array('status' => '1'));
		$cover_images = ($query_cover_images->num_rows()) ? $query_cover_images->result_array() : [];

		$static_wording = array(
			'title' => '',
			'description' => ''
		);
		
		$query_static_word = $this->modglobal->find('static_content', array('type' => 'wording'));
		if ($query_static_word->num_rows()) {
			$row = $query_static_word->row_array();
			$static_wording['title'] = $row['title_'.active_language()];
			$static_wording['description'] = $row['content_'.active_language()];
		}

		$query_paketliburan = $this->modglobal->find('destination', array('status' => 1), 'order_number ASC');
		$paketliburan = $query_paketliburan->result_array();

		$data = array(
			'content' => 'home',
			'cover_images' => $cover_images,
			'static_wording' => $static_wording,
			'paketliburan' => $paketliburan,
		);
		
		$this->load->view('layouts/base', $data);
	}

	public function helpcenter() 
	{
		$data = array(
			'content' => 'helpcenter',
		);
		$this->load->view('layouts/base', $data);
	}


	public function language($lang) 
	{
		if ($lang) {
			$this->session->set_userdata(array(
				'lang' => $lang
			));
		}

		$callback = site_url($this->input->get('callback'));
		if ($callback) {
			redirect($callback);
		} else {
			redirect('/');
		}
	}

	public function getDestinationPrice($destination_id) 
	{
		if (!is_numeric($destination_id)) {
			show_404();
		}

		$query = $this->modglobal->getLowPrice($destination_id);
		
		$prices = array(
			'price' => 0,
			'price_normal' => 0,
			'promo_seat_discount' => 0,
		);

		if ($query->num_rows()) {
			$row = $query->row();
			$prices = array(
				'price' => $row->price,
				'price_normal' => $row->price_normal,
				'promo_seat_discount' => $row->promo_seat_discount - getenv('BNI_PROMO')
			);
		}
		
		return $prices;
	}
}
