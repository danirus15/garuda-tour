<div class="container">
	<div class="dashboard">
		<h1>Hello <?php echo $this->session->userdata('name')?></h1>
		<?php echo $this->session->userdata('email')?>
		<div class="clearfix"></div>
		<div class="link_dash">
			<a href="#" class="selected">Dashboard</a>
			<a href="#" alt="<?php echo site_url('profile')?>" class="box_modal_full">Edit Profile</a>
			<a href="#" alt="<?php echo site_url('change_password')?>" class="box_modal_full">Change Password</a>
			<a href="<?php echo site_url('logout')?>">Logout</a>
		</div>
		<div class="clearfix"></div>
		<h3>Daftar Pesanan</h3>
		<br>
		<?php foreach ($order->result_array() as $row) { ?>
			<div class="box_ box_2 box_dash">
				<div class="text">
					<div class="desti">Paket <?php echo $row['destination_name']?></div>
					<div class="clearfix"></div>
					<div class="info_total info_code">
						Kode Pesanan
						<strong><?php echo strtoupper($row['code'])?></strong>
					</div>
					<div class="group-input info_time">
						<div class="ico">
							<img src="<?php echo assets_url('images/ico_time.png')?>" alt="">
						</div>
						<label class="input-date">
							<span>Berangkat</span>
							<?php echo date('d-m-Y', strtotime($row['depart_date'])) ?>
						</label>
						<label class="input-date">
							<span>Kembali</span>
							<?php echo date('d-m-Y', strtotime($row['return_date'])) ?>
						</label>
						<div class="clearfix"></div>
					</div>
					<div class="group-input info_wis">
						<div class="ico">
							<img src="<?php echo assets_url('images/ico_dewasa.png')?>" alt="">
						</div>
						<div class="info"> <?php echo $row['qty'] + $row['qty_promo']?> Wisatawan</div>
						<div class="clearfix"></div>
					</div>
					<div class="info_total">
						TOTAL
						<strong>Rp.<?php echo price_format((($row['qty'] * $row['price']) + ($row['qty_promo'] * $row['price_promo']) + $row['suplement']) - (($row['qty'] + $row['qty_promo']) * $row['cashback']))?>,-</strong>
					</div>
					<div class="info_total status">
						STATUS
						
							<?php if ($row['status'] == '0') { ?>
								<strong class="paid unpaid">Unpaid</strong>
							<?php } elseif ($row['status'] == '2') { ?>
								<strong class="paid unpaid">Failed</strong>
							<?php } else { ?>
								<strong class="paid">Paid</strong>
							<?php } ?>

					</div>
					<div class="action">
						<?php if ($row['status'] == '1') { ?>
							<a href="#" class="box_modal_full btn" alt="<?php echo site_url('payment/printing/'.$row['id'])?>">Lihat Rincian</a>
						<?php } ?>
						
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		<?php } ?>

	</div>
</div>