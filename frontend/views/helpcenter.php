<style>
	ol li{
		padding: 3px 0;
		line-height: 130%;
	}
	ol li p {
		margin: 0;
	}
	.container_hc {
		padding: 0 30px;
	}
	.faq p strong {
		font-size: 18px;
		line-height: 130%;
	}
</style>
<div class="container container_hc">
	<br><br>
	<div class="title_help"><h1>Help Center</h1></div>
	<div id="bahasa"></div>
	<div class="link_">
		<a href="#bahasa" class="selected">Bahasa Indonesia</a>
		<a href="#english">English</a>
	</div>
	
	<div class="title_help2">Hubungi Kami</div>
	<div class="text">
		Apakah kamu memiliki pertanyaan lebih lanjut dan membutuhkan bantuan lainnya? Kamu dapat menghubungi kami di 0822-1140-1146 dari pukul 09:00 - 17:00 atau email kami di ayoliburan@garuda-indonesia.com. Tim kami akan siap membantumu!

	</div>
	<div class="title_help2">FAQ</div>
	<div class="text faq">
		<p dir="ltr"><strong>1. Bagaimana Proses pemesanan paket wisata #AyoLiburan?</strong></p>
		<p dir="ltr">Pemesanan dan pembelian paket wisata #AyoLiburan bisa kamu lakukan secara eksklusif di website
resmi Ayo Liburan - Garuda Indonesia di ayoliburan.garuda-indonesia.com &amp; menggunakan Kartu
Kredit BNI.</p>
		<p dir="ltr"><strong>2. Kapan maksimal waktu sebelum keberangkatan,saya dapat melakukan proses
pemesanan dan pembelian paket wisata #AyoLiburan?</strong></p>
		<p dir="ltr">Pemesanan dan pembelian tiket paket wisata #AyoLiburan, dapat dilakukan 8 (delapan) hari
sebelum periode perjalanan untuk destinasi Malang, Belitung, Labuan Bajo, Banyuwangi dan Raja
Ampat. Maksimum pembelian paket wisata Garuda Indonesia- #AyoLiburan dapat dilakukan 15
(lima belas) hari sebelum periode perjalanan untuk destinasi Padang.</p>
		<p dir="ltr"><strong>3. Bagaimana saya melakukan pembayaran untuk pesanan saya?</strong></p>
		<p dir="ltr">Pembayaran dapat kamu lakukan melalui website resmi ayoliburan.garuda-indonesia.com dengan
menggunakan Kartu Kredit BNI.</p>
		<p dir="ltr"><strong>4. Metode pembayaran apa saja yang diterima dalam pemesanan paket
#AyoLiburan?</strong></p>
		<p dir="ltr">#AyoLiburan hanya menerima metode pembayaran melalui Kartu Kredit BNI saja</p>
		<p dir="ltr"><strong>5. Mengapa transaksi saya tidak berhasil?</strong></p>
		<p dir="ltr">Jangan khawatir, hal ini biasa terjadi. Transaksi bisa saja tidak berhasil dikarenakan beberapa hal.
Kegagalan transaksi dapat terjadi dikarenakan adanya masalah pada kartu kredit atau kesalahan
teknis. Anda dapat memeriksa kembali kelengkapan dokumen pembayaran serta detail kartu kredit
dan mengulang proses pembayaran Anda. Silahkan hubungi call center kami di 0822-1140- 1146
atau mengirimkan email melalui ayoliburan@garuda-indonesia.com jika Anda membutuhkan
bantuan lainnya.</p>
		<p dir="ltr"><strong>6. Apakah terdapat jumlah minimum dan maksimum pembelian paket #AyoLiburan
dalam satu reservasi menggunakan 1 (satu) Kartu Kredit?</strong></p>
		<p dir="ltr">Kamu dapat melakukan pembelian Paket #AyoLiburan dengan maksimum pembelian untuk 8
(delapan) orang dalam satu reservasi menggunakan 1 (satu) Kartu Kredit BNI.</p>
		<p dir="ltr"><strong>7. Apakah pembelian paket wisata untuk anak-anak atau bayi akan mendapatkan
harga khusus?</strong></p>
		<p dir="ltr">Tidak. Pembelian paket wisata untuk anak-anak akan dikenakan harga normal.</p>
		<p dir="ltr"><strong>8. Apakah pembelian paket wisata untuk usia lanjut (60 tahun keatas) akan mendapatkan harga khusus?</strong></p>
		<p dir="ltr">Tidak. Pembelian paket wisata untuk usia lanjut akan dikenakan harga normal.</p>
		<p dir="ltr"><strong>9. Dari mana sajakah titik keberangkatan perjalanan Ayo Liburan 2017?</strong></p>
		<p dir="ltr">Untuk saat ini titik keberangkatan Ayo Liburan 2017 hanya akan diadakan dari kota Jakarta saja.</p>
		<p dir="ltr"><strong>10. Apakah harga yang tertera pada website sudah termasuk tax dan biaya lain-lain?</strong></p>
		<p dir="ltr">Ya, harga yang tertera pada website ayoliburan.garuda-indonesia.com sudah termasuk harga tax, tiket pesawat, hotel, dan akomodasi selama perjalanan.</p>
		<p dir="ltr"><strong>11. Bagaimana saya dapat melihat status pemesanan dan pembelian saya?</strong></p>
		<p dir="ltr">Kamu dapat melihat status pemesanan atau pembelianmu dengan melihat pada halaman dashboardmu.</p>
		<p dir="ltr"><strong>12. Apakah saya dapat mencetak bukti pembayaran saya?</strong></p>
		<p dir="ltr">Ya, kamu dapat mencetak bukti pembelian dan pembayaran setelah seluruh proses transaksi berhasil dilakukan. Kamu hanya perlu menekan tombol cetak pada halaman dashboardmu.</p>
		<p dir="ltr"><strong>13. Bagaimana jika terjadi kesalahan sistem ketika melakukan pembelian atau pembayaran?</strong></p>
		<p dir="ltr">Kamu dapat menghubungi call center kami di 0822-1140-1146 atau mengirimkan email melalui ayoliburan@garuda-indonesia.com .</p>
		<p dir="ltr"><strong>14. Apa yang harus saya lakukan jika saya melakukan pemesanan namun tidak dapat melakukan pembayaran?</strong></p>
		<p dir="ltr">Jangan khawatir, hal ini biasa terjadi. Transaksi bisa saja tidak berhasil dikarenakan beberapa hal. Kegagalan transaksi dapat terjadi dikarenakan adanya masalah pada kartu kredit atau kesalahan teknis. Kamu dapat memeriksa kembali kelengkapan dokumen pembayaran serta detail kartu kredit dan mengulang proses pembayaranmu. Silahkan hubungi call center kami di 0822-1140-1146 atau mengirimkan email melalui ayoliburan@garuda-indonesia.com jika kamu membutuhkan bantuan lainnya.</p>
		<p dir="ltr"><strong>15. Bagaimana jika saya belum melakukan pembayaran akan tetapi time limit telah habis? (Bagaimana jika saya belum melakukan pembayaran namun batas waktu yang disediakan telah habis ?)</strong></p>
		<p dir="ltr">Kamu akan diberikan waktu session hold selama 20 (dua puluh) menit. Jika kamu telah melewati batas waktu pembayaran, maka pesananmu dianggap batal dan kamu dapat melakukan pemesanan ulang (kamu akan diberikan waktu yang terus berjalan selama 20 menit. Jika telah melewati batas waktu pembayaran, maka pesananmu dianggap batal. Kamu dapat kembali melakukan pemesanan ulang).</p>
		<p dir="ltr"><strong>16. Apakah terdapat minimum kuota keberangkatan pada setiap paket Ayo Liburan 2017?</strong></p>
		<p dir="ltr">Ya, minimum kuota keberangkatan untuk destinasi wisata Malang, Belitung, Labuan Bajo, dan Banyuwangi adalah 10 pax. Untuk destinasi Padang, minimum kuota keberangkatan adalah 20 pax</p>
		<p dir="ltr"><strong>17. Bagaimana jika saya ingin membatalkan pemesanan paket wisata Ayo Liburan 2017?</strong></p>
		<p dir="ltr">Kamu dapat melakukan proses pembatalan pesanan paket wisata Ayo Liburan 2017 dengan menghubungi call center kami di 0822-1140-1146 atau dengan mengajukan email ke ayoliburan@garuda-indonesia.com sesuai dengan syarat dan ketentuan yang berlaku.</p>
		<p dir="ltr"><strong>18. Jika saya membatalkan perjalanan saya, apakah paket liburan yang sudah saya beli dapat digunakan oleh teman saya atau orang lain</strong></p>
		<p dir="ltr">Maaf, akan tetapikamu tidak dapat mengubah nama pemesanan.</p>
		<p dir="ltr"><strong>19. Jika saya melakukan pembatalan pembelian, apakah saya dapat melakukan proses refund?</strong></p>
		<p dir="ltr">Ya, kamu dapat mengajukan proses refund jika kamu melakukan pembatalan pembelian paket wisata Ayo Liburan 2017 sesuai dengan syarat dan ketentuan yang berlaku.</p>
		<p dir="ltr"><strong>20. Jika terdapat kesalahan teknis yang menyebabkan pesanan saya tidak dapat di proses tetapi saya sudah melakukan pembayaran, apakah saya dapat melakukan refund?</strong></p>
		<p dir="ltr">Ya, kamu dapat melakukan proses refund.</p>
		<p dir="ltr"><strong>21. Berapa lama maksimal proses refund?</strong></p>
		<p dir="ltr">Proses refund yang kamu ajukan akan kami proses selama 14 (empat belas) hari kerja</p>
		<p dir="ltr"><strong>22. Berapa banyak jumlah refund yang akan saya terima?</strong></p>
		<p dir="ltr">Jika terdapat kesalahan teknis yang disebabkan dari pihak Ayo Liburan 2017, maka kamu akan mendapatkan refund sebesar 100% dari total pesananmu.</p>
		<p dir="ltr"><strong>23. Berapa banyak jumlah refund yang akan saya terima jika saya melakukan pembatalan?</strong></p>
		<p dir="ltr">Refund sebesar 50% jika melakukan pembatalan maksimum 7 hari atau 1 minggu sebelum tanggal keberangkatan.</p>
		<p dir="ltr"><strong>24. Bagaimana cara saya untuk menghubungi pihak Ayo Liburan-Garuda 2017? Apakah terdapat call center?</strong></p>
		<p dir="ltr">Jika kamu memiliki pertanyaan dan informasi lebih lanjut, kamu dapat menghubungi call center kami di 0822-1140-1146 atau email kami di ayoliburan@garuda-indonesia.com. Tim kami akan dengan senang hati untuk membantumu.</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
	</div>
	<div class="title_help2">Syarat & Ketentuan</div>
	<div class="text">
		<b>Selamat datang di Garuda Indonesia – #AyoLiburan</b>

		<p dir="ltr">Garuda Indonesia – #AyoLiburan merupakan paket wisata yang menawarkan perjalanan ke
enam destinasi wisata di Indonesia. Perjalanan wisata ini akan diadakan pada periode tertentu.
Untuk itu, kami telah menyiapkan beberapa syarat dan ketentuan serta kebijakan privasi guna
memastikan bahwa website ini dapat memberikan pelayanan yang memuaskan dan bersifat
aman bagi semua pihak. Garuda Indonesia – #AyoLiburan berhak untuk mengubah pedoman ini
setiap saat.</p>

		<p dir="ltr">Kamu disarankan untuk membaca serta memperhatikan syarat dan ketentuan berikut ini dengan seksama. Dengan membaca syarat dan ketentuan yang tertera, kamu dianggap telah menerima dan menyetujui segala hal yang tertera pada pedoman yang ada.</p>

		<p dir="ltr">Semua konten yang ada pada website Garuda Indonesia – #AyoLiburan dimiliki dan
dikendalikan oleh pihak Garuda Indonesia – #AyoLiburan dan dilindungi oleh undang-undang
dan hak cipta. Isi dan materi pada halaman website termasuk dan tidak terbatas pada teks,
gambar, dan ikon tertentu dilindungi oleh hak cipta, merek dagang, merek layanan, nama
dagang, dan hak kekayaan intelektual dimilki dan dikendalikan oleh Garuda Indonesia</p>
		<p dir="ltr">Berikut merupakan syarat dan ketentuan yang perlu diperhatikan dalam pembelian paket
liburan Garuda Indonesia – #AyoLiburan:</p>
		<ol>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Garuda Indonesia – #AyoLiburan menyediakan pilihan paket liburan untuk 6 (enam)
destinasi wisata di Indonesia, yaitu Malang, Belitung, Labuan Bajo, Banyuwangi, Padang,
dan Raja Ampat dengan total paket sebanyak 800 paket wisata dengan paket wisata ke
Malang sebanyak 25 paket per periode untuk 4 kali periode perjalan, Belitung sebanyak
25 paket per periode untuk 4 kali periode perjalanan, Labuan Bajo sebanyak 25 paket
per periode untuk 6 kali periode perjalanan, Banyuwangi sebanyak 25 paket per periode
untuk 6 kali periode perjalanan, Padang sebanyak 75 paket periode untuk 4 kali periode
perjalanan, dan Raja Ampat sebanyak 20 paket periode untuk 4 kali periode perjalanan.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Pemesanan dan pembelian tiket Garuda Indonesia – #AyoLiburan hanya dapat dilakukan
pada website resmi kami di ayoliburan.garuda-indonesia.com.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Pembayaran untuk pembelian paket wisata hanya dapat Anda lakukan dengan
menggunakan kartu kredit BNI (Mastercard, Visa, dan JCB).</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Setiap pembelian paket wisata Garuda Indonesia – #AyoLiburan menggunakan kartu
kredit BNI, akan mendapatkan cahsback dengan ketentuan sebagai berikut :</p>
		<ol>
		<li dir="ltr" style="list-style-type: lower-alpha;">
		<p dir="ltr">Promo Cashback Kartu Kredit BNI berlaku untuk seluruh jenis kartu kredit BNI
kecuali iB Hasanah Card.</p>
		</li>
		<li dir="ltr" style="list-style-type: lower-alpha;">
		<p dir="ltr">Cashback Rp 1.000.000,00 (Satu Juta Rupiah) untuk pemegang kartu BNI
MasterCard dan Rp 500.000,00 (Lima Ratus Ribu Rupiah) untuk pemegang kartu
BNI Visa dan JCB.</p>
		</li>
		</ol>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Setiap pembelian paket wisata Garuda Indonesia – #AyoLiburan menggunakan kartu
kredit BNI, akan mendapatkan fasilitas cicilan BNI Installment 0% (nol persen) dengan
ketentuan sebagai berikut :</p>
		<ol>
		<li dir="ltr" style="list-style-type: lower-alpha;">
		<p dir="ltr">Promo cicilan BNI 0% (nol persen) berlaku untuk seluruh jenis kartu kredit BNI
kecuali BNI Corporate Card dan iB Hasanah Card.</p>
		</li>
		<li dir="ltr" style="list-style-type: lower-alpha;">
		<p dir="ltr">BNI cicilan 0% (nol persen) dengan periode cicilan 6 (enam) bulan dan 12 (dua
belas) bulan.</p>
		</li>
		<li dir="ltr" style="list-style-type: lower-alpha;">
		<p dir="ltr">Minimum transaksi untuk dapat menggunakan fasilitas cicilan 0% (nol persen)
adalah Rp1.000.000,- ( satu juta Rupiah), termasuk apabila Pemegang Kartu
melakukan Point Redemption.</p>
		</li>
		<li dir="ltr" style="list-style-type: lower-alpha;">
		<p dir="ltr">Transaksi cicilan 0% (nol persen) dapat dilakukan langsung melalui website resmi
kami ayoliburan.garuda-indonesia.com.</p>
		</li>
		</ol>
		</li>
		<!-- <li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Setiap 15 (lima belas) orang pembeli pertama yang melakukan pembelian di website untuk masing-masing tujuan destinasi Malang, Banyuwangi, Labuan Bajo, Belitung, dan 50 (lima puluh) orang pembeli pertama untuk destinasi Padang akan mendapatkan harga spesial.</p>
		</li> -->
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Pembelian maksimum  paket #AyoLiburan adalah untuk 8 (delapan) orang dalam satu
reservasi menggunakan 1 (satu) kartu kredit BNI.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Maksimum pembelian paket wisata Garuda Indonesia – #AyoLiburan dapat dilakukan 1
(satu) minggu atau 7 (tujuh) hari sebelum periode perjalanan.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Harga pembelian paket Ayo Liburan untuk bayi dan anak-anak akan dikenakan harga
normal.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Seluruh harga yang tertera pada website sudah termasuk harga tax, tiket pesawat, hotel, dan akomodasi selama perjalanan.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Status pemesanan paket wisata Garuda Indonesia – #AyoLiburan dapat Anda lihat pada
dashboard akun Anda.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Harap untuk menyimpan bukti transaksi pembelian paket wisata Garuda Indonesia –
#AyoLiburan yang telah Anda cetak setelah menyelesaikan seluruh proses transaksi.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Bukti transaksi akan dikirimkan melalui email.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Paket wisata yang telah dibeli tidak dapat dipindahtangakan kepada pihak manapun.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Paket wisata Garuda Indonesia – #AyoLiburan dapat dibatalkan dengan maksimum
waktu pembatalan adalah 7 (tujuh) hari sebelum tanggal keberangkatan.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Jika kamu melakukan proses pembatalan dengan maksimum waktu pembatalan adalah 7 (tujuh) hari, maka kamu dapat mengajukan proses refund (pengembalian). Permintaan pengembalian akan diproses dengan waktu maksimal 14 hari kerja dengan jumlah pengembalian sebesar 50% (lima puluh persen) dari total paket yang kamu batalkan. Jika kamu melakukan pembatalan kurang dari 7 (tujuh) hari sebelum tanggal keberangkatan, maka uang yang telah kamu bayarkan tidak dapat dikembalikan.</p>
		</li>
		</ol>
		<p>&nbsp;</p>
		<div class="title_help2">Kebijakan Privasi</div>
		<p dir="ltr">Kami sadar bahwa privasi merupakan hal yang sangat penting. Kebijakan privasi ini berlaku
untuk seluruh aktivitas dan transaksi yang ada pada website Garuda Indonesia- #AyoLiburan.
Garuda Indonesia #AyoLiburan berkomitmen untuk melindungi dan menjaga kerahasiaan
informasi pribadi yang Anda berikan dari tindakan pemalsuan, penyalahgunaan, atau akses lain

yang tidak diizinkan untuk tujuan-tujuan yang tidak bertanggungjawab. Sesuai dengan
kebijakan kami, kami tidak berhak untuk menyebarkan, menjual, atau menyewakan informasi
apapun terkait dengan data pribadi Anda kepada pihak-pihak lain. Garuda Indonesia – #Ayo
Liburan memiliki hak untuk merubah kebijakan privasi ini tanpa pemberitahuan. Kami
merekomendasikan Anda untuk secara berkala membaca ulang pernyataan privasi untuk
mengetahui perubahan-perubahan ini.</p>
		<p></p>
	</div>
	<br><br><br><br>
	<div class="link_">
		<a href="#bahasa">Bahasa Indonesia</a>
		<a href="#english" class="selected">English</a>
	</div>
	<div id="english"></div>
	<div class="title_help2">Contact Us</div>
	<div class="text">
		Have any question or need some assistance?  Please contact us through our call center 0822-1140-1146 from 09:00 - 17:00 or send your email to ayoliburan@garuda-indonesia.com. We are ready to help you! 
	</div>
	<div class="title_help2">FAQ</div>
	<div class="text faq">
		<p dir="ltr"><strong>1. How can I book #AyoLiburan holiday package?</strong></p>
		<p dir="ltr">You can only book and purchase #AyoLiburan holiday packages through the Garuda Indonesia –
#AyoLiburan official website at ayoliburan.garuda-indonesia.com &amp; using the BNI Credit Card.</p>
		<p dir="ltr"><strong>2. How long before the trip period can I make the process of booking or purchasing
#AyoLiburan Holiday Package?</strong></p>
		<p dir="ltr">You can place your order 8 (eight) days before the travel period for destination Malang, Belitung,
Labuan Bajo, Banyuwangi, and Raja Ampat. The deadline for purchasing vacation package for
destination to Padang is 15 (fifteen) days before the travel period.</p>
		<p dir="ltr"><strong>3.How do I make a payment for my order?</strong></p>
		<p dir="ltr">We aim to offer you a convenient payment process for your order. Your payment process can be
done through the official website ayoliburan.garuda-indonesia.com only using BNI Credit Card.</p>
		<p dir="ltr"><strong>4. What payment methods are accepted in purchasing Ayo Liburan 2017 Holiday Package?</strong></p>
		<p dir="ltr">#AyoLiburan Holiday Package only receives payment method using BNI Credit Card.</p>
		<p dir="ltr"><strong>5. Why did my transaction fail?</strong></p>
		<p dir="ltr">Don&rsquo;t worry! It happens sometimes. Transactions can be declined for several reasons. It might be caused by credit card issues or technical errors. Please re-check your document, personal informations, and your credit card detail before re-doing your payment process. You can contact us through our call center at 0822-1140-1146 or send us an email at ayoliburan@garuda-indonesia.com for further assistance.</p>
		<p dir="ltr"><strong>6. Is there any minimum or maximum purchase for #AyoLiburan Holiday Package in
one reservation using 1 (one) credit card?</strong></p>
		<p dir="ltr">You can purchase #AyoLiburan Package with maximum purchasing 8 (eight) persons in one
reservation using 1 (one) BNI Credit Card.</p>
		<p dir="ltr"><strong>7. Is there any special price for children or infant?</strong></p>
		<p dir="ltr">We are sorry, but children or infant will be charged the normal price</p>
		<p dir="ltr"><strong>8. Is there any special price for elderly (60 years and above)?</strong></p>
		<p dir="ltr">We are sorry, but elderly will be charged the normal price.</p>
		<p dir="ltr"><strong>9. From which point of departure of Ayo Liburan 2017?</strong></p>
		<p dir="ltr">For now, the departure point for Ayo Liburan 2017 will only be held from Jakarta.</p>
		<p dir="ltr"><strong>10. Does the price listed on the website includes taxes and other fees?</strong></p>
		<p dir="ltr">Yes, the price listed on the website includes taxes, flight ticket, hotel, and accommodations during the trip.</p>
		<p dir="ltr"><strong>11. How can I check my booking status?</strong></p>
		<p dir="ltr">You can check your booking status on your dashboard page. But don&rsquo;t worry, we will inform your purchasing status to your email as well.</p>
		<p dir="ltr"><strong>12.Can I print out my invoice after I pay?</strong></p>
		<p dir="ltr">Yes, you can print out your invoice after the whole transaction process is succeed by clicking the Print button on your dashboard page.</p>
		<p dir="ltr"><strong>13. What if there is any system error occurs during the purchasing or payment process?</strong></p>
		<p dir="ltr">You can contact us through our call center 0822-1140-1146or send us email to ayoliburan@garuda-indonesia.com</p>
		<p dir="ltr"><strong>14. What should I do if I already placed an order but can not make a payment?</strong></p>
		<p dir="ltr">Don&rsquo;t worry! It happens sometimes. Transactions can be declined for several reasons. It might be caused by some credit card issues or technical errors. Please re-check your document, personal informations , and your credit card detail before re-doing your payment process. You contact us through our call center at 0822-1140-1146 or send us email at ayoliburan@garuda-indonesia.com for further assistance.</p>
		<p dir="ltr"><strong>15. If I have not made the payment process, but the time limit has expired, what would happen to my order?</strong></p>
		<p dir="ltr">You will be given session hold for 20 (twenty) minutes. If the time limit is out, you order is considered cancelled and you can make new reservation.</p>
		<p dir="ltr"><strong>16. Is there any minimum quota for the trip to begin?</strong></p>
		<p dir="ltr">Yes, there will be a minimum quota for each destination. 10 (ten) packages for trip to Malang, Belitung, Labuan Bajo, Banyuwangi and minimum 20 (twenty) packages for trip to Padang.</p>
		<p dir="ltr"><strong>17. Is it possible to cancel my order?</strong></p>
		<p dir="ltr">You will be possible to cancel your order by contacting us through our call center 0822-1140-1146 or send us your email to ayoliburan@garuda-indonesia.com</p>
		<p dir="ltr"><strong>18. I will not be able to go, can my friend or my relatives use the Holiday Package I already purchased?</strong></p>
		<p dir="ltr">Sorry, but you cannot change the name of the reservation.</p>
		<p dir="ltr"><strong>19. If I cancel my order, will I be able to make a refund?</strong></p>
		<p dir="ltr">Yes, you will be able to make a refund process according to our Terms and Conditions.</p>
		<p dir="ltr"><strong>20. If there is any technical error which caused my order cannot be processed, whereas I already did the payment, will I be able to make a refund?</strong></p>
		<p dir="ltr">Yes, you will be able to make a refund process according to our Terms and Conditions</p>
		<p dir="ltr"><strong>21. How long will my refund be processed?</strong></p>
		<p dir="ltr">Your refund will be processed in 14 (fourteen) work days.</p>
		<p dir="ltr"><strong>22. How much refund will I receive?</strong></p>
		<p dir="ltr">If there is any technical error caused by our party, you will get 100% refund of your total order.</p>
		<p dir="ltr"><strong>23. How much refund will I receive if I cancel my booking?</strong></p>
		<p dir="ltr">If you cancel your booking one week prior the departure day, you will receive 50% refund.</p>
		<p dir="ltr"><strong>24. How can I contact Ayo Liburan 2017 customer service if I need further assistance?</strong></p>
		<p dir="ltr">If you have any further questions, you can contact us through our call center 0822-1140-1146 or if you prefer to send us an email, please kindly send your email to ayoliburan@garuda-indonesia.com. Our team will be glad to assist you.</p>
		<p>&nbsp;</p>
		<p dir="ltr">&nbsp;</p>

	</div>
	<div class="title_help2">Terms & Conditions</div>
	<div class="text">
		<b>Welcome to Garuda Indonesia - #AyoLiburan</b>
		<p dir="ltr">Garuda Indonesia – #AyoLiburan program is a vacation package that offers journeys to six
tourism destinations in Indonesia. These travel packages will be available on the dates
determined by the program. Therefore, we have prepared some terms and conditions and a
privacy policy to ensure that this website can provide all parties with the best and secure
service. Garuda Indonesia – #AyoLiburan has the right to change these terms and conditions at
any time without prior notice.</p>
		<p dir="ltr">You are advised to read and observe these terms and conditions. By reading our terms and
conditions, you will be considered to have accepted and approved of all items included in our
terms and conditions.</p>
		<p dir="ltr">All content published on the Garuda Indonesia – #AyoLiburan website is owned and controlled
by Garuda Indonesia – #AyoLiburan and is protected by related laws and the copyright laws.
The contents and materials of the website, including and not limited to the text, pictures and
icons, are protected by copyright laws, trademark, service brand, name brand and intellectual
rights owned and controlled by Garuda Indonesia.</p>
		<p dir="ltr">The following are the terms and conditions that must be observed before purchasing a Garuda
Indonesia – #AyoLiburan vacation package:</p>
		<ol>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Garuda Indonesia – #AyoLiburan offers vacation packages for 6 (six) tourism
destinations in Indonesia, which include Malang, Belitung, Labuan Bajo, Banyuwangi,
Padang, and Raja Ampat with a total number of 800 tourism packages divided into 25
packages to Malang per travel period for 4 (four) travel periods, 25 packages to Belitung
per travel period for 4 (four) travel periods, 25 packages to Labuan Bajo per travel
period for 6 (six) travel periods, 25 packages to Banyuwangi per travel period for 6 (six)
travel periods, 75 packages to Padang per travel period for 4 (four) travel periods, 20
packages to Raja Ampat per travel period for 4 (four) travel periods.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Reservations and purchase of tickets for Garuda Indonesia – #AyoLiburan can only be
done through our official website at ayoliburan.garuda-indonesia.com.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Payment for these travel packages can only be done using a BNI Credit Card(
Mastercard, Visa, JCB).</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">All purchases of Garuda Indonesia – #AyoLiburan travel packages using the BNI Credit
Card will receive a cash back according to the following provisions :</p>
		<ol>
		<li dir="ltr" style="list-style-type: lower-alpha;">
		<p dir="ltr">Promo Cashback BNI Credit Card is valid for all types of BNI Credit Card except
for the iB Hasanah Card &amp; Corporate Card.</p>
		</li>
		<li dir="ltr" style="list-style-type: lower-alpha;">
		<p dir="ltr">Cashback Rp 1.000,000 (one million rupiahs) for BNI MasterCard holders and Rp
500,000 (five hundred thousand rupiahs) for BNI Visa and JCB.</p>
		</li>
		</ol>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">All purchases of Garuda Indonesia – #AyoLiburan travel packages using a BNI Credit Card
will receive a facility in the form of 0% (zero percent) Installments according to the
following provisions:</p>
		<ol>
		<li dir="ltr" style="list-style-type: lower-alpha;">
		<p dir="ltr">BNI 0% (zero percent) Installment promo is valid for all types BNI Credit Card
except for the iB Hasanah Card &amp; Corporate Card.</p>
		</li>
		<li dir="ltr" style="list-style-type: lower-alpha;">
		<p dir="ltr">BNI 0% (zero percent) Installment will be available for a 6 (six) month or 12
(twelve) month installment payment program.</p>
		</li>
		<li dir="ltr" style="list-style-type: lower-alpha;">
		<p dir="ltr">Minimum transaction for using this 0% (zero percent) Installment promo is
Rp1,000,000 ( one million Rupiah), including if the card holder uses Redemption
Points.</p>
		</li>
		<li dir="ltr" style="list-style-type: lower-alpha;">
		<p dir="ltr">BNI 0% (zero percent) Installment promo transactions can be done directly
through our official website at ayoliburan.garuda-indonesia.com.</p>
		</li>
		</ol>
		</li>
		<!-- <li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">The first 15 (fifteen) buyers that purchase vacation packages for each of the following destinations, including Malang, Banyuwangi, Labuan Bajo, Belitung, and the first 50 (fifty) buyers that purchase vacation packages for Padang will get a special price.</p>
		</li> -->
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">The deadline for purchasing vacation packages under the Garuda Indonesia –
#AyoLiburan program is 1 (one) week or 7 (seven) days before the travel period.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">All prices published on the website include taxes, airfare, hotels and accommodation
during the trip.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">The status of your purchase of the tickets for the Garuda Indonesia – #AyoLiburan
vacation package can be viewed on the dashboard of your Account.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">You must keep your proof of transaction when purchasing a vacation package under the
Garuda Indonesia – #AyoLiburan program by printing it after your transaction is
completed.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">Your proof of transaction will be sent to your e-mail.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">The vacation package purchased is non-transferrable.</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">The Garuda Indonesia – #AyoLiburan vacation package can be cancelled maximum 7
(seven) days before the date of departure..</p>
		</li>
		<li dir="ltr" style="list-style-type: decimal;">
		<p dir="ltr">If you cancel your trip maximum 7 (seven) days before the date of departure, then you
can submit a proposal to refund your purchase. This refund will be processed maximum
14 (fourteen) working days with a total refund amounting to 50% (fifty percent) out of
the total cost of the package you cancelled. If you cancel your package less than 7
(seven) days before the date of departure, then you will not be liable to receive a refund
for the cost of the trip.</p>
		</li>
		</ol>
		<p><br /><br /></p>
		<div class="title_help2">Privacy Policy</div>
		<p dir="ltr">We realize that privacy is a important matter. This privacy policy applies for all activities and
transactions performed on the Garuda Indonesia- #AyoLiburan website. Garuda Indonesia-
#AyoLiburan is committed to protecting the privacy of your personal information from identity
theft, misuse of information or other prohibited and irresponsible actions. In accordance with
our policy, we do not have the right to spread, sell or rent out your private information to other
parties. Garuda Indonesia- #AyoLiburan has the right to change this privacy policy without prior
notice. We recommend you periodically read our privacy policy to see if any changes are made.</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
	</div>
	
	
</div>