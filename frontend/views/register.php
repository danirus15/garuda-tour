<!doctype html>	
<html>
	<?php $this->load->view('partials/head')?>
</html>
<body class="body_pop">
<div class="container_pop container_pop_reg">
	<a href="<?php echo site_url('')?>"><img src="<?php echo assets_url('images/btn_close.png')?>" alt="" class="close_pop close_box_in"></a>
	<div class="title_pop">REGISTER</div>
	<?php echo validation_errors('<div class="notif">', '</div>'); ?>
	<?php if ($this->session->flashdata('error')) { ?>
		<div class="notif">
		  <?php echo $this->session->flashdata('error')?>
		</div>
	<?php } ?>
	<?php echo form_open('')?>
		<div class="group-input">
			<label>NAMA</label>
			<?php echo form_input('name',set_value('name'), array('placeholder' => 'Name', 'required' => 'true'))?>
            <?php echo form_error('name');?>
		</div>
		<div class="group-input">
			<label>ALAMAT EMAIL</label>
			<?php echo form_input('email',set_value('email'), array('data-validation'=>'email', 'placeholder' => 'Email Address', 'required' => 'true'))?>
            <?php echo form_error('email');?>
		</div>
		<div class="group-input">
			<label>PASSWORD</label>
			<?php echo form_password('password_confirmation','', array('data-validation'=>'length', 'data-validation-length' => 'min8'))?>
            <?php echo form_error('password');?>
		</div>
		<div class="group-input">
			<label>RE-PASSWORD</label>
			<?php echo form_password('password','', array('data-validation'=>'confirmation'))?>
            <?php echo form_error('password');?>
		</div>
		<div class="clearfix"></div>
		<div align="center">
			<input type="submit" value="REGISTER" class="btn">
			<div class="clearfix"></div>
			<br>
			Sudah terdaftar sebelumnya?
			<br>
			<a href="<?php echo site_url('login')?>">Login Sekarang</a>
		</div>

	</form>
</div>
<?php $this->load->view('partials/js')?>
</body>
</html>