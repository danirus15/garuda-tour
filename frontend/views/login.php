<!doctype html>	
<html>
<?php $this->load->view('partials/head')?>
</html>
<body class="body_pop">
<div class="container_pop">
	<a href="<?php echo site_url('')?>"><img src="<?php echo assets_url('images/btn_close.png')?>" alt="" class="close_pop close_box_in"></a>
	<div class="title_pop">LOGIN</div>
	<?php echo validation_errors('<div class="notif">', '</div>'); ?>
	<?php if ($this->session->flashdata('error')) { ?>
		<div class="notif">
		  <?php echo $this->session->flashdata('error')?>
		</div>
	<?php } ?>
	<?php echo form_open(site_url('login').$callback, array('target' => '_parent'))?>
		<div class="group-input">
			<label><?php echo strtoupper($this->lang->line('alamat_email'))?></label>
			<?php echo form_input('email',set_value('email'), array('data-validation'=>'email', 'placeholder' => $this->lang->line('alamat_email'), 'required' => 'true'))?>
            <?php echo form_error('email');?>
		</div>
		<div class="group-input">
			<label>PASSWORD</label>
			<?php echo form_password('password','', array('required'=>'true', 'placeholder' => 'Password'))?>
            <?php echo form_error('password');?>
		</div>
		<div class="clearfix"></div>
		<div align="center">
			<input type="submit" value="LOGIN" class="btn">
			<div class="clearfix"></div>
			<a href="<?php echo site_url('forgotpassword')?>"><?php echo $this->lang->line('lupa_password')?></a>
			<br><br>
			<?php echo $this->lang->line('belum_terdaftar')?><br>
			<a href="<?php echo site_url('register')?>"><?php echo $this->lang->line('daftar_sekarang')?></a>
		</div>

	</form>
</div>

<?php $this->load->view('partials/js')?>
</body>
</html>