<!doctype html>	
<html>
<?php $this->load->view('partials/head')?>
<body class="body_pop">
<img src="<?php echo assets_url('images/btn_close.png')?>" alt="" class="close_pop close_box_in close_box_out">
<div class="container_pop container_pop2">
	<div class="box_ box_2">
		<div class="text">
			<div class="title">
				<img src="<?php echo assets_url('images/logo_ayo2.png')?>" alt="" height="80" style="float: left; margin-right: 30px;">
				<?php echo $order->destination_name?>
				<span class="code">
					<?php echo strtoupper($this->lang->line('kode_pesanan'))?>
					<strong><?php echo strtoupper($order->code)?></strong>
				</span>
			</div>
			<div class="clearfix"></div>
			<div>
				<div>
					<b>Pembayaran:</b> <?php echo ($order->installment) ? $order->installment.' bln cicilan' : 'Full Payment' ?><br>
					<b>Suplement:</b> <?php echo ($order->suplement) ? 'Yes' : 'No' ?>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="info_box">
				<div class="group-input info_time">
					<div class="ico">
						<img src="<?php echo assets_url('images/ico_time.png')?>" alt="">
					</div>
					<label class="input-date">
						<span><?php echo $this->lang->line('berangkat')?></span>
						<?php echo date('d-m-Y', strtotime($order->depart_date))?>
					</label>
					<label class="input-date">
						<span><?php echo $this->lang->line('kembali')?></span>
						<?php echo date('d-m-Y', strtotime($order->return_date))?>
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="group-input info_wis">
					<div class="ico">
						<img src="<?php echo assets_url('images/ico_dewasa.png')?>" alt="">
					</div>
					<div class="info"> <?php echo $order->qty + $order->qty_promo?> Wisatawan</div>
					<div class="clearfix"></div>
				</div>
				<div class="info_total">	
					<?php 
					$total = (($order->qty_promo * $order->price_promo) + ($order->qty * $order->price) + $order->suplement)  - (($order->qty + $order->qty_promo) * $order->cashback);
					?>
					TOTAL
					<strong>Rp.<?php echo price_format($total)?>,-</strong>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="infowisatawan">
				<h3><?php echo $this->lang->line('info_pemesan')?></h3>
				<div class="iw">
					<div class="isi">
						<div class="jdl"><?php echo $this->lang->line('nama')?></div>
						<div class="isi2"><?php echo $order->user_name?></div>
						<div class="clearfix"></div>
						<div class="jdl"><?php echo $this->lang->line('alamat_email')?></div>
						<div class="isi2"><?php echo $order->user_email?></div>
						<div class="clearfix"></div>
						<div class="jdl"><?php echo $this->lang->line('telepon')?></div>
						<div class="isi2"><?php echo $order->contact_phone?></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<br><br>
				<h3><?php echo $this->lang->line('informasi_wisatawan')?></h3>
				<?php $i = 1; ?>
				<?php foreach ($order_detail as $detail) { ?>
					<div class="iw">
						<div class="no"><?php echo $i?></div>
						<div class="isi">
							<div class="jdl"><?php echo $this->lang->line('nama')?></div>
							<div class="isi2"><?php echo ucfirst($detail['title'])?>. <?php echo ucwords($detail['first_name'].' '.$detail['last_name'])?></div>
							<div class="clearfix"></div>
							<div class="jdl"><?php echo $this->lang->line('tanggal_lahir')?></div>
							<div class="isi2"><?php echo date('d M Y', strtotime($detail['birthdate']))?></div>
							<div class="clearfix"></div>
							<div class="jdl"><?php echo $this->lang->line('nomer_id')?></div>
							<div class="isi2"><?php echo $detail['id_number']?></div>
							<div class="clearfix"></div>
							<div class="jdl"><?php echo $this->lang->line('telepon')?></div>
							<div class="isi2"><?php echo $detail['phone']?></div>
						</div>
						<div class="clearfix"></div>
					</div>	
					<?php $i++; ?>
				<?php } ?>
			</div>
			<br>
			<div class="clearfix"></div>
			<div class="line"></div>
			<div align="center">
				<a href="<?php echo site_url('payment/pdf/'.$order->id)?>" class="btn" target="blank"><?php echo strtoupper($this->lang->line('download_pdf'))?></a>
				<a href="#" class="btn" onclick="window.print();"><?php echo strtoupper($this->lang->line('cetak_pesanan'))?></a>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('partials/js')?>
</script>
</body>
</html>