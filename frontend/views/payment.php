<div class="nav_order">
	<div class="container">
		<div class="page">3. <?php echo $this->lang->line('bayar')?></div>
		<div class="order_pos">
			<span class="selected">1. <?php echo $this->lang->line('pilih_pesan')?></span>
			<span class="selected">2. <?php echo $this->lang->line('data_wisatawan')?></span>
			<span class="selected">3. <?php echo $this->lang->line('bayar')?></span>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container" data-sticky_parent>
	<?php echo form_open(current_url().'?'.$_SERVER['QUERY_STRING'], array('id' => 'payment-form'))?>
	<!-- s:detail_left -->
	<div class="detail_left">
		<h3 class="title3"><?php echo $this->lang->line('informasi_kartu')?></h3>
		<div class="notif m10">
			<?php echo $this->lang->line('batas_bayar')?>
		</div>
		
		<?php if ($this->session->flashdata('error')) { ?>
			<div class="notif m10">
				<?php echo $this->session->flashdata('error')?>
			</div>
		<?php } ?>
		
		<div class="box_kontak">
			<div class="t_payment">
				<h1><?php echo $this->lang->line('informasi_bayar_bni')?></h1>
			</div>
			<div class="text">
			<b><?php echo $this->lang->line('pilih_kartu_bni')?></b>
			<div class="clearfix"></div>
			<label class="select_card">
				<input type="radio" name="card_type" class="card_type" value="VISA">
				<img src="<?php echo assets_url('images/bni_other.png')?>" alt="">
				BNI Visa or JCB
			</label>
			<label class="select_card">
				<input type="radio" name="card_type" class="card_type" value="MASTERCARD">
				<img src="<?php echo assets_url('images/bni_mastercard.png')?>" alt="">
				BNI Mastercard
			</label>
			<label class="select_card">
				<input type="radio" name="card_type" class="card_type" value="TITANIUM">
				<img src="<?php echo assets_url('images/bni_titanium.png')?>" alt="">
				BNI Mastercard Titanium
			</label>
			</div>
			<div class="clearfix"></div>
			<div style="padding: 15px;">
				<div class="group-input">
					<label>Cicilan BNI</label>
					<div class="select-style">
						<select name="installment" class="" required="">
							<option value="0">Full Payment</option>
							<option value="6">6</option>
							<option value="12">12</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		
	</div>
	<!-- e:detail_left -->
	<!-- s:detail_right -->
	<div class="detail_right" data-sticky_column>
		<div class="box_">
			<div class="title2"><?php echo $this->lang->line('pesanan_anda')?></div>
			
			<div class="text">
				<div class="group-input">
					<div class="ico">
						<img src="<?php echo assets_url('images/ico_time.png')?>" alt="">
					</div>
					<label class="input-date">
						<span><?php echo $this->lang->line('berangkat')?></span>
						<?php echo date('d-m-Y', strtotime($schedule->depart_date))?>
					</label>
					<label class="input-date">
						<span><?php echo $this->lang->line('kembali')?></span>
						<?php echo date('d-m-Y', strtotime($schedule->return_date))?>
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="<?php echo assets_url('images/ico_dewasa.png')?>" alt="">
					</div>
					<div class="info"><?php echo $this->lang->line('wisatawan')?></div>
					<div class="input_num">
						<input type='text' name='quantity' value='<?php echo $order->qty_promo + $order->qty?>' class='qty' disabled/>
					</div>
					<div class="clearfix"></div>
					<ol class="list_nama">
						<?php foreach ($order_detail as $value) { ?>
							<li><?php echo $value['first_name']." ".$value['last_name'] ?> </li>	
						<?php } ?>
					</ol>
				</div>

				
			</div>
			<div class="text simulasi_cicil">
				<h3><?php echo $this->lang->line('simulasi_cicilan')?></h3>
				<div class="fl">6 <?php echo $this->lang->line('bulan_bunga')?> 0%</div>
				<div class="fr">Rp <span id="cicilan_6">-</span></div>
				<div class="clearfix"></div>
				<div class="fl">12 <?php echo $this->lang->line('bulan_bunga')?> 0%</div>
				<div class="fr">Rp <span id="cicilan_12">-</span></div>
				<div class="clearfix"></div>
			</div>
			<div class="total total2">
				<div class="num">
					<?php
					$total = ($order->qty_promo * $order->price_promo) + ($order->qty * $order->price);
					?>
					TOTAL 
					<b>Rp.<?php echo price_format($total)?>,-</b>
					<?php if ($order->suplement) { ?>
						Suplement
						<b>Rp.<?php echo price_format($order->suplement)?>,-</b>
					<?php } ?>
					<span style="color: white;">
						Discount
						<b>Rp.<span style="color:white;" id="cashback_total"></span>,-</b>
					</span>
					TOTAL PAYMENT
					<strong>Rp.<span style="color:white;" id="total_payment"><?php echo price_format($total + $order->suplement)?></span>,-</strong>
				</div>
				<div class="clearfix"></div>
			</div>
			<div align="center">
				<input type="submit" id="submit-button" class="btn_submit" value="<?php echo strtoupper($this->lang->line('konfirmasi_pembayaran'))?>">
			</div>
		</div>
	</div>
	<!-- e:detail_right -->
	</form>
	<div class="clearfix"></div>
</div>
<script type="text/javascript">
var cashback = 0;
var total_payment = <?php echo ($total  + $order->suplement)?>;
var total_payment_after_casback = total_payment;
var qty_all = <?php echo $order->qty_promo + $order->qty?>;

$(function () {
    $("#submit-button").click(function (event) {
        console.log("SUBMIT");
        event.preventDefault();

        if (typeof $('input[name=card_type]:checked').val() == 'undefined') {
        	alert('<?php echo $this->lang->line('only_bni')?>');
        } else {
        	$("#payment-form").submit();
        }
        
        return false;
    });

});

$(document).on('change', 'input[name=card_type]:checked', function() {
	if ($('input[name=card_type]:checked').val() == 'MASTERCARD') {
		cashback = <?php echo getenv('MIDTRANS_BINS_NONE_PLATINUM_CASHBACK')?>;
	} else if ($('input[name=card_type]:checked').val() == 'TITANIUM') {
		cashback = <?php echo getenv('MIDTRANS_BINS_PLATINUM_CASHBACK')?>;
	} else {
		cashback = <?php echo getenv('MIDTRANS_BINS_BNI_JBC_VISA_CASHBACK')?>;
	}

	console.log(cashback);
	calculate_total_payment();
});

$('.select_card').click(function(){
	$(".select_card").removeClass("selected");
	$(this).addClass("selected");
});

</script>