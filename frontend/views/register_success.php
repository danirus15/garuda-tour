<!doctype html>	
<html>
	<?php $this->load->view('partials/head')?>
</html>
<body class="body_pop">
<div class="container_pop">
	<a href="<?php echo site_url('')?>"><img src="<?php echo assets_url('images/btn_close.png')?>" alt="" class="close_pop close_box_in"></a>
	<div class="title_pop"><?php echo $this->lang->line('register_success')?></div>
	<div class="group-input" align="center">
		<?php echo $this->lang->line('register_message')?>.
	</div>
	<div class="clearfix"></div>
	<div align="center">
		<br>
		<a href="<?php echo site_url('login')?>"><?php echo $this->lang->line('masuk_sekarang')?></a>
		<br><br>
	</div>
</div>

<?php $this->load->view('partials/js')?>
</body>
</html>