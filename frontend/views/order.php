<div class="nav_order">
	<div class="container">
		<div class="page">2. <?php echo $this->lang->line('informasi_wisatawan')?></div>
		<div class="order_pos">
			<span class="selected">1. <?php echo $this->lang->line('pilih_pesan')?></span>
			<span class="selected">2. <?php echo $this->lang->line('data_wisatawan')?></span>
			<span>3. <?php echo $this->lang->line('bayar')?></span>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container" data-sticky_parent>
	<?php echo form_open(current_url().'?'.$_SERVER['QUERY_STRING'])?>
	<input type="hidden" name="destination_id" value="<?php echo $destination['id']?>">
	<input type="hidden" name="schedule_id" value="<?php echo $schedule->id?>">
	<input type="hidden" name="city_id" value="<?php echo $schedule_price->city_id?>">
	<input type="hidden" name="total" id="total" value="<?php echo $this->input->get('total')?>">
	<!-- s:detail_left -->
	<div class="detail_left">
		<h3 class="title3"><?php echo $this->lang->line('informasi_wisatawan')?></h3>
		<?php 
		$days = "";
		for ($i = 1; $i <= 31; $i++) { 
			$days .= '<option value="'.$i.'">'.$i.'</option>';
		}

		$month = "";
		for ($i = 1; $i <= 12; $i++) {
			$month .= '<option value="'.$i.'">'.$this->libglobal->monthString($i).'</option>';
		}

		$year = ""; 
		for ($i = 1950; $i <= date('Y'); $i++) {
			$year .= '<option value="'.$i.'">'.$i.'</option>';
		} ?>
		<?php for ($i = 1; $i <= $this->input->get('quantity'); $i++) { ?>
			<!-- s:box-info wisatawan -->
			<div class="box_wisatawan">
				<div class="t_box">
					<div class="ico"><img src="<?php echo assets_url('images/ico_dewasa.png')?>" alt=""></div>
					<?php echo $this->lang->line('wisatawan')?>
				</div>
				<div class="text">
					<div class="group-input">
						<label>Title</label>
						<div class="select-style">
							<select name="title[]" id="" required="">
								<option value=""><?php echo $this->lang->line('pilih_title')?></option>
								<option value="mr">Mr.</option>
								<option value="mrs">Mrs.</option>
								<option value="ms">Ms.</option>
							</select>
						</div>
					</div>
					<div class="group-input">
						<label><?php echo $this->lang->line('nama_depan')?></label>
						<input type="text" name="first_name[]" required="">
					</div>
					<div class="group-input">
						<label><?php echo $this->lang->line('nama_belakang')?></label>
						<input type="text" name="last_name[]"  required="">
					</div>
					<div class="group-input">
						<label><?php echo $this->lang->line('id_number')?></label>
						<input type="text" name="id_number[]"  required="">
					</div>
					<div class="group-input">
						<label>Email</label>
						<input type="text" name="email[]"  required="" data-validation="email">
					</div>
					<div class="group-input">
						<label><?php echo $this->lang->line('telepon')?></label>
						<input type="text" name="phone[]"  required="" data-validation="number">
					</div>
					<div class="group-input">
						<label><?php echo $this->lang->line('tanggal_lahir')?></label>
						<div class="select-style select3">
							<select name="birth_day[]" id="" class="" required="">
								<option value=""><?php echo $this->lang->line('hari')?></option>
								<?php echo $days?>
							</select>
						</div>
						<div class="select-style select3">
							<select name="birth_month[]" id="" class="" required="">
								<option value=""><?php echo $this->lang->line('bulan')?></option>
								<?php echo $month?>
							</select>
						</div>
						<div class="select-style select3">
							<select name="birth_year[]" id="" class="" required="">
								<option value=""><?php echo $this->lang->line('tahun')?></option>
								<?php echo $year?>
							</select>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<!-- e:box-info wisatawan -->
		<?php } ?>
		<?php if ($this->input->get('quantity') % 2) { ?>
			<!-- s:box-info wisatawan -->
			<!-- <div class="box_wisatawan">
				<div class="supplement">
					<label>
						<input type="checkbox" name="suplement" id="suplement" value="<?php echo $schedule_price->suplement?>">
						<div class="text">
							Single supplement
							<strong>(Rp.<?php echo price_format($schedule_price->suplement)?>,-)</strong>
						</div>
					</label>
					<div class="clearfix"></div>
					<?php echo $this->lang->line('info_supplement')?>
				</div>
				<div class="clearfix"></div>
			</div> -->
			<!-- e:box-info wisatawan -->
		<?php } ?>

		<div class="clearfix"></div>
		<br><br>
		<h3 class="title3"><?php echo $this->lang->line('informasi_kontak')?></h3>
		<div class="box_kontak">
			<div class="text">
				<div class="group-input">
					<label>E-mail</label>
					<input type="text" name="email_c" data-validation="email" required="">
				</div>
				<div class="group-input">
					<label><?php echo $this->lang->line('telepon_selular')?></label>
					<input type="text" name="phone_c" required="" data-validation="number">
				</div>
				<div class="group-input">
					<label><?php echo $this->lang->line('telepon_bisnis')?></label>
					<input type="text" name="phone_bc" data-validation="number">
				</div>
				<div class="t2"><?php echo $this->lang->line('kontak_darurat')?></div>
				<div class="clearfix"></div>
				<div class="group-input group-input5">
					<label><?php echo $this->lang->line('nama')?></label>
					<input type="text" name="name_ec">
				</div>
				<div class="group-input group-input5">
					<label><?php echo $this->lang->line('telepon')?></label>
					<input type="text" name="phone_ec" data-validation="number">
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="group-input tnc_box">
			<input type="checkbox" name="tnc" required=""> 
			<?php echo $this->lang->line('check_tnc')?>
		</div>
	</div>
	<!-- e:detail_left -->
	<!-- s:detail_right -->
	<div class="detail_right" data-sticky_column>
		<div class="box_">
			<div class="title2"><?php echo $this->lang->line('pesanan_anda')?></div>
			
			<div class="text">
				<div class="group-input">
					<div class="ico">
						<img src="<?php echo assets_url('images/ico_map.png')?>" alt="">
					</div>
					<label class="input-date input-date_full">
						<span><?php echo $this->lang->line('kota_keberangkatan')?></span>
						<?php echo $schedule_price->city_name?>
					</label>
					
					<div class="clearfix"></div>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="<?php echo assets_url('images/ico_time.png')?>" alt="">
					</div>
					<label class="input-date">
						<span><?php echo $this->lang->line('berangkat')?></span>
						<?php echo date('d-m-Y', strtotime($schedule->depart_date))?>
					</label>
					<label class="input-date">
						<span><?php echo $this->lang->line('kembali')?></span>
						<?php echo date('d-m-Y', strtotime($schedule->return_date))?>
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="<?php echo assets_url('images/ico_dewasa.png')?>" alt="">
					</div>
					<div class="info"><?php echo $this->lang->line('wisatawan')?></div>
					<div class="input_num">
						<input type='text' name='quantity' value='<?php echo $quantity?>' class='qty' readonly/>
					</div>
					<div class="clearfix"></div>
				</div>
				
			</div>
			<div class="total total2">
				<div class="num">
					TOTAL
					<strong id="total_cost">Rp.<?php echo price_format($this->input->get('total'))?>,-</strong>
					<br/><span id="include_suplement" style="display: none;">(include Supplement Rp.<?php echo price_format($schedule_price->suplement)?>,-)</span>
				</div>
				<div class="clearfix"></div>
			</div>
			<div align="center">
				<input type="submit" class="btn_submit" value="<?php echo strtoupper($this->lang->line('lanjut_bayar'))?>">
			</div>
		</div>
	</div>
	<!-- e:detail_right -->
	</form>
	<div class="clearfix"></div>
</div>

<div class="body_pop body_pop2" id="pop_box2">
	<div class="container_pop container_pop_bni offer_cc">
		<img src="<?php echo assets_url('images/btn_close.png')?>" alt="" class="close_box close_box_in">
		<a href="#"><img src="<?php echo assets_url('images/cc_bni2.jpg')?>" alt=""></a>
		<?php echo $this->lang->line('banner_promo')?>
	</div>
</div>
<script>
function GA_event() {
   ga('send', 'event', {
    eventCategory: 'bni_card_banner_popup',
    eventAction: 'click',
    eventLabel: event.target.href
  });
}
</script>