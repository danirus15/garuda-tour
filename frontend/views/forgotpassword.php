<!doctype html>	
<html>
<?php $this->load->view('partials/head')?>
</html>
<body class="body_pop">
<div class="container_pop">
	<a href="<?php echo site_url('')?>"><img src="<?php echo assets_url('images/btn_close.png')?>" alt="" class="close_pop close_box_in"></a>
	<div class="title_pop"><?php echo strtoupper($this->lang->line('lupa_password'))?></div>
	<?php echo form_open('')?>
		<div class="group-input">
			<label><?php echo strtoupper($this->lang->line('alamat_email'))?></label>
			<?php echo form_input('email',set_value('email'), array('data-validation'=>'email', 'placeholder' => 'Email Address', 'required' => 'true'))?>
            <?php echo form_error('email');?>
		</div>
		<div class="clearfix"></div>
		<div align="center">
			<input type="submit" value="KIRIM" class="btn">
			<div class="clearfix"></div>
			Sudah Terdaftar?<br>
			<a href="<?php echo site_url('login')?>"><?php echo $this->lang->line('masuk_sekarang')?></a>
		</div>

	</form>
</div>

<?php $this->load->view('partials/js')?>
</body>
</html>