<div class="blue_block">
	<div class="container container2">
		<h1>Selamat! Pesanan Anda Telah Kami Terima!</h1>
		<p>
		<b>Halo <?php echo $order['user_name']?>, </b>
		<br><br>
		Silahkan mengunjungi Sales Office kami di Senayan City, atau Kota Kasablanka, atau Living World Alam Sutera untuk melakukan pembayaran paket Ayo Liburan 2017. Harap menunjukkan email konfirmasi ini ketika kamu melakukan pembayaran.
		 <br><br>
		Pembayaran dapat kamu lakukan maksimal hingga satu hari setelah pemesanan (atau s/d jam tutup di tiap ticketing office pada hari tersebut) dengan menggunakan Kartu Kredit BNI Mastercard (BNI Mastercard Titanium, BNI Mastercard Silver, BNI Mastercard Gold, BNI Mastercard Platinum, dan BNI Mastercard World, termasuk Co-Branding).
		 <br><br>
		Harga belum termasuk potongan sebesar Rp500.000 untuk pemegang Kartu Kredit BNI Mastercard Titanium atau Rp250.000 untuk pemegang Kartu Kredit BNI Mastercard lainnya (BNI Mastercard World, BNI Mastercard Platinum, BNI Mastercard Gold, BNI Mastercard Silver, termasuk Co-Branding).
		Potongan akan dilakukan saat pembayaran di ticketing office kami yang telah ditunjuk.
		 <br><br>
		Kami tunggu kamu di trip #AyoLiburan bersama Garuda Indonesia & BNI Titanium Mastercard!
		</p>
	</div>
</div>
<div class="container container2">
	<h3>RINCIAN PESANAN</h3>
	<br>
	<div class="box_ box_2">
		<div class="text">
			<div class="title">
				<?php echo $order['destination_name']?>
				<span class="code">
					KODE PESANAN
					<strong><?php echo strtoupper($order['code'])?></strong>
				</span>
			</div>
			<div class="clearfix"></div>
			<div>
				<div>
					<b>Suplement:</b> <?php echo ($order['suplement']) ? 'Yes' : 'No'?>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="info_box">
				<div class="group-input info_time">
					<div class="ico">
						<img src="<?php echo assets_url('images/ico_time.png')?>" alt="">
					</div>
					<label class="input-date">
						<span>Berangkat</span>
						<?php echo $order['depart_date']?>
					</label>
					<label class="input-date">
						<span>Kembali</span>
						<?php echo $order['return_date']?>
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="group-input info_wis">
					<div class="ico">
						<img src="<?php echo assets_url('images/ico_dewasa.png')?>" alt="">
					</div>
					<div class="info"> <?php echo $order['total_tourist']?> Wisatawan</div>
					<div class="clearfix"></div>
				</div>
				<div class="info_total">
					TOTAL
					<strong>Rp.<?php echo price_format($order['total'])?>,-</strong>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="infowisatawan">
				<h3>Info Pemesan</h3>
				<div class="iw">
					<div class="isi">
						<div class="jdl">Nama</div>
						<div class="isi2"><?php echo $order['user_name']?></div>
						<div class="clearfix"></div>
						<div class="jdl">Email</div>
						<div class="isi2"><?php echo $order['user_email']?></div>
						<div class="clearfix"></div>
						<div class="jdl">No. Telepon</div>
						<div class="isi2"><?php echo $order['contact_phone']?></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<br><br>
				<h3>Info Wisatawan</h3>
				<?php $i = 1; ?>
				<?php foreach ($order['order_details'] as $order_detail) { ?>
					<div class="iw">
						<div class="no"><?php echo $i?></div>
						<div class="isi">
							<div class="jdl">Nama</div>
							<div class="isi2"><?php echo $order_detail['title']?>. <?php echo $order_detail['first_name']?> <?php echo $order_detail['last_name']?></div>
							<div class="clearfix"></div>
							<div class="jdl">Tanggal Lahir</div>
							<div class="isi2"><?php echo date('d M Y', strtotime($order_detail['birthdate']))?></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<?php $i++; ?>
				<?php } ?>
				
			</div>
			<br>
			<div class="clearfix"></div>
			<div class="line"></div>
			<div align="center">
				<a class="btn" onclick="window.print();">CETAK PESANAN</a>
				<a href="<?php echo site_url('/')?>" class="btn ">BACK TO HOME</a>
			</div>
		</div>
	</div>
</div>