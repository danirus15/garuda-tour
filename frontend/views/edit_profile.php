<!doctype html>	
<html>
<?php $this->load->view('partials/head')?>
</html>
<body class="body_pop">
<div class="container_pop">
	<img src="<?php echo assets_url('images/btn_close.png')?>" alt="" class="close_pop close_box_in">
	<div class="title_pop"><?php echo strtoupper($this->lang->line('edit_profile'))?></div>
	<?php echo validation_errors('<div class="notif">', '</div>'); ?>
	<?php if ($this->session->flashdata('error')) { ?>
		<div class="notif">
		  <?php echo $this->session->flashdata('error')?>
		</div>
	<?php } ?>
	<?php echo form_open('')?>
		<div class="group-input">
			<label><?php echo strtoupper($this->lang->line('nama'))?></label>
			<?php echo form_input('name', $row->name, array('placeholder' => $this->lang->line('name'), 'required' => 'true'))?>
            <?php echo form_error('name');?>
		</div>
		<div class="group-input">
			<label><?php echo strtoupper($this->lang->line('email'))?></label>
			<?php echo form_input('email',$row->email, array('data-validation'=>'email', 'placeholder' => $this->lang->line('alamat_email'), 'required' => 'true'))?>
            <?php echo form_error('email');?>
		</div>
		<div class="group-input">
			<label><?php echo $this->lang->line('telepon_selular')?></label>
			<?php echo form_input('mobile', $row->mobile, array('placeholder' => $this->lang->line('telepon_selular'), 'required' => 'true'))?>
            <?php echo form_error('mobile');?>
		</div>
		<div class="group-input">
			<label><?php echo $this->lang->line('telepon_bisnis')?></label>
			<?php echo form_input('phone',	$row->phone, array('placeholder' => $this->lang->line('telepon_selular'), 'required' => 'true'))?>
            <?php echo form_error('phone');?>
		</div>
		<div class="clearfix"></div>
		<div align="center">
			<input type="submit" value="<?php echo strtoupper($this->lang->line('simpan'))?>" class="btn">
		</div>

	</form>
</div>

<?php $this->load->view('partials/js')?>
</body>
</html>