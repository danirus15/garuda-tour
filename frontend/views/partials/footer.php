<div id="footer">
	<div class="container">
		<div class="foot_sosmed">
			<span>Find us on</span>
			<a href="https://www.facebook.com/garudaindonesia/" target="_blank"><img src="<?php echo assets_url('images/ico-fb.png')?>" alt=""></a>
			<a href="https://twitter.com/IndonesiaGaruda"  target="_blank"><img src="<?php echo assets_url('images/ico-tw.png')?>" alt=""></a>
			<a href="https://www.instagram.com/garuda.indonesia/?hl=id" target="_blank"><img src="<?php echo assets_url('images/ico-ig.png')?>" alt=""></a>
			<a href="https://www.youtube.com/user/GarudaIndonesia1949" target="_blank"><img src="<?php echo assets_url('images/ico-youtube.png')?>" alt=""></a>
		</div>
		<div class="fl">
			<div class="logo_foot">
				<a href="#" class="fl" alt=""><img src="<?php echo assets_url('images/logo_footer.png')?>" alt=""></a>
				<a href="<?php echo site_url('helpcenter')?>" class="hc">Help Center</a>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
			Copyright of Garuda Indonesia @<?php echo date('Y')?> - Allright reserved
		</div>
		
		<div class="clearfix"></div>
	</div>
</div>