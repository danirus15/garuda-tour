<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="Penawaran Paket Liburan Domestik Terbaik bersama Garuda Indonesia & BNI Mastercard">
<link rel="shortcut icon" href="<?php echo assets_url('images/favicon.png')?>">
<link rel="apple-touch-icon" href="<?php echo assets_url('images/favicon.png')?>"/> 
<title>Garuda Indonesia-Ayo Liburan</title>
<link href="<?php echo assets_url('css/style.css')?>" rel="stylesheet" type="text/css" charset="utf-8"/>
<link href="<?php echo assets_url('js/jquery.fancybox.min.css')?>" rel="stylesheet" type="text/css" charset="utf-8"/>
<script type="text/javascript" src="<?php echo assets_url('js/jquery.min.js')?>"></script>
<script type="text/javascript" src="<?php echo assets_url('js/jquery.fancybox.min.js')?>"></script>
<script src="https://api.midtrans.com/v2/assets/js/midtrans.min.js" type="text/javascript"></script>
