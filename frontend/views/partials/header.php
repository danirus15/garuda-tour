<header>
	<div class="container">
		<a href="<?php echo site_url('/')?>"><img src="<?php echo assets_url('images/logo_ayo2_small.png')?>" alt="" class="logo"></a>
		<a href="<?php echo site_url('/')?>"><img src="<?php echo assets_url('images/logo_ayo2_small.png')?>" alt="" class="logo_ayo"></a>
		<div class="language">
			<img src="<?php echo assets_url('images/ico_language.png')?>" alt="">
			<span>
				<?php if ($this->session->userdata('lang') == 'en') { ?>
					EN
				<?php } else { ?>
					ID
				<?php } ?>
			</span>
			<img src="<?php echo assets_url('images/ico_expand.png')?>" alt="" class="arrow">
			<div class="box_bahasa">
				<a href="<?php echo site_url('language/id?callback='.urlencode(uri_string().'?'.$_SERVER['QUERY_STRING']))?>">Bahasa Indonesia</a>
				<a href="<?php echo site_url('language/en?callback='.urlencode(uri_string().'?'.$_SERVER['QUERY_STRING']))?>">English</a>
			</div>
		</div>
		<div class="user_login">
			<?php if ($this->session->userdata('logged_in')) { ?>
				Hello 
				<a href="<?php echo site_url('dashboard')?>""><b><?php echo $this->session->userdata('name')?></b></a> /  
				<a href="<?php echo site_url('logout')?>""><?php echo $this->lang->line('keluar')?></a>
			<?php } else{ ?>
				<a href="<?php echo site_url('login')?>" alt="<?php echo site_url('login?callback='.uri_string())?>" class="box_modal_full"><?php echo $this->lang->line('masuk')?></a> /
				<a href="<?php echo site_url('register')?>" alt="<?php echo site_url('register')?>" class="box_modal_full"><?php echo $this->lang->line('daftar')?></a>
			<?php } ?>
		</div>
	</div>
</header>