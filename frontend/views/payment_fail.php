<div class="white_block">
	<div class="container container2">
		<h1><?php echo $this->lang->line('payment_fail_title')?></h1>
		<?php if ($this->session->flashdata('error')) { ?>
			<b><i><?php echo $this->session->flashdata('error')?></i></b><br>
		<?php } ?>
		<p><?php echo str_replace('{name}', $this->session->userdata('name'), $this->lang->line('payment_fail_body'));?></p>

		<br>
		<a href="<?php echo site_url('/')?>" class="btn"><?php echo $this->lang->line('back_to_home')?></a>
	</div>
</div>