<!doctype html>	
<html>
<head>
	<?php $this->load->view('partials/head')?>	
</head>
<?php
if ($this->uri->segment(1) == 'welcome' || $this->uri->segment(1) == null) {
	echo "<body class=\"home\">";
} else {
	echo "<body>";
}
?>
<?php $this->load->view('partials/header')?>
<?php
if (isset($content) && !empty($content)) {
  $this->load->view($content);
}
?>
<div class="clearfix"></div>
<?php $this->load->view('partials/footer')?>
<?php $this->load->view('partials/js')?>
</body>
</html>