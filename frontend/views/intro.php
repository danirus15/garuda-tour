<!doctype html>	
<html>
<?php $this->load->view('partials/head')?>	
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-102475055-1', 'auto');
  ga('send', 'pageview');

</script>
<style>
	.counting {
		width: 430px;
		position: absolute;
		z-index: 2;
		left: calc(50% - 210px);
		top: 50%;
		color: #fff;
		font-size: 20px;
		text-align: center;
	}
	.defaultCountdown {
		text-align: center;
	}
	.countdown-section {
		float: left;
		margin: 10px 20px;
	}
	.countdown-section .countdown-amount,
	.countdown-section .countdown-period {
		display: block;
		text-align: center;
	}
	.countdown-section .countdown-amount {
		font-weight: bold;
		font-size: 48px;
		line-height: 120%;
	}
	.countdown-section .countdown-period {
		text-transform: ;
	}
	.home header .logo {
		left: calc(50% - 200px);
	}
	.home header.smaller .logo {
		left: calc(50% - 50px);
	}
	.list1 .more {
		background: grey !important;
	}
	@media screen and (max-width: 650px) {
		.counting {
			width: 300px;
			left: calc(50% - 135px);
			top: 40%;
			font-size: 16px;
		}
		.countdown-section {
			margin: 10px;
		}
		.countdown-section .countdown-amount {
			font-size: 32px;
		}
	}
</style>
<body class="home">
<header>
	<div class="container">
		<a href="index.php"><img src="<?php echo assets_url('images/logo_ayo2_small.png')?>" alt="" class="logo"></a>
		<img src="<?php echo assets_url('images/logo_ayo2_small.png')?>" alt="" class="logo_ayo">
		
	</div>
</header>
<!-- s:cover -->
<section id="cover">
	<div id="slide_cover">
		<div class="ratio16_9 box_img">
			<a href="#">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="<?php echo assets_url('images/logo_ayo2.png')?>" alt="">
				</div>
				<div class="img_con lqd">
					<img src="<?php echo assets_url('images/c2.jpg')?>" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="#">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="<?php echo assets_url('images/logo_ayo2.png')?>" alt="">
				</div>
				<div class="img_con lqd">
					<img src="<?php echo assets_url('images/c1.jpg')?>" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="#">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="<?php echo assets_url('images/logo_ayo2.png')?>" alt="">
				</div>
				<div class="img_con lqd">
					<img src="<?php echo assets_url('images/c3.jpg')?>" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="#">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="<?php echo assets_url('images/logo_ayo2.png')?>" alt="">
				</div>
				<div class="img_con lqd">
					<img src="<?php echo assets_url('images/c4.jpg')?>" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="#">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="<?php echo assets_url('images/logo_ayo2.png')?>" alt="">
				</div>
				<div class="img_con lqd">
					<img src="<?php echo assets_url('images/c5.jpg')?>" alt="" class="bg">
				</div>
			</a>
		</div>
	</div>
	<img src="<?php echo assets_url('images/logo_5star_color.png')?>" alt="" class="bni_logo">
	<img src="<?php echo assets_url('images/bni_pesona.png')?>" class="bni_card box_modal_full" alt="bni_card.php">
	<div class="bgfloat2"></div>
	<a href="#intro" class="more">
		<!-- EXPLORE MORE<br> -->
		<img src="<?php echo assets_url('images/expand more.png')?>" alt="">
	</a>
	<div class="counting">
		<h3>COMING SOON</h3>
		<div id="defaultCountdown"></div>
	</div>
	
</section>

<!-- e:cover -->
<div class="container_full">
<!-- s:intro -->
<section id="intro" class="section">
	<div class="container">
		<h2 class="title"><span style="color: #FCB03B !important;">#Ayo</span><span style="color: #008C9A !important;">Liburan</span> bersama Garuda Indonesia</h2>
		<p>Mau ngerasain serunya piknik di berbagai destinasi keren di Indonesia? #AyoLiburan bareng Garuda Indonesia!
</p>
<p>
Piknik di pantai dengan <i>view</i> menakjubkan di <b>Kepulauan Komodo</b>, atau di salah satu pulau cantik berpasir putih di <b>Belitung</b>, atau di tepi sungai di <b>Malang</b>, atau di tengah <i>savannah ala Africa</i> di <b>Banyuwangi</b>, atau di pulau dengan laut biru <i>tosca</i> yang <i>instagrammable</i> di <b>Padang</b>, tinggal kamu pilih sesuai selera di paket perjalanan seru #AyoLiburan.
</p>
<p>
Selain bisa menikmati alam Indonesia yang nggak kalah keren sama  destinasi luar negeri, kamu bisa mencicipi sajian kuliner khas Nusantara dengan ditemani para <i>Travel Influencers</i> yang akan bikin liburan kamu makin tak terlupakan.
</p>
<p>
Yuk pesan petualangan seru #AyoLiburan sekarang!
</p>
</div>
</section>
<!-- e:intro -->
<!-- s:list -->

<div class="container">
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/e895301bcfa4b5b778f22f0077f1469e.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Chasing Sunrise in Bromo</h2>
			<div class="place">Malang & Bromo 3H2M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp.9.775.600,-</strong>
				<strong>Rp.4.365.000,- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a class="more">#AyoLiburan</a>
			
		</div>
	</article>
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/10/fe9045d6780b9b808be20c01e695bf09.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Starfish & The Powdery Sand</h2>
			<div class="place">Belitung 3H2M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp.8.525.000,-</strong>
				<strong>Rp.3.070.000,- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a class="more">#AyoLiburan</a>
			
		</div>
	</article>
	
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/a18e6b63be6bc5af9da56f11fa6d3ab5.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Sailing the Magnificent Komodo Islands</h2>
			<div class="place">Labuan Bajo & Kep. Komodo 4H3M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp.12.747.000,-</strong>
				<strong>Rp.5.625.000- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a class="more">#AyoLiburan</a>
			
		</div>
	</article>
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="<?php echo assets_url('images/img_padang.jpg')?>" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Picnic Lunch in Pasumpahan Island</h2>
			<div class="place">Padang 3H2M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp.12.460.000,-</strong>
				<strong>Rp.3.880.000,- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a class="more">#AyoLiburan</a>
			
		</div>
	</article>
	
	<article class="list1">
		<div class="box_img ratio_box ">
		
			<div class="img_con lqd">
				<img src="<?php echo assets_url('images/img_banyuwangi.jpg')?>" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Escape to The Blue Flames</h2>
			<div class="place">Banyuwangi 3H2M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp.11.901.600,-</strong>
				<strong>Rp.5.805.000,- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a class="more">#AyoLiburan</a>
			
		</div>
	</article>
	
</div>
<div class="clearfix"></div>
</div>

<!-- e:list -->
<div class="clearfix"></div>
<div id="footer">
	<div class="container">
		<div class="foot_sosmed">
			<span>Find us on</span>
			<a href="https://www.facebook.com/garudaindonesia/" target="_blank"><img src="<?php echo assets_url('images/ico-fb.png')?>" alt=""></a>
			<a href="https://twitter.com/IndonesiaGaruda" target="_blank"><img src="<?php echo assets_url('images/ico-tw.png')?>" alt=""></a>
			<a href="https://www.instagram.com/garuda.indonesia/?hl=id" target="_blank"><img src="<?php echo assets_url('images/ico-ig.png')?>" alt=""></a>
			<a href="https://www.youtube.com/user/GarudaIndonesia1949" target="_blank"><img src="<?php echo assets_url('images/ico-youtube.png')?>" alt=""></a>
		</div>
		<div class="fl">
			<a href="#" class="box_modal_full" alt="bni_card.php"><img src="<?php echo assets_url('images/logo_footer.png')?>" alt=""></a>
			Copyright of Garuda Indonesia @2017 - Allright reserved
		</div>
		
		<div class="clearfix"></div>
	</div>
</div>
<div class="banner_float">
	<a class="banner_close"><img src="<?php echo assets_url('images/btn_close.png')?>" alt=""></a>
	<a href="https://eform.bni.co.id/BNI_eForm/informasiPersyaratan?option_cc=umum" target="_blank"><img src="<?php echo assets_url('images/bni_promo2.jpg')?>" alt=""></a>
</div>
<script type="text/javascript" src="<?php echo assets_url('js/jquery.min.js')?>"></script>
<script type="text/javascript" src="<?php echo assets_url('js/jquery.carouFredSel-6.0.4-packed.js')?>"></script>
<script type="text/javascript" src="<?php echo assets_url('js/liquid.js')?>"></script>
<script type="text/javascript" src="<?php echo assets_url('js/header.js')?>"></script>
<script type="text/javascript" src="<?php echo assets_url('js/jquery.plugin.min.js')?>"></script>
<script type="text/javascript" src="<?php echo assets_url('js/jquery.countdown.min.js')?>"></script>
<script type="text/javascript" src="<?php echo assets_url('js/controller.js')?>"></script>

<script>

$('#defaultCountdown').countdown({until: new Date(2017, 7-1, 21)});

$('.banner_close').click(function(){
	$('.banner_float').remove();
});
</script>


</body>
</html>