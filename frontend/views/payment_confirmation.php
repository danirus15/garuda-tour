<div class="nav_order">
	<div class="container">
		<div class="page">3. Konfirmasi & Bayar</div>
		<div class="order_pos">
			<span class="selected">1. Pilih & Pesan</span>
			<span class="selected">2. Data Wisatawan</span>
			<span class="selected">3. Konfirmasi & Bayar</span>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container">
	<!-- s:detail_right -->
	<div class="detail_right detail_konfirmasi">
		<div id="result-json"></div>
		<div class="box_">
			<div class="title2">Konfirmasi Pesanan Anda</div>
			<div class="text">
				<div class="group-input">
					Tujuan
					<h2><?php echo $destination['destination_'.active_language()]?></h2>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="<?php echo assets_url('images/ico_time.png')?>" alt="">
					</div>
					<label class="input-date">
						<span><?php echo $this->lang->line('berangkat')?></span>
						<?php echo date('d-m-Y', strtotime($order->depart_date))?>
					</label>
					<label class="input-date">
						<span><?php echo $this->lang->line('kembali')?></span>
						<?php echo date('d-m-Y', strtotime($order->return_date))?>
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="<?php echo assets_url('images/ico_dewasa.png')?>" alt="">
					</div>
					<div class="info">Wisatawan</div>
					<div class="input_num">
						<input type='text' name='quantity' value='<?php echo count($order_detail)?>' class='qty' disabled/>
					</div>
					<div class="clearfix"></div>
					<ol class="list_nama">
						<?php foreach ($order_detail as $detail) { ?>
							<li><?php echo ucfirst($detail['title'])?>. <?php echo ucwords($detail['first_name'].' '.$detail['last_name'])?></li>
						<?php } ?>
					</ol>
				</div>
			</div>
			<?php 
			$total = (($order->qty_promo * $order->price_promo) + ($order->qty * $order->price) + $order->suplement) - (($order->qty + $order->qty_promo) * $order->cashback);
			?>
			<div class="text simulasi_cicil">
				<h3>Simulasi Cicilan</h3>
				<div class="fl">6 Bulan Bunga 0%</div>
				<div class="fr">Rp.<?php echo price_format($total / 6)?></div>
				<div class="clearfix"></div>
				<div class="fl">12 Bulan Bunga 0%</div>
				<div class="fr">Rp.<?php echo price_format($total / 12)?></div>
				<div class="clearfix"></div>
			</div>
			<div class="total total2">
				<div class="num">
					TOTAL 
					<b>Rp.<?php echo price_format((($order->qty_promo * $order->price_promo) + ($order->qty * $order->price) + $order->suplement))?>,-</b>
					Discount
					<b>Rp.<?php echo price_format($order->qty * $order->cashback)?>,-</b>
					TOTAL PAYMENT
					<strong>Rp.<?php echo price_format($total)?>,-</strong>
				</div>
				<div class="clearfix"></div>
			</div>
			<div align="center">
				<input type="button" class="btn_submit" id='pay-button' value="KONFIRMASI PEMBAYARAN">
			</div>
		</div>
	</div>
	<!-- e:detail_right -->
	
	<div class="clearfix"></div>
	<br>
</div>
<div class="loading_page" id="loading_page" style="display: none;">
	<div class="loader">
	    <span></span>
	    <span></span>
	    <span></span>
	    <br><br>
	    Proses pembayaran sedang berlangsung. Jangan tutup browser ini. Harap tunggu.
	</div>
</div>
<script src="<?php echo getenv('MIDTRANS_URL')?>/snap/snap.js" data-client-key="<?php echo getenv('MIDTRANS_CLIENT_KEY')?>"></script>
<script type="text/javascript">
	var csrfName = '<?php echo $this->security->get_csrf_token_name(); ?>',
    	csrfHash = '<?php echo $this->security->get_csrf_hash(); ?>';
  	document.getElementById('pay-button').onclick = function(){
    	// SnapToken acquired from previous step
    	snap.pay('<?php echo $snap_token?>', {
      	// Optional
      	onSuccess: function(result){
      		$('#loading_page').show();
        	if (result.status_code == '200' && result.transaction_status == 'capture') {
        		$.post( "<?php echo site_url('payment/status')?>", {
        			'order_id': result.order_id,
        			[csrfName]: csrfHash
        		}).done(function(data) {
        			window.location = "<?php echo site_url('payment/success')?>?order_id="+result.order_id;
        		}); 
        	} else {
        		alert(result.status_message);
        	}
      	},
      // Optional
      onPending: function(result){
        console.log(result);
      },
      // Optional
      onError: function(result){
        console.log(result);
      }
    });
  };
</script>