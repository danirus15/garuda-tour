<!doctype html>	
<html>
	<?php $this->load->view('partials/head')?>
</html>
<body class="body_pop">
<div class="container_pop">
	<a href="<?php echo site_url('dashboard')?>"><img src="<?php echo assets_url('images/btn_close.png')?>" alt="" class="close_pop close_box_in"></a>
	<div class="title_pop"><?php echo $this->lang->line('change_password_success')?></div>
	<div class="group-input" align="center">
		<?php echo $this->lang->line('change_password_message')?>.
	</div>
	<div class="clearfix"></div>
</div>

<?php $this->load->view('partials/js')?>
</body>
</html>