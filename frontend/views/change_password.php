<!doctype html>	
<html>
<?php $this->load->view('partials/head')?>
</html>
<body class="body_pop">
<div class="container_pop">
	<a href="<?php echo site_url('dashboard')?>"><img src="<?php echo assets_url('images/btn_close.png')?>" alt="" class="close_pop close_box_in"></a>
	<div class="title_pop"><?php echo strtoupper($this->lang->line('ganti_password'))?></div>
	<?php echo validation_errors('<div class="notif">', '</div>'); ?>
	<?php if ($this->session->flashdata('error')) { ?>
		<div class="notif">
		  <?php echo $this->session->flashdata('error')?>
		</div>
	<?php } ?>
	<?php echo form_open('')?>
		<div class="group-input">
			<label><?php echo strtoupper($this->lang->line('current_password'))?></label>
			<?php echo form_password('current_password','', array('required'=>'true', 'placeholder' => $this->lang->line('current_password')))?>
		</div>
		<div class="group-input">
			<label><?php echo strtoupper($this->lang->line('new_password'))?></label>
			<?php echo form_password('new_password','', array('required'=>'true', 'placeholder' => $this->lang->line('new_password')))?>
          
		</div>
		<div class="group-input">
			<label><?php echo strtoupper($this->lang->line('re_password'))?></label>
			<?php echo form_password('re_password','', array('required'=>'true', 'placeholder' => $this->lang->line('re_password')))?>
		</div>
		<div class="clearfix"></div>
		<div align="center">
			<input type="submit" value="<?php echo strtoupper($this->lang->line('simpan'))?>" class="btn">
		</div>

	</form>
</div>

<?php $this->load->view('partials/js')?>
</body>
</html>