<!-- s:cover -->
<section id="cover">
	<div id="slide_cover">
		<?php foreach ($cover_images as $row) { ?>
			<div class="ratio16_9 box_img">
				<a href="<?php echo ($row['link']) ? $row['link'] : '' ?>">
					<?php if ($row['is_logo_exist']) { ?>
						<div class="text">
							<img src="<?php echo assets_url('images/logo_ayo2.png')?>" alt="">
						</div>
					<?php } ?>
					<div class="img_con lqd">
						<img src="<?php echo image_url($row['photo'])?>" alt="" class="bg">
					</div>
				</a>
			</div>
		<?php } ?>
	</div>
	<img src="<?php echo assets_url('images/arrow_left.png')?>" alt="" class="nav_cover nav_left">
	<img src="<?php echo assets_url('images/arrow_right.png')?>" alt="" class="nav_cover nav_right">
	<img src="<?php echo assets_url('images/logo_5star_color.png')?>" alt="" class="bni_logo">
	<img src="<?php echo assets_url('images/bni_pesona.png')?>" class="bni_card box_modal_full" alt="bni_card.php">
	<a href="#intro" class="more">
		<?php echo strtoupper($this->lang->line('lihat_lebih'))?><br>
		<img src="<?php echo assets_url('images/expand more.png')?>" alt="">
	</a>
</section>
<!-- e:cover -->
<!-- s:intro -->
<section id="intro" class="section">
	<div class="container">
		<h2 class="title"><?php echo $this->lang->line('title_home')?></h2>
		<?php echo $static_wording['description']?>
	</div>
</section>
<!-- e:intro -->
<!-- s:list -->
<div class="container">
	<?php if ($paketliburan) { 
		$CI =& get_instance();
		?>
		<?php foreach ($paketliburan as $record) { ?>
			<article class="list1">
				<div class="box_img ratio_box ">
					<div class="img_con lqd">
						<img src="<?php echo image_url($record['photo'])?>" alt="">
					</div>
					<div class="bgfloat"></div>
				</div>
				<div class="text">
					<h2><?php echo $record['destination_'.active_language()]?></h2>
					<div class="place"><?php echo $record['title_'.active_language()]?></div>
					<div class="clearfix"></div>
					<div class="price">
						<span><?php echo $this->lang->line('harga_mulai_dari')?></span>
						<strong class="pc2">Rp <?php echo price_format($CI->getDestinationPrice($record['id'])['price_normal'])?></strong>
						<strong>Rp <?php echo price_format($CI->getDestinationPrice($record['id'])['promo_seat_discount'])?> *</strong>
						<div class="note"><?php echo $this->lang->line('syarat_ketentuan')?></div>
					</div>
					<a href="<?php echo site_url('destination/detail/'.$record['id'].'/'.url_title(strtolower($record['title_'.active_language()])))?>" class="more"><?php echo $this->lang->line('lihat_rincian')?></a>
					
				</div>
				<?php if ($record['is_sold_out']) { ?>
					<div class="soldout_now">SOLD OUT</div>
				<?php } ?>
			</article>
		<?php } ?>
	<?php } ?>
</div>
<!-- e:list -->
<div class="clearfix"></div>
<div class="container banner_bottom">
	<a href="https://www.garuda-indonesia.com/id/id/special-offers/sales-promotion/ayo-liburan.page" target="_blank"><img src="<?php echo assets_url('images/Banner-FIX-fixed.jpg')?>" alt=""></a>
</div>
<div class="banner_float">
	<a class="banner_close"><img src="<?php echo assets_url('images/btn_close.png')?>" alt=""></a>
	<a href="https://eform.bni.co.id/BNI_eForm/informasiPersyaratan?option_cc=umum" target="_blank" onclick="ga('send', 'event', 'bni_card_banner_floating', 'Click', 'BNI Credit Card Form Apply', '0');"><img src="<?php echo assets_url('images/bni_promo.jpg')?>" alt=""></a>
</div>