<section id="cover">
	<div id="slide_cover">
		<div class="ratio3_1 box_img">
			<div class="img_con lqd">
				<img src="<?php echo image_url($destination['photo'])?>" alt="">
			</div>
		</div>
		<?php foreach ($gallery as $image) { ?>
			<div class="ratio3_1 box_img">
				<div class="img_con lqd">
					<img src="<?php echo image_url($image['photo'])?>" alt="">
				</div>
			</div>
			</a>
		<?php } ?>
	</div>
	<img src="<?php echo assets_url('images/arrow_left.png')?>" alt="" class="nav_cover nav_left">
	<img src="<?php echo assets_url('images/arrow_right.png')?>" alt="" class="nav_cover nav_right">
</section>
<br><br>
<div class="container" data-sticky_parent>
	<!-- s:detail_left -->
	<div class="detail_left">
		<h1><?php echo $destination['title_'.active_language()]?></h1>
		<h2><?php echo $this->lang->line('galeri')?></h2>
		<div class="gal_img">
			<?php foreach ($gallery as $image) { ?>
				<a class="box_img ratio4_3 " data-fancybox="images" href="<?php echo image_url($image['photo'])?>">
					<div class="img_con lqd">
						<img src="<?php echo image_url($image['photo'])?>" alt="">
					</div>
				</a>
			<?php } ?>
		</div>
		<div class="clearfix"></div>
		<h2><?php echo $this->lang->line('rencana_perjalanan')?></h2>
		<div class="list_rp">
			<?php echo $destination['trip_plan_'.active_language()]?>
		</div>
		<h2><?php echo $this->lang->line('rincian')?></h2>
		<div class="desc">
			<?php echo $destination['summary_'.active_language()]?>
		</div>
		<h2><?php echo $this->lang->line('deskripsi')?></h2>
		<div class="desc">
			<?php echo $destination['description_'.active_language()]?>
		</div>
	</div>
	<!-- e:detail_left -->
	<!-- s:detail_right -->
	<div class="detail_right" data-sticky_column>
		<div class="box_">
			<?php echo form_open(site_url('destination/ordersnap'), array('id' => 'form_order', 'method' => 'get'))?>
			<input type="hidden" name="destination_id" id="destination_id" value="<?php echo $destination['id']?>">
			<input type="hidden" name="schedule_id" id="schedule_id" value="">
			<input type="hidden" name="total" id="total" value="">
			<div class="text">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="notif m10">
						<?php echo $this->session->flashdata('error')?>
					</div>
				<?php } ?>
				<div class="title"><?php echo $destination['title_'.active_language()]?></div>
				<div class="group-input">
					<div class="ico">
						<img src="<?php echo assets_url('images/ico_map.png')?>" alt="">
					</div>
					<label class="input-date input-date_full">
						<span><?php echo $this->lang->line('kota_keberangkatan')?></span>
						Jakarta
						<div class="select-style dnone">
							<select name="city_id" id="city" required="">
								<!-- <option value=""><?php echo $this->lang->line('pilih_kota')?></option> -->
								<?php foreach ($cities as $city) {?>
									<option value="<?php echo $city['id']?>" selected><?php echo $city['name'] ?></option>
								<?php } ?>
							</select>
						</div>
					</label>
					
					<div class="clearfix"></div>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="<?php echo assets_url('images/ico_time.png')?>" alt="">
					</div>
					<label class="input-date">
						<span>Berangkat</span>
						<input type="text" class="pilihtanggal" placeholder="Pilih tanggal" id="from_date" required>
					</label>
					<label class="input-date">
						<span>Kembali</span>
						<input type="text" disabled id="to_date">
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="<?php echo assets_url('images/ico_dewasa.png')?>" alt="">
					</div>
					<div class="info"><?php echo $this->lang->line('wisatawan')?></div>
					<div class="input_num">
						 <input id="btn_min" type='button' value='-' class='qtyminus' field='quantity' />
					    <input type='text' name='quantity' id="quantity" value='0' class='qty' readonly="true" required />
					    <input id="btn_plus" type='button' value='+' class='qtyplus' field='quantity' />
					</div>
					<div class="clearfix"></div>
				</div>
				
			</div>
			<div class="price">
				<?php echo strtoupper($this->lang->line('harga'))?>
				<div class="price2">
					<span id="total_promo_seat">0</span> <?php echo $this->lang->line('pemesan_pertama')?>
					<div class="status" id="sisa_promo"><?php echo $this->lang->line('sisa')?> <span id="sisa_promo_number">-</span></div>
					<div class="status soldout" id="soldout_promo" style="display: none;"><?php echo strtoupper($this->lang->line('habis'))?></div>
					<strong id="promo_seat_discount">Rp</strong>
				</div>
				<div class="price2">
					<?php echo ucfirst($this->lang->line('harga_normal'))?>
					<div class="status" id="sisa_normal"><?php echo $this->lang->line('sisa')?> <span id="sisa_normal_number">-</span></div>
					<div class="status soldout" id="soldout_normal" style="display: none;"><?php echo strtoupper($this->lang->line('habis'))?></div>
					<strong id="normal_price">Rp</strong>
				</div>
				<div class="bni_price">
					Diskon <b>Rp 500.000</b> <br>
					untuk Mastercard BNI Style Titanium <br>
					dan <b>Rp 250.000</b><br>
					untuk BNI Mastercard lainnya
				</div>
			</div>
			<div class="total">
				<div class="num">
					TOTAL
					<strong class="total_cost" id="total_cost">Rp</strong>
					<!-- <span id="summary">(- <?php echo $this->lang->line('wisatawan')?> x Rp)</span> -->
				</div>
				
				<div class="clearfix"></div>
			</div>
			<div align="center">
				<input type="submit" class="btn_submit" value="<?php echo strtoupper($this->lang->line('pesan_sekarang'))?>">
			</div>
			</form>
		</div>
	</div>
	<!-- e:detail_right -->
	<div class="clearfix"></div>
</div>
<script type="text/javascript">
$(window).load(function() {
    $('form').get(0).reset(); //clear form data on page load
});
var url_schedules = '<?php echo site_url('destination/schedules')?>';
var max_tourist = <?php echo getenv('MAX_TOURIST'); ?>;
<?php 
$depart_dates = "";
$schedules_id = "";
foreach ($schedules as $schedule) {
	$depart_dates .= ($depart_dates) ? ',"'.$schedule['depart_date'].'"' : '"'.$schedule['depart_date'].'"';
	$schedules_id .= ($schedules_id) ? ',"'.$schedule['id'].'"' : '"'.$schedule['id'].'"';
} 
?>
var sch = [<?php echo $schedules_id?>];
var array = [<?php echo $depart_dates?>];
var total_days = <?php echo $destination['total_days']?>;
</script>
