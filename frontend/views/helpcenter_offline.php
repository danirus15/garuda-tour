<style>
	ol li{
		padding: 3px 0;
		line-height: 130%;
	}
	ol li p {
		margin: 0;
	}
	.container_hc {
		padding: 0 30px;
	}
	.faq p strong {
		font-size: 18px;
		line-height: 130%;
	}
</style>
<div class="container container_hc">
	<br><br>
	<div class="title_help"><h1>Help Center</h1></div>
	<div id="bahasa"></div>
	<div class="link_">
		<a href="#bahasa" class="selected">Bahasa Indonesia</a>
		<a href="#english">English</a>
	</div>
	
	<div class="title_help2">Hubungi Kami</div>
	<div class="text">
		Apakah kamu memiliki pertanyaan lebih lanjut dan membutuhkan bantuan lainnya? Kamu dapat menghubungi kami di 0822-1140-1146 dari pukul 09:00 - 17:00 atau email kami di ayoliburan@garuda-indonesia.com. Tim kami akan siap membantumu!

	</div>
	<div class="title_help2">FAQ</div>
	<div class="text faq">
		<p dir="ltr"><strong>1. Bagaimana Proses pemesanan paket wisata Ayo Liburan 2017?</strong></p>
		<p dir="ltr">Pemesanan dan pembelian paket wisata Ayo Liburan 2017 bisa kamu lakukan secara eksklusif di website resmi Ayo Liburan - Garuda Indonesia di ayoliburan.garuda-indonesia.com & menggunakan Kartu Kredit BNI Mastercard, dan melakukan pembayaran pada Kantor Penjualan Tiket Garuda Indonesia yang telah kami tunjuk (lokasi dan jam operasi kantor tertera di halaman ini)
</p>
		<p dir="ltr"><strong>2. Kapan maksimal waktu sebelum keberangkatan,saya dapat melakukan proses pemesanan dan pembelian paket wisata Ayo Liburan 2017 ?</strong></p>
		<p dir="ltr">Pemesanan dan pembelian tiket paket wisata Ayo Liburan 2017, dapat dilakukan 8 (delapan) hari sebelum periode perjalanan untuk destinasi Malang, Belitung, Labuan Bajo, dan Banyuwangi. Maksimum pembelian paket wisata Garuda Indonesia-AyoLiburan 2017 dapat dilakukan 15 (lima belas) hari sebelum periode perjalanan untuk destinasi Padang.</p>
		<p dir="ltr"><strong>3. Bagaimana saya melakukan pembayaran untuk pesanan saya?</strong></p>
		<p dir="ltr">Pembayaran dapat kamu lakukan melalui website resmi ayoliburan.garuda-indonesia.com dengan menggunakan Kartu Kredit BNI Mastercard.</p>
		<p dir="ltr"><strong>4. Metode pembayaran apa saja yang diterima dalam pemesanan paket Ayo Liburan 2017?</strong></p>
		<p dir="ltr">Ayo Liburan 2017 hanya menerima metode pembayaran melalui Kartu Kredit Mastercard BNI saja</p>
		<p dir="ltr"><strong>5. Mengapa transaksi saya tidak berhasil?</strong></p>
		<p dir="ltr">Jangan khawatir, hal ini biasa terjadi. Transaksi bisa saja tidak berhasil dikarenakan beberapa hal. Kegagalan transaksi dapat terjadi dikarenakan adanya masalah pada kartu kredit atau kesalahan teknis. Anda dapat memeriksa kembali kelengkapan dokumen pembayaran serta detail kartu kredit dan mengulang proses pembayaran Anda. Silahkan hubungi call center kami di 0822-1140-1146 atau mengirimkan email melalui ayoliburan@garuda-indonesia.com jika Anda membutuhkan bantuan lainnya.</p>
		<p dir="ltr"><strong>6. Apakah terdapat jumlah minimum dan maksimum pembelian paket Ayo Liburan 2017 dalam satu reservasi menggunakan 1 (satu) Kartu Kredit?</strong></p>
		<p dir="ltr">Kamu dapat melakukan pembelian Paket Ayo Liburan 2017 dengan maksimum pembelian untuk 8 (delapan) orang dalam satu reservasi menggunakan 1 (satu) Kartu Kredit BNI Mastercard.</p>
		<p dir="ltr"><strong>7. Apakah pembelian paket wisata untuk anak-anak atau bayi akan mendapatkan harga khusus?</strong></p>
		<p dir="ltr">Tidak. Pembelian paket wisata untuk anak-anak akan dikenakan harga normal.</p>
		<p dir="ltr"><strong>8. Apakah pembelian paket wisata untuk usia lanjut (60 tahun keatas) akan mendapatkan harga khusus?</strong></p>
		<p dir="ltr">Tidak. Pembelian paket wisata untuk usia lanjut akan dikenakan harga normal.</p>
		<p dir="ltr"><strong>9. Dari mana sajakah titik keberangkatan perjalanan Ayo Liburan 2017?</strong></p>
		<p dir="ltr">Untuk saat ini titik keberangkatan Ayo Liburan 2017 hanya akan diadakan dari kota Jakarta saja.</p>
		<p dir="ltr"><strong>10. Apakah harga yang tertera pada website sudah termasuk tax dan biaya lain-lain?</strong></p>
		<p dir="ltr">Ya, harga yang tertera pada website ayoliburan.garuda-indonesia.com sudah termasuk harga tax, tiket pesawat, hotel, dan akomodasi selama perjalanan.</p>
		<p dir="ltr"><strong>11. Bagaimana saya dapat melihat status pemesanan dan pembelian saya?</strong></p>
		<p dir="ltr">Kamu dapat melihat status pemesanan atau pembelianmu dengan melihat pada halaman dashboardmu.</p>
		<p dir="ltr"><strong>12. Apakah saya dapat mencetak bukti pembayaran saya?</strong></p>
		<p dir="ltr">Ya, kamu dapat mencetak bukti pembelian dan pembayaran setelah seluruh proses transaksi berhasil dilakukan. Kamu hanya perlu menekan tombol cetak pada halaman dashboardmu.</p>
		<p dir="ltr"><strong>13. Bagaimana jika terjadi kesalahan sistem ketika melakukan pembelian atau pembayaran?</strong></p>
		<p dir="ltr">Kamu dapat menghubungi call center kami di 0822-1140-1146 atau mengirimkan email melalui ayoliburan@garuda-indonesia.com .</p>
		<p dir="ltr"><strong>14. Apa yang harus saya lakukan jika saya melakukan pemesanan namun tidak dapat melakukan pembayaran?</strong></p>
		<p dir="ltr">Jangan khawatir, hal ini biasa terjadi. Transaksi bisa saja tidak berhasil dikarenakan beberapa hal. Kegagalan transaksi dapat terjadi dikarenakan adanya masalah pada kartu kredit atau kesalahan teknis. Kamu dapat memeriksa kembali kelengkapan dokumen pembayaran serta detail kartu kredit dan mengulang proses pembayaranmu. Silahkan hubungi call center kami di 0822-1140-1146 atau mengirimkan email melalui ayoliburan@garuda-indonesia.com jika kamu membutuhkan bantuan lainnya.</p>
		<p dir="ltr"><strong>15. Bagaimana jika saya belum melakukan pembayaran akan tetapi time limit telah habis? (Bagaimana jika saya belum melakukan pembayaran namun batas waktu yang disediakan telah habis ?)</strong></p>
		<p dir="ltr">Kamu akan diberikan waktu session hold selama 20 (dua puluh) menit. Jika kamu telah melewati batas waktu pembayaran, maka pesananmu dianggap batal dan kamu dapat melakukan pemesanan ulang (kamu akan diberikan waktu yang terus berjalan selama 20 menit. Jika telah melewati batas waktu pembayaran, maka pesananmu dianggap batal. Kamu dapat kembali melakukan pemesanan ulang).</p>
		<p dir="ltr"><strong>16. Apakah terdapat minimum kuota keberangkatan pada setiap paket Ayo Liburan 2017?</strong></p>
		<p dir="ltr">Ya, minimum kuota keberangkatan untuk destinasi wisata Malang, Belitung, Labuan Bajo, dan Banyuwangi adalah 10 pax. Untuk destinasi Padang, minimum kuota keberangkatan adalah 20 pax</p>
		<p dir="ltr"><strong>17. Bagaimana jika saya ingin membatalkan pemesanan paket wisata Ayo Liburan 2017?</strong></p>
		<p dir="ltr">Kamu dapat melakukan proses pembatalan pesanan paket wisata Ayo Liburan 2017 dengan menghubungi call center kami di 0822-1140-1146 atau dengan mengajukan email ke ayoliburan@garuda-indonesia.com sesuai dengan syarat dan ketentuan yang berlaku.</p>
		<p dir="ltr"><strong>18. Jika saya membatalkan perjalanan saya, apakah paket liburan yang sudah saya beli dapat digunakan oleh teman saya atau orang lain</strong></p>
		<p dir="ltr">Maaf, akan tetapikamu tidak dapat mengubah nama pemesanan.</p>
		<p dir="ltr"><strong>19. Jika saya melakukan pembatalan pembelian, apakah saya dapat melakukan proses refund?</strong></p>
		<p dir="ltr">Ya, kamu dapat mengajukan proses refund jika kamu melakukan pembatalan pembelian paket wisata Ayo Liburan 2017 sesuai dengan syarat dan ketentuan yang berlaku.</p>
		<p dir="ltr"><strong>20. Jika terdapat kesalahan teknis yang menyebabkan pesanan saya tidak dapat di proses tetapi saya sudah melakukan pembayaran, apakah saya dapat melakukan refund?</strong></p>
		<p dir="ltr">Ya, kamu dapat melakukan proses refund.</p>
		<p dir="ltr"><strong>21. Berapa lama maksimal proses refund?</strong></p>
		<p dir="ltr">Proses refund yang kamu ajukan akan kami proses selama 14 (empat belas) hari kerja</p>
		<p dir="ltr"><strong>22. Berapa banyak jumlah refund yang akan saya terima?</strong></p>
		<p dir="ltr">Jika terdapat kesalahan teknis yang disebabkan dari pihak Ayo Liburan 2017, maka kamu akan mendapatkan refund sebesar 100% dari total pesananmu.</p>
		<p dir="ltr"><strong>23. Berapa banyak jumlah refund yang akan saya terima jika saya melakukan pembatalan?</strong></p>
		<p dir="ltr">Refund sebesar 50% jika melakukan pembatalan maksimum 7 hari atau 1 minggu sebelum tanggal keberangkatan.</p>
		<p dir="ltr"><strong>24. Bagaimana cara saya untuk menghubungi pihak Ayo Liburan-Garuda 2017? Apakah terdapat call center?</strong></p>
		<p dir="ltr">Jika kamu memiliki pertanyaan dan informasi lebih lanjut, kamu dapat menghubungi call center kami di 0822-1140-1146 atau email kami di ayoliburan@garuda-indonesia.com. Tim kami akan dengan senang hati untuk membantumu.</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
	</div>
	<div class="title_help2">Syarat & Ketentuan</div>
	<div class="text">
		<b>Selamat datang di Garuda Indonesia &ndash; Ayo Liburan 2017.</b>

		<p dir="ltr">Garuda Indonesia &ndash; Ayo Liburan 2017 merupakan paket wisata yang menawarkan perjalanan ke lima destinasi wisata di Indonesia. Perjalanan wisata ini akan diadakan pada periode tertentu. Untuk itu, kami telah menyiapkan beberapa syarat dan ketentuan serta kebijakan privasi guna memastikan bahwa website ini dapat memberikan pelayanan yang memuaskan dan bersifat aman bagi semua pihak. Garuda Indonesia &ndash; Ayo Liburan 2017 berhak untuk mengubah pedoman ini setiap saat.</p>
<p dir="ltr">Kamu disarankan untuk membaca serta memperhatikan syarat dan ketentuan berikut ini dengan seksama. Dengan membaca syarat dan ketentuan yang tertera, kamu dianggap telah menerima dan menyetujui segala hal yang tertera pada pedoman yang ada.</p>
<p dir="ltr">Semua konten yang ada pada website Garuda Indonesia &ndash; Ayo Liburan 2017 dimiliki dan dikendalikan oleh pihak Garuda Indonesia &ndash; Ayo Liburan 2017 dan dilindungi oleh undang-undang dan hak cipta. Isi dan materi pada halaman website termasuk dan tidak terbatas pada teks, gambar, dan ikon tertentu dilindungi oleh hak cipta, merek dagang, merek layanan, nama dagang, dan hak kekayaan intelektual dimilki dan dikendalikan oleh Garuda Indonesia.</p>
<p dir="ltr">Berikut merupakan syarat dan ketentuan yang perlu diperhatikan dalam pembelian paket liburan Gauda Indonesia &ndash; Ayo Liburan 2017 :</p>
<p>&nbsp;</p>
<ol>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Garuda Indonesia &ndash; Ayo Liburan 2017 menyediakan pilihan paket liburan untuk 5 (lima) destinasi wisata di Indonesia, yaitu Malang, Belitung, Labuan Bajo, Banyuwangi, dan Padang dengan total paket sebanyak 800 paket wisata dengan paket wisata ke Malang sebanyak 25 paket per periode untuk 4 kali periode perjalan, Belitung sebanyak 25 paket per periode untuk 4 kali periode perjalanan, Labuan Bajo sebanyak 25 paket per periode untuk 6 kali periode perjalanan, Banyuwangi sebanyak 25 paket per periode untuk 6 kali periode perjalanan, dan Padang sebanyak 75 paket periode untuk 4 kali periode perjalanan.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Pemesanan dan pembelian tiket Garuda Indonesia &ndash; Ayo Liburan 2017 hanya dapat dilakukan pada website resmi kami di ayoliburan.garuda-indonesia.com.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Pembayaran paket Ayo Liburan 2017 dapat dilakukan di Kantor Penjualan tiket kami di :</p>
</li>
</ol>
<ul>
<li dir="ltr" style="list-style-type: disc;">
<p dir="ltr">Mall Senayan City , Jalan Asia Afrika No. 19, Gelora, Tanah Abang, Kota Jakarta Pusat.</p>
</li>
<li dir="ltr" style="list-style-type: disc;">
<p dir="ltr">Mall Kota Kasablanka Lt. 1 Unit 159, Jalan Casablanca Raya Kav.88, Tebet, Kota Jakarta Selatan.</p>
</li>
<li dir="ltr" style="list-style-type: disc;">
<p dir="ltr">Living World Lantai F1 / 10, Jl. Alam Sutera Boulevard No. 21, Pakulonan, Serpong Utara, Kota Tangerang Selatan.</p>
<p dir="ltr">Pada jam operasional :</p>
</li>
</ul>
<ul>
<li dir="ltr" style="list-style-type: disc;">
<p dir="ltr">Weekdays (10:00 - 18:00)</p>
</li>
<li dir="ltr" style="list-style-type: disc;">
<p dir="ltr">Weekends (10:00 - 17:00)</p>
</li>
</ul>
<ol style="list-style-type: lower-alpha;" start="4">
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Pembayaran paket Ayo Liburan 2017 dapat dilakukan di Kantor Penjualan tiket kami maksimal hingga satu hari setelah pemesanan (atau s/d jam tutup di tiap Kantor Penjualan pada hari tersebut).</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Pembayaran untuk pembelian paket wisata hanya dapat kamu lakukan dengan menggunakan Kartu Kredit BNI Mastercard (BNI Mastercard Titanium, BNI Mastercard Silver, BNI Mastercard Gold, BNI Mastercard Platinum, BNI World Mastercard, termasuk Co-Branding).</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Setiap pembelian paket wisata Garuda Indonesia &ndash; Ayo Liburan 2017 menggunakan Kartu Kredit BNI Mastercard, akan mendapatkan potongan langsung dengan ketentuan sebagai berikut :</p>
<ol style="list-style-type: lower-alpha;">
<li dir="ltr">Promo potongan langsung Kartu Kredit BNI Mastercard berlaku untuk seluruh jenis Kartu Kredit BNI Mastercard kecuali IB Hasanah Card.</li>
<li dir="ltr">Potongan langsung sebesar Rp500.000,00 (lima ratus ribu rupiah) untuk pemegang Kartu Kredit BNI Mastercard Titanium dan Rp250.000,00 (dua ratus lima puluh ribu rupiah) untuk pemegang Kartu Kredit BNI Mastercard lainnya (World, Platinum, Gold, Silver, termasuk Co-Branding). Potongan harga akan dilakukan langsung di mesin EDC di Kantor Penjualan Garuda Indonesia yang kamu datangi untuk membayar.</li>
</ol>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Setiap pembelian paket wisata Garuda Indonesia &ndash; Ayo Liburan 2017 menggunakan Kartu Kredit BNI Mastercard, akan mendapatkan fasilitas cicilan BNI sebesar 0% (nol persen) dengan ketentuan sebagai berikut :</p>
<p dir="ltr">- Promo cicilan BNI 0% (nol persen) berlaku untuk seluruh jenis Kartu Kredit BNI Mastercard kecuali BNI Corporate Card dan IB Hasanah Card.</p>
<p>- BNI cicilan 0% (nol persen) dengan periode cicilan 6 (enam) bulan dan 12 (dua belas) bulan.</p>
<p>- Minimum transaksi untuk dapat menggunakan fasilitas cicilan 0% (nol persen) adalah Rp3.000.000,- ( tiga juta Rupiah). Setiap 15 (lima belas) orang pembeli pertama yang melakukan pembelian di website untuk masing-masing tujuan destinasi Malang, Banyuwangi, Labuan Bajo, Belitung, dan 50 (lima puluh) orang pembeli pertama untuk destinasi Padang akan mendapatkan harga spesial.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Pembelian maksimum paket Ayo Liburan 2017 adalah 8 (delapan) orang dalam satu reservasi menggunakan 1 (satu) Kartu Kredit BNI Mastercard.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Maksimum pembelian paket wisata Garuda Indonesia-Ayo Liburan 2017 dapat dilakukan 8 (delapan) hari sebelum periode perjalanan untuk destinasi Malang, Belitung, Labuan Bajo, dan Banyuwangi. Maksimum pembelian paket wisata Garuda Indonesia-Ayo Liburan 2017 dapat dilakukan 15 (lima belas) hari sebelum periode perjalanan untuk destinasi Padang.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Harga pembelian paket Garuda Indonesia - Ayo Liburan 2017 untuk bayi, anak-anak, dan lanjut usia akan dikenakan harga normal.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Seluruh harga yang tertera pada website sudah termasuk harga tax, tiket pesawat, hotel, dan akomodasi selama perjalanan.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Tanggal dan jadwal trip dapat berubah sewaktu-waktu sesuai ketersediaan komponen trip dan jika tersedia jadwal baru, peserta akan diinfo untuk kesediaannya diikutkan pada jadwal baru tersebut. Jika tidak bersedia, biaya yang telah dibayarkan peserta akan dikembalikan dan peserta tidak diikutkan ke trip Ayo Liburan 2017.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Status pemesanan paket wisata Garuda Indonesia &ndash; Ayo Liburan 2017 dapat kamu lihat pada dashboard akunmu.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Harap untuk menyimpan bukti transaksi pembelian paket wisata Garuda Indonesia &ndash; Ayo Liburan 2017 yang telah kamu cetak setelah menyelesaikan seluruh proses transaksi.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Bukti transaksi akan dikirimkan melalui email.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Paket wisata yang telah dibeli tidak dapat dipindahtangakan kepada pihak manapun.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Paket wisata Garuda Indonesia &ndash; Ayo Liburan 2017 dapat dibatalkan dengan maksimum waktu pembatalan adalah 7 (tujuh) hari sebelum tanggal keberangkatan.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Jika kamu melakukan proses pembatalan dengan maksimum waktu pembatalan adalah 7 (tujuh) hari, maka kamu dapat mengajukan proses refund (pengembalian). Permintaan pengembalian akan diproses dengan waktu maksimal 14 hari kerja dengan jumlah pengembalian sebesar 50% (lima puluh persen) dari total paket yang kamu batalkan. Jika kamu melakukan pembatalan kurang dari 7 (tujuh) hari sebelum tanggal keberangkatan, maka uang yang telah kamu bayarkan tidak dapat dikembalikan.</p>
</li>
</ol>
<p>&nbsp;</p>
<p dir="ltr"><strong>*Jadwal trip dapat berubah sewaktu-waktu.</strong></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
		<p>&nbsp;</p>
		<div class="title_help2">Kebijakan Privasi</div>
		<p dir="ltr">Kami sadar bahwa privasi merupakan hal yang sangat penting. Kebijakan privasi ini berlaku untuk seluruh aktivitas dan transaksi yang ada pada website Garuda Indonesia-Ayo Liburan 2017. Garuda Indonesia Ayo Liburan 2017 berkomitmen untuk melindungi dan menjaga kerahasiaan informasi pribadi yang Anda berikan dari tindakan pemalsuan, penyalahgunaan, atau akses lain yang tidak diizinkan untuk tujuan-tujuan yang tidak bertanggungjawab. Sesuai dengan kebijakan kami, kami tidak berhak untuk menyebarkan, menjual, atau menyewakan informasi apapun terkait dengan data pribadi Anda kepada pihak-pihak lain. Garuda Indonesia &ndash; Ayo Liburan 2017 memiliki hak untuk merubah kebijakan privasi ini tanpa pemberitahuan. Kami merekomendasikan Anda untuk secara berkala membaca ulang pernyataan privasi untuk mengetahui perubahan-perubahan ini.</p>
		<p></p>
	</div>
	<br><br><br><br>
	<div class="link_">
		<a href="#bahasa">Bahasa Indonesia</a>
		<a href="#english" class="selected">English</a>
	</div>
	<div id="english"></div>
	<div class="title_help2">Contact Us</div>
	<div class="text">
		Have any question or need some assistance?  Please contact us through our call center 0822-1140-1146 from 09:00 - 17:00 or send your email to ayoliburan@garuda-indonesia.com. We are ready to help you! 
	</div>
	<div class="title_help2">FAQ</div>
	<div class="text faq">
		<p dir="ltr"><strong>1. How can I book Ayo Liburan 2017 holiday package?</strong></p>
		<p dir="ltr">You can only book and purchase Ayo Liburan 2017 holiday packages through the Garuda Indonesia – Ayo Liburan 2017 official website at ayoliburan.garuda-indonesia.com & using the BNI Mastercard, and do the payment in the selected Garuda Indonesia Ticketing Office (Locations and operational hours stated below in this page).</p>
		<p dir="ltr"><strong>2. How long before the trip period can I make the process of booking or purchasing Ayo Liburan 2017 Holiday Package?</strong></p>
		<p dir="ltr">You can place your order 8 (eight) days before the travel period for destination Malang, Belitung, Labuan Bajo, and Banyuwangi. The deadline for purchasing vacation package for destination to Padang is 15 (fifteen) days before the travel period.</p>
		<p dir="ltr"><strong>3.How do I make a payment for my order?</strong></p>
		<p dir="ltr">We aim to offer you a convenient payment process for your order. Your payment process can be done through the official website ayoliburan.garuda-indonesia.com only using BNI MasterCard.</p>
		<p dir="ltr"><strong>4. What payment methods are accepted in purchasing Ayo Liburan 2017 Holiday Package?</strong></p>
		<p dir="ltr">Ayo Liburan 2017 Holiday Package only receives payment method using BNI MasterCard.</p>
		<p dir="ltr"><strong>5. Why did my transaction fail?</strong></p>
		<p dir="ltr">Don&rsquo;t worry! It happens sometimes. Transactions can be declined for several reasons. It might be caused by credit card issues or technical errors. Please re-check your document, personal informations, and your credit card detail before re-doing your payment process. You can contact us through our call center at 0822-1140-1146 or send us an email at ayoliburan@garuda-indonesia.com for further assistance.</p>
		<p dir="ltr"><strong>6. Is there any minimum or maximum purchase for Ayo Liburan 2017 Holiday Package in one reservation using 1 (one) credit card?</strong></p>
		<p dir="ltr">You can purchase Ayo Liburan 2017 Package with maximum purchasing 8 (eight) persons in one reservation using 1 (one) BNI Mastercard</p>
		<p dir="ltr"><strong>7. Is there any special price for children or infant?</strong></p>
		<p dir="ltr">We are sorry, but children or infant will be charged the normal price</p>
		<p dir="ltr"><strong>8. Is there any special price for elderly (60 years and above)?</strong></p>
		<p dir="ltr">We are sorry, but elderly will be charged the normal price.</p>
		<p dir="ltr"><strong>9. From which point of departure of Ayo Liburan 2017?</strong></p>
		<p dir="ltr">For now, the departure point for Ayo Liburan 2017 will only be held from Jakarta.</p>
		<p dir="ltr"><strong>10. Does the price listed on the website includes taxes and other fees?</strong></p>
		<p dir="ltr">Yes, the price listed on the website includes taxes, flight ticket, hotel, and accommodations during the trip.</p>
		<p dir="ltr"><strong>11. How can I check my booking status?</strong></p>
		<p dir="ltr">You can check your booking status on your dashboard page. But don&rsquo;t worry, we will inform your purchasing status to your email as well.</p>
		<p dir="ltr"><strong>12.Can I print out my invoice after I pay?</strong></p>
		<p dir="ltr">Yes, you can print out your invoice after the whole transaction process is succeed by clicking the Print button on your dashboard page.</p>
		<p dir="ltr"><strong>13. What if there is any system error occurs during the purchasing or payment process?</strong></p>
		<p dir="ltr">You can contact us through our call center 0822-1140-1146or send us email to ayoliburan@garuda-indonesia.com</p>
		<p dir="ltr"><strong>14. What should I do if I already placed an order but can not make a payment?</strong></p>
		<p dir="ltr">Don&rsquo;t worry! It happens sometimes. Transactions can be declined for several reasons. It might be caused by some credit card issues or technical errors. Please re-check your document, personal informations , and your credit card detail before re-doing your payment process. You contact us through our call center at 0822-1140-1146 or send us email at ayoliburan@garuda-indonesia.com for further assistance.</p>
		<p dir="ltr"><strong>15. If I have not made the payment process, but the time limit has expired, what would happen to my order?</strong></p>
		<p dir="ltr">You will be given session hold for 20 (twenty) minutes. If the time limit is out, you order is considered cancelled and you can make new reservation.</p>
		<p dir="ltr"><strong>16. Is there any minimum quota for the trip to begin?</strong></p>
		<p dir="ltr">Yes, there will be a minimum quota for each destination. 10 (ten) packages for trip to Malang, Belitung, Labuan Bajo, Banyuwangi and minimum 20 (twenty) packages for trip to Padang.</p>
		<p dir="ltr"><strong>17. Is it possible to cancel my order?</strong></p>
		<p dir="ltr">You will be possible to cancel your order by contacting us through our call center 0822-1140-1146 or send us your email to ayoliburan@garuda-indonesia.com</p>
		<p dir="ltr"><strong>18. I will not be able to go, can my friend or my relatives use the Holiday Package I already purchased?</strong></p>
		<p dir="ltr">Sorry, but you cannot change the name of the reservation.</p>
		<p dir="ltr"><strong>19. If I cancel my order, will I be able to make a refund?</strong></p>
		<p dir="ltr">Yes, you will be able to make a refund process according to our Terms and Conditions.</p>
		<p dir="ltr"><strong>20. If there is any technical error which caused my order cannot be processed, whereas I already did the payment, will I be able to make a refund?</strong></p>
		<p dir="ltr">Yes, you will be able to make a refund process according to our Terms and Conditions</p>
		<p dir="ltr"><strong>21. How long will my refund be processed?</strong></p>
		<p dir="ltr">Your refund will be processed in 14 (fourteen) work days.</p>
		<p dir="ltr"><strong>22. How much refund will I receive?</strong></p>
		<p dir="ltr">If there is any technical error caused by our party, you will get 100% refund of your total order.</p>
		<p dir="ltr"><strong>23. How much refund will I receive if I cancel my booking?</strong></p>
		<p dir="ltr">If you cancel your booking one week prior the departure day, you will receive 50% refund.</p>
		<p dir="ltr"><strong>24. How can I contact Ayo Liburan 2017 customer service if I need further assistance?</strong></p>
		<p dir="ltr">If you have any further questions, you can contact us through our call center 0822-1140-1146 or if you prefer to send us an email, please kindly send your email to ayoliburan@garuda-indonesia.com. Our team will be glad to assist you.</p>
		<p>&nbsp;</p>
		<p dir="ltr">&nbsp;</p>

	</div>
	<div class="title_help2">Terms & Conditions</div>
	<div class="text">
		<b>Welcome to Garuda Indonesia - Ayo Liburan 2017</b>
		<p dir="ltr">Garuda Indonesia &ndash; Ayo Liburan 2017 program is a vacation package that offers journeys to five tourism destinations in Indonesia. These travel packages will be available on the dates determined by the program. Therefore, we have prepared some terms and conditions and a privacy policy to ensure that this website can provide all parties with the best and secure service. Garuda Indonesia &ndash; Ayo Liburan 2017 has the right to change these terms and conditions at any time without prior notice.</p>
<p dir="ltr">You are advised to read and observe these terms and conditions. By reading our terms and conditions, you will be considered to have accepted and approved of all items included in our terms and conditions.</p>
<p dir="ltr">All content published on the Garuda Indonesia &ndash; Ayo Liburan 2017 website is owned and controlled by Garuda Indonesia &ndash; Ayo Liburan 2017 and is protected by related laws and the copyright laws. The contents and materials of the website, including and not limited to the text, pictures and icons, are protected by copyright laws, trademark, service brand, name brand and intellectual rights owned and controlled by Garuda Indonesia.</p>
<p dir="ltr">The following are the terms and conditions that must be observed before purchasing a Garuda Indonesia &ndash; Ayo Liburan 2017 vacation package:</p>
<ol>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Garuda Indonesia &ndash; Ayo Liburan 2017 offers vacation packages for 5 (five) tourism destinations in Indonesia, which include Malang, Belitung, Labuan Bajo, Banyuwangi, and Padang with a total number of 800 tourism packages divided into 25 packages to Malang per travel period for 4 (four) travel periods, 25 packages to Belitung per travel period for 4 (four) travel periods, 25 packages to Labuan Bajo per travel period for 6 (six) travel periods, 25 packages to Banyuwangi per travel period for 6 (six) travel periods, and 75 packages to Padang per travel period for 4 (four) travel periods.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Reservations and purchase of tickets for Garuda Indonesia &ndash; Ayo Liburan 2017 can only be done through our official website at ayoliburan.garuda-indonesia.com.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Purchasement can be done in our Ticketing Office :</p>
</li>
</ol>
<ul>
<li dir="ltr" style="list-style-type: disc;">
<p dir="ltr">Mall Senayan City, Jalan Asia Afrika No. 19, Gelora, Tanah Abang, Kota Jakarta Pusat.</p>
</li>
<li dir="ltr" style="list-style-type: disc;">
<p dir="ltr">Mall Kota Kasablanka Lt. 1 Unit 159, Jalan Casablanca Raya Kav. 88, Tebet, Kota Jakarta Selatan.</p>
</li>
<li dir="ltr" style="list-style-type: disc;">
<p dir="ltr">Living World Lantai F1 / 10, Jalan Alam Sutera Boulevard No. 21, Pakulonan, Serpong Utara, Kota Tangerang Selatan.</p>
<p dir="ltr">Operational hour :</p>
</li>
</ul>
<ul>
<li dir="ltr" style="list-style-type: disc;">
<p dir="ltr">Weekdays (10:00 - 18:00)</p>
</li>
<li dir="ltr" style="list-style-type: disc;">
<p dir="ltr">Weekends (10:00 - 17:00)</p>
</li>
</ul>
<ol start="4">
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Payment can be done at our Ticketing Office's maximum one day after the booking (within the office operational hours).</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Payment for these travel packages can only be done using BNI Mastercard (BNI Mastercard Titanium, BNI Mastercard Silver, BNI Mastercard Gold, BNI Mastercard Platinum, BNI World Mastercard, including Co-Branding).</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">All purchases of Garuda Indonesia &ndash; Ayo Liburan 2017 travel packages using the BNI MasterCard will receive a cash back according to the following provisions :</p>
<ol>
<li dir="ltr" style="list-style-type: lower-alpha;">
<p dir="ltr">Direct discount BNI Mastercard is valid for all types of BNI Mastercard except for the IB Hasanah Card &amp; Corporate Card.</p>
</li>
<li dir="ltr" style="list-style-type: lower-alpha;">
<p dir="ltr">Direct discount Rp500,000 (five hundred thousand rupiahs) for BNI Mastercard Titanium holders and Rp250,000 (two hundred and fifty thousand rupiahs) for other BNI Mastercard holders (World, Platinum, Gold, Silver, including Co-Branding). Discount will be made directly on EDC machine at Garuda Indonesia&rsquo;s Ticketing Office.</p>
</li>
</ol>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">All purchases of Garuda Indonesia &ndash; Ayo Liburan 2017 travel packages using a BNI Mastercard will receive a facility in the form of 0% (zero percent) Installments according to the following provisions:</p>
<ol>
<li dir="ltr" style="list-style-type: lower-alpha;">
<p dir="ltr">BNI 0% (zero percent) Installment promo is valid for all types BNI Mastercard except for the IB Hasanah Card &amp; Corporate Card.</p>
</li>
<li dir="ltr" style="list-style-type: lower-alpha;">
<p dir="ltr">BNI 0% (zero percent) Installment will be available for a 6 (six) month or 12 (twelve) month installment payment program.</p>
</li>
<li dir="ltr" style="list-style-type: lower-alpha;">
<p dir="ltr">Minimum transaction for using this 0% (zero percent) Installment promo is Rp1,000,000 ( one million Rupiah), including if the card holder uses redemption points.</p>
</li>
</ol>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">The first 15 (fifteen) buyers that purchase vacation packages for each of the following destinations, including Malang, Banyuwangi, Labuan Bajo, Belitung, and the first 50 (fifty) buyers that purchase vacation packages for Padang will get a special price.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Maximum purchasement for Ayo Liburan Holiday Package is 8 (eight persons) in one reservation using 1 (one) BNI Mastercard.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">The deadline for purchasing vacation packages under the Garuda Indonesia &ndash; Ayo Liburan 2017 program is 8 (eight) days before the travel period for destination Malang, Belitung, Labuan Bajo, and Banyuwangi. The deadline for purchasing vacation package for destination to Padang is 15 (fifteen) days before the travel period.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Purcahsement for Ayo Liburan Holiday Package for infant, children, and eldery will be charged the normal price.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">All prices published on the website include taxes, airfare, hotels and accommodation during the trip.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">The status of your purchase of the tickets for the Garuda Indonesia &ndash; Ayo Liburan 2017 vacation package can be viewed on the dashboard of your Account.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">You must keep your proof of transaction when purchasing a vacation package under the Garuda Indonesia &ndash; Ayo Liburan 2017 program by printing it after your transaction is completed.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">Your proof of transaction will be sent to your e-mail.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">The vacation package purchased is non-transferrable.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">The Garuda Indonesia &ndash; Ayo Liburan 2017 vacation package can be cancelled maximum 7 (seven) days before the date of departure.</p>
</li>
<li dir="ltr" style="list-style-type: decimal;">
<p dir="ltr">If you cancel your trip maximum 7 (seven) days before the date of departure, then you can submit a proposal to refund your purchase. This refund will be processed maximum 14 (fourteen) working days with a total refund amounting to 50% (fifty percent) out of the total cost of the package you cancelled. If you cancel your package less than 7 (seven) days before the date of departure, then you will not be liable to receive a refund for the cost of the trip.</p>
</li>
</ol>
<p dir="ltr"><strong>*The trip schedule may be changed at any time.</strong></p>
<p>&nbsp;</p>
<p dir="ltr">&nbsp;</p>
		<p><br /><br /></p>
		<div class="title_help2">Privacy Policy</div>
		<p dir="ltr">We realize that privacy is a important matter. This privacy policy applies for all activities and transactions performed on the Garuda Indonesia-Ayo Liburan 2017 website. Garuda Indonesia-Ayo Liburan 2017 is committed to protecting the privacy of your personal information from identity theft, misuse of information or other prohibited and irresponsible actions. In accordance with our policy, we do not have the right to spread, sell or rent out your private information to other parties. Garuda Indonesia-Ayo Liburan 2017 has the right to change this privacy policy without prior notice. We recommend you periodically read our privacy policy to see if any changes are made.</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
	</div>
	
	
</div>