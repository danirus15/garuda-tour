<?php
class Modglobal extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    public function getTotalSeatOrdered($schedule_id, $city_id)
    {
        $remind = 0;
        $query = $this->db->query('
            SELECT SUM(qty) total_normal, SUM(qty_promo) total_promo  FROM `order` WHERE schedule_id = ? AND city_id = ? AND status != \'2\'
        ', array($schedule_id, $city_id));
        if ($query->num_rows()) {
            $result = $query->row();
            $remind = $result->total_normal + $result->total_promo;
        }

        return $remind;
    }

    public function setExpireOrder()
    {
        return $this->db->query('
            UPDATE `order` SET status=\'2\'
            WHERE TIMESTAMPDIFF(MINUTE, created_at, NOW()) >= ? AND status = \'0\'
        ', array(getenv('MAX_ORDER_EXPIRE')));
    }

    public function getScheduleCity($destination_id, $city_id) {
        return $this->db->query('
            SELECT a.*, b.price, b.price_normal, b.promo_seat_max, b.promo_seat_discount FROM schedule a
            LEFT JOIN schedule_price b on a.id = b.schedule_id 
            WHERE 
            a.status = "1"
            AND a.destination_id = ?
            AND b.city_id = ?
        ', array($destination_id, $city_id));
    }


    public function getLowPrice($destination_id) {
        return $this->db->query('
            SELECT price, price_normal, promo_seat_max, promo_seat_discount FROM schedule_price WHERE schedule_id in (
                SELECT id FROM schedule WHERE destination_id = ?
            ) ORDER BY price ASC LIMIT 1
        ', array($destination_id));
    }


    public function find($table, $where=array(), $order='', $like=array(), $limit=null, $page=null)
    {
        
        if ($order) {
            $this->db->order_by($order);
        }
        if ($like) {
            $this->db->like($like);
        }
        if ($where) {
            $this->db->where($where);
        }
        if (!is_null($limit) && !is_null($page)) {
            $this->db->limit($limit, $page);
        }
        return $this->db->select()
            ->from($table)
            ->get();
    }

    //CRUD
    public function insert($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    public function insert_batch($table, $data)
    {
        return $this->db->insert_batch($table, $data);
    }

    public function insert_id()
    {
        return $this->db->insert_id();
    }

    public function update($table, $data, $where)
    {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }

    public function delete($table, $where)
    {
        $this->db->where($where);
        return $this->db->delete($table);
    }
}
?>