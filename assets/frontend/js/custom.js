var schedules = [];
var selected_schedule = {};
function reset_form() {
	$('#schedule option').each(function() {
		if ($(this).val() !== "") {
			$(this).remove();
		}
	});

	$('#quantity').val(0);
	$('#total_cost').text('Rp.-');
	$('#summary').text('(- Wisatawan x Rp.-)');
	$('#avaliable_seat').text('-');

	schedules = [];
	selected_schedule = {};
}

function populate_schedule() {
	$.each(schedules, function(index, value) {
		$('#schedule').append('<option value="' + value.id + '">' + value.depart_date + ' - ' + value.return_date + '</option>');
	});
}

function populate_price(schedule_id) {
	$.each(schedules, function(index, value) {
		if (value.id === schedule_id) {
			selected_schedule = value;
			var quantity = $('#quantity').val();
			var total = $('#quantity').val() * selected_schedule.price;
			$('#total_cost').text('Rp.' + total);
			$('#summary').text('(' + quantity + ' Wisatawan x Rp.' + selected_schedule.price + ')');
			$('#avaliable_seat').text(selected_schedule.total_seat - quantity);
			return false;
		}
	});
}

function format_number (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
}

$('#suplement').change(function() {
    var total = $('#total').val();
    var suplement = $(this).val();
    if (this.checked) {
        var total_with_suplement = parseInt(total) + parseInt(suplement);
        $('#total_cost').text('Rp.'+format_number(total_with_suplement)+',-');
        $('#include_suplement').show();
    } else {
        $('#total_cost').text('Rp.'+format_number(total)+',-');
        $('#include_suplement').hide();
    }
})

function update_total() {
	if (!$.isEmptyObject(selected_schedule)) {
		var quantity = $('#quantity').val();
        
        var sisa_promo = (selected_schedule.promo_seat_avaliable > 0 && (selected_schedule.promo_seat_avaliable - quantity) > 0) ? (selected_schedule.promo_seat_avaliable - quantity) : 0;
        var sisa_normal = (sisa_promo > 0) ? selected_schedule.total_seat_normal_avaliable : selected_schedule.total_seat_normal_avaliable + (selected_schedule.promo_seat_avaliable - quantity);

        var price = selected_schedule.price;
        var total = (quantity * price);
        if (selected_schedule.promo_seat_avaliable > 0) {
            if (quantity > selected_schedule.promo_seat_avaliable) {
                total = (selected_schedule.promo_seat_avaliable * selected_schedule.promo_seat_discount) + ((quantity - selected_schedule.promo_seat_avaliable) * selected_schedule.price);
            } else {
                total = quantity * selected_schedule.promo_seat_discount;
            }
        }
        
		// var price_formating = format_number(price);
		
		var avaliable_seat = selected_schedule.total_seat - quantity;

		$('#total_cost').text('Rp.' + format_number(total));
        $('#total').val(total);    
		// $('#summary').text('(' + quantity + ' Wisatawan x Rp.' + price_formating + ')');
		// $('#avaliable_seat').text(avaliable_seat);
     
        if (sisa_promo > 0 ) {
            $('#sisa_promo').show();
            $('#soldout_promo').hide();
            $('#sisa_promo_number').html(sisa_promo);
        } else {
            $('#sisa_promo').hide();
            $('#soldout_promo').show();
        }

        if (sisa_normal > 0) {
            $('#sisa_normal').show();
            $('#soldout_normal').hide();
            $('#sisa_normal_number').html(sisa_normal);
        } else {
            $('#sisa_normal').hide();
            $('#soldout_normal').show();
        }
	}
}

// This button will increment the value
$('#btn_plus').click(function(e){
    // Stop acting like a button
    e.preventDefault();
    // Get the field name
    fieldName = $(this).attr('field');
    // Get its current value
    var currentVal = parseInt($('input[name='+fieldName+']').val());
    // If is not undefined
    if (!isNaN(currentVal)) {
        // Increment
        if (currentVal < selected_schedule.total_seat) {
            if (currentVal < max_tourist) {
                if (selected_schedule.promo_seat_avaliable > 0 || selected_schedule.total_seat_normal_avaliable > 0) {
                    $('input[name='+fieldName+']').val(currentVal + 1);
                }
            } else {
                alert('Maksimal wisatawan adalah ' + max_tourist + ' orang');
            }
        }
    } else {
        // Otherwise put a 0 there
        $('input[name='+fieldName+']').val(0);
    }

    update_total();
});
// This button will decrement the value till 0
$("#btn_min").click(function(e) {
    // Stop acting like a button
    e.preventDefault();
    // Get the field name
    fieldName = $(this).attr('field');
    // Get its current value
    var currentVal = parseInt($('input[name='+fieldName+']').val());
    // If it isn't undefined or its greater than 0
    if (!isNaN(currentVal) && currentVal > 0) {
        // Decrement one
        $('input[name='+fieldName+']').val(currentVal - 1);
    } else {
        // Otherwise put a 0 there
        $('input[name='+fieldName+']').val(0);
    }

    update_total();
});

function get_schedule($city_id) {
    data = {
        'destination_id': $('#destination_id').val(),
        'city_id': $city_id,
        [csrfName]: csrfHash
    }

	$.post(url_schedules, data, function(data) {
        csrfName = data.csrfName;
        csrfHash = data.csrfHash;
		schedules = data.data;
		populate_schedule();
	});
} 

$(document).ready(function() {
	if ($('#city').val()) {
		get_schedule($('#city').val());
	}
})

$(document).on('change', '#city', function() {
	reset_form();
	get_schedule($(this).val());
});

$(document).on('change', '#schedule', function() {
	if ($(this).val() !== "") {
		populate_price($(this).val());
	}
});

//selecte from date
if ($('#from_date').length) {
    $('.qtyminus').hide();
    $('.qtyplus').hide();
	$( "#from_date, #to_date" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        beforeShowDay: function(date){
	        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
	        return [ array.indexOf(string) >= 0 ]
	    },
        onSelect: function( selectedDate ) {
        	
            reset_order();

        	var string = selectedDate.split('/');
        	sch_id = sch[array.indexOf(string[2]+'-'+string[0]+'-'+string[1])];

        	$.each(schedules, function(index, value) {
        		if (sch_id == value.id) {
        			selected_schedule = value;
        			
        			$('#total_promo_seat').html(value.promo_seat_max);
        			$('#promo_seat_discount').html('Rp.' + format_number(value.promo_seat_discount) + ',-');
        			$('#normal_price').html('Rp.' + format_number(value.price) + ',-');
                    $('#schedule_id').val(value.id);

                    update_total();

        			return false;
        		}
        	});

            if(this.id == 'from_date'){
              	var dateMin = $('#from_date').datepicker("getDate");
              	var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); 
              	//set jumlah hari
              	var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + total_days); 
				$('#to_date').val($.datepicker.formatDate('mm-dd-yy', new Date(rMax)));                    
            }
            $('.qtyminus').show();
            $('.qtyplus').show();
        }
    });
}

function reset_order()
{
    $('#quantity').val(0);
    $('#total_cost').val(0);
    $('#total').val(0);
}

//functio submit order form
$('#form_order').submit(function(event) {
	if ($('#quantity').val() <= 0) {
		alert('Silahkan isi jumlah penumpang');
	} else {
		return;
	}
  	event.preventDefault();
});