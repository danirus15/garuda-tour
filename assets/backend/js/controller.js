$(function() {
    $("#uploadFile").on("change", function()
    {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#imagePreview").css("background-image", "url("+this.result+")");
            }
        }
    });

});

// Preview Image
function PreviewImage(no) {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("file" + no).files[0]);

    oFReader.onload = function(oFREvent) {
        var imgPath=oFREvent.target.result;
        if( $("#appendImg" + no).html().length > 1){
            $('.img-wrap').addClass('sel');
            document.getElementById("uploadPreview" + no).src = imgPath;

        }
        else{
            $("#appendImg" + no).append('<div class="ratiobox imgcount'+ no +' ratio_1_1"><div class="ratiobox_content lqd"><img id="uploadPreview'+ no +'" src="'+ imgPath +'" /></div></div>');
            $('.img-wrap').addClass('sel');
        }
        
    };
}
// var count = 0;

$("#addImg").click(function(){
    // count++;
    var upload = $('.group-img > div').children().length + 1;
    $(".img-area").append('<div class="img-wrap"><div class="img-loc"><div class="style-input"><input type="file" name="files[]" id="file'+ upload +'" class="inputfile" onchange="PreviewImage('+ upload +');" /><label class="file" for="file'+ upload +'"><span>Upload Foto </span></label></div><div class="clearfix"></div><div id="appendImg'+ upload +'" class="appImg"></div></div><div class="img-cap"><h5>Caption</h5><input type="text" name="captions[]"></div><span class="del"><img src="' + baseUrl + 'assets/backend/images/ico_del.png"></span><div class="clearfix"></div></div>');

    //delete image
    
     
});

$('body').on("click", ".img-wrap .del",function(){
    var url = $(this).attr('ref');
    var upload = $('.group-img > div').children().length;
    $(this).parents('.img-wrap').remove();
    if (upload == 1) {
        $(".img-area").append('<div class="img-wrap"><div class="img-loc"><div class="style-input"><input type="file" name="files[]" id="file'+ upload +'" class="inputfile" onchange="PreviewImage('+ upload +');" /><label class="file" for="file'+ upload +'"><span>Upload Foto </span></label></div><div class="clearfix"></div><div id="appendImg'+ upload +'" class="appImg"></div></div><div class="img-cap"><h5>Caption</h5><input type="text" name="captions[]"></div><span class="del"><img src="' + baseUrl + 'assets/backend/images/ico_del.png"></span><div class="clearfix"></div></div>');
    }

    if (url != "") {

        $.post( url, {
            [csrfName]: csrfHash
        }).done(function (data){
            csrfName = data.csrfName;
            csrfHash = data.csrfHash;
            $('input[name='+csrfName+']').val(csrfHash);
        });
    }
});

function reset () {
    alertify.set({
        labels : {
            ok     : "OK",
            cancel : "Cancel"
        },
        delay : 5000,
        buttonReverse : false,
        buttonFocus   : "ok"
    });
}
function deletes () {
    alertify.set({
        labels : {
            ok     : "Yes",
            cancel : "No"
        },
        delay : 5000,
        buttonReverse : false,
        buttonFocus   : "ok"
    });
}

$(".alert").on( 'click', function () {
    reset();
    alertify.alert("This is an alert dialog");
    return false;
});
$(".delete").on( 'click', function () {
    var target  = $(this).attr("href");
    deletes();
    alertify.confirm("Are you sure to delete this", function (e) {
        if (e) {
            window.location = target;
            //alert("ok");
        } else {
            // user clicked "cancel"
            //alert("cancel");
        }
    });
    //alertify.confirm("Are you sure to delete this", function (e) {});
    return false;
    
});


function popup(pageURL, title) {
  var w = 1000;
  var h = 600;
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2);
  var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 

$( document ).ready(function() {
    $(".add-report-kar").click(function() {
        $("#kar-2").removeClass();
        $(".add-report-kar").hide();
    });
    $(".add-report-per").click(function() {
        $("#per-2").removeClass();
        $(".add-report-per").hide();
    });
});

tinymce.init({
  selector: "textarea",
  height: 300,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools'
  ],
  toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  toolbar2: 'print preview media | forecolor backcolor emoticons',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });


