confirm_delete = function(url){
    bootbox.confirm('Are you sure want to delete this data?', function(result) {
        if (result) {
            console.log(url);
            window.location = url;
        }
    });
}