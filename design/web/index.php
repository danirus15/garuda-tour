<!doctype html>	
<html>
<?php include "includes/head.php";?>
<body class="home">
<?php 
	include "includes/header.php";
?>
<!-- s:cover -->
<section id="cover">
	<div id="slide_cover">
		<div class="ratio16_9 box_img">
			<a href="#">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="assets/images/logo_ayo2.png" alt="">
				</div>
				<div class="img_con lqd">
					<img src="assets/images/c2.jpg" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="#">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="assets/images/logo_ayo2.png" alt="">
				</div>
				<div class="img_con lqd">
					<img src="assets/images/c1.jpg" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="#">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="assets/images/logo_ayo2.png" alt="">
				</div>
				<div class="img_con lqd">
					<img src="assets/images/c3.jpg" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="#">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="assets/images/logo_ayo2.png" alt="">
				</div>
				<div class="img_con lqd">
					<img src="assets/images/c4.jpg" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="#">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="assets/images/logo_ayo2.png" alt="">
				</div>
				<div class="img_con lqd">
					<img src="assets/images/c5.jpg" alt="" class="bg">
				</div>
			</a>
		</div>
	</div>
	<img src="assets/images/arrow_left.png" alt="" class="nav_cover nav_left">
	<img src="assets/images/arrow_right.png" alt="" class="nav_cover nav_right">
	<img src="assets/images/logo_5star_color.png" alt="" class="bni_logo">
	<img src="assets/images/bni_pesona.png" class="bni_card box_modal_full" alt="bni_card.php">
	
	<a href="#intro" class="more">
		<!-- EXPLORE MORE<br> -->
		<img src="assets/images/expand more.png" alt="">
	</a>
	
</section>
<!-- e:cover -->
<div class="container_full">
<!-- s:intro -->
<section id="intro" class="section">
	<div class="container">
		<h2 class="title"><span style="color: #FCB03B !important;">#Ayo</span><span style="color: #008C9A !important;">Liburan</span> bersama Garuda Indonesia</h2>
		<!-- #AyoLiburan bersama Garuda Indonesia dan alami petualangan seru!
<br><br>
Saksikan pemandangan alam Indonesia sambil mencicipi sajian kuliner khas Nusantara ditemani para Travel Influencers yang akan membuat liburanmu semakin seru.
<br><br>
Menikmati cantiknya pantai-pantai di Pulau Belitung sambil mencicipi cita rasa Mie Belitung Atep yang menggugah selera.
Mengunjungi museum Angkut yang instagramble di Malang.
Berburu sunrise sambil menikmati keindahan alam Kawah Ijen di Banyuwangi.
Melihat langsung Komodo dan trekking ke Pulau Padar (Labuan Bajo) dengan spot foto keren.
<br><br>
Pilih kotanya dan siapkan dirimu untuk menikmati pengalaman tak terlupakan bersama #AyoLiburan -->
Mau ngerasain asyiknya picnic sambil barbekyu-an di Pantai Pink (Komodo) atau di salah satu pulau cantik di Belitung? #AyoLiburan bareng Garuda Indonesia!
Selain bisa menikmati alam Indonesia yang nggak kalah keren sama  destinasi luar negeri, kamu bisa mencicipi sajian kuliner khas Nusantara dengan ditemani para Travel Influencers yang akan bikin liburan kamu tambah seru.
 <br><br>
Selain itu, kita juga punya aktivitas lain yang nggak kalah seru: Nyobain Mi Atep di Belitung, Minum Teh Talua Khas Minang, Berburu Blue Fire di  Kawah Ijen, Atau ke Museum Satwa di Batu Secret Zoo yang instagrammable banget buat kalian pecinta otomotif.

Tunggu apa lagi, yuk pesan paket #AyoLiburan pilihanmu sekarang!
</div>
</section>
<!-- e:intro -->
<!-- s:list -->

<div class="container">
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="assets/images/rajaampat1.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Raja Ampat in Papua</h2>
			<div class="place">Raja Ampat 3H2M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp. 12.970.000</strong>
				<strong>Rp. 9.970.000,- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a href="detail_rajaampat.php" class="more">#AyoLiburan</a>
			
		</div>
	</article>
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/e895301bcfa4b5b778f22f0077f1469e.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Chasing Sunrise in Bromo</h2>
			<div class="place">Malang & Bromo 3H2M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp.9.775.600,-</strong>
				<strong>Rp.4.365.000,- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a href="detail.php" class="more">#AyoLiburan</a>
		</div>
		<div class="soldout_now">SOLD OUT</div>
	</article>
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/f6d6759821647a8f91d9e60bbf5e264d.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Starfish & The Powdery Sand</h2>
			<div class="place">Belitung 3H2M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp.8.525.000,-</strong>
				<strong>Rp.3.070.000,- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a href="detail.php" class="more">#AyoLiburan</a>
			
		</div>
		<div class="soldout_now">SOLD OUT</div>
	</article>
	
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/a18e6b63be6bc5af9da56f11fa6d3ab5.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Sailing the Magnificent Komodo Islands</h2>
			<div class="place">Labuan Bajo & Kep. Komodo 4H3M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp.12.747.000,-</strong>
				<strong>Rp.5.625.000- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a href="detail.php" class="more">#AyoLiburan</a>
			
		</div>
		<div class="soldout_now">SOLD OUT</div>
	</article>
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="assets/images/img_padang.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Picnic Lunch in Pasumpahan Island</h2>
			<div class="place">Padang 3H2M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp.12.460.000,-</strong>
				<strong>Rp.3.880.000,- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a href="detail.php" class="more">#AyoLiburan</a>
			
		</div>
		<div class="soldout_now">SOLD OUT</div>
	</article>
	
	<article class="list1">
		<div class="box_img ratio_box ">
		
			<div class="img_con lqd">
				<img src="assets/images/img_banyuwangi.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Escape to The Blue Flames</h2>
			<div class="place">Banyuwangi 3H2M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp.11.901.600,-</strong>
				<strong>Rp.5.805.000,- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a href="detail.php" class="more">#AyoLiburan</a>
			
		</div>
		<div class="soldout_now">SOLD OUT</div>
	</article>
	
</div>
<div class="clearfix"></div>
<div class="container banner_bottom">
	<a href="https://www.garuda-indonesia.com/id/id/special-offers/sales-promotion/ayo-liburan.page" target="_blank"><img src="assets/images/Banner-FIX-fixed.jpg" alt=""></a>
</div>
</div>
<div class="banner_float">
	<a class="banner_close"><img src="assets/images/btn_close.png" alt=""></a>
	<a href="https://eform.bni.co.id/BNI_eForm/informasiPersyaratan?option_cc=umum" target="_blank"><img src="assets/images/bni_promo.jpg" alt=""></a>
</div>
<!-- e:list -->
<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
</body>
</html>