<!doctype html>	
<html>
<div style="width: 900px; margin: 0 auto; border: 1px solid #eee; padding: 20px; font-family: helvetica, arial; font-size: 13px; background: #fff;">
	<div>
		<div>
			<div >
				<img src="assets/images/logo_ayo2.png" alt="" height="80" style="float: left; margin-right: 30px;">
				<span style="float: left; font-size: 22px; font-weight: bold; margin-top: 10px;">Paket 3D2N Padang</span>
				<span style="float: right; text-align: left;">
					KODE PESANAN
					<strong style="display: block; color: #019198; font-size: 24px;">B46DF</strong>
				</span>
			</div>

			<div style="clear: both; border-bottom: 1px solid #eee; padding-top: 20px; margin-bottom: 20px;"></div>
			<div>
				<div>
					<b>Pembayaran:</b> Full Payment / 6 bln cicilan<br>
					<b>Suplement:</b> Yes
				</div>
			</div>
			<div class="info_box">
				<div style="float: left; width: 45%;">
					<div style="float: left; margin-right: 20px;">
						<img src="assets/images/ico_time.png" alt="" height="60">
					</div>
					<label style="font-size: 20px;
					font-weight: bold; float: left; width: 40%; margin-top: 10px;">
						<div style="color: #9B9B9B; font-size: 13px; font-weight: normal;">Berangkat</div>
						19-07-2017
					</label>
					<label style="font-size: 20px;
					font-weight: bold;  margin-top: 10px; float: left;">
						<div style="color: #9B9B9B; font-size: 13px; font-weight: normal;">Kembali</div>
						19-07-2017
					</label>
					<div class="clearfix"></div>
				</div>
				<div  style="float: left; width: 25%; margin-right: 30px;">
					<div style="float: left; margin-right: 20px;">
						<img src="assets/images/ico_dewasa.png" alt=""  height="60">
					</div>
					<div style="font-size: 20px; font-weight: bold; margin-top: 20px;"> 2 Wisatawan</div>
					<div class="clearfix"></div>
				</div>
				<div  style="float: left; width: 20%;">
					<div style="font-size: 13px;">TOTAL</div>
					<strong style="font-size: 20px;">Rp.12.000.000,-</strong>
				</div>
				<div style="clear: both;"></div>
			</div>
			<div style="clear: both; border-bottom: 1px solid #eee; padding-top: 20px; margin-bottom: 20px;"></div>
			<div class="infowisatawan">
				<h2>Info Pemesan</h2>
				<div style="font-size: 15px; line-height: 150%;">
					<div style="float: left; width: 450px;">
						<div style="float: left; width: 150px;">Nama</div>
						<div style="float: left; width: 250px;"><b>Mr. Rudianto Sucipto</b></div>
						<div style="clear: both;"></div>
						<div style="float: left; width: 150px;">Email</div>
						<div style="float: left; width: 250px;"><b>mail@mail.com</b></div>
						<div style="clear: both;"></div>
						<div style="float: left; width: 150px;">No. Tlp</div>
						<div style="float: left; width: 250px;"><b>0812932112</b></div>
					</div>
					<div style="clear: both; padding-top: 20px;"></div>
				</div>
				<br><br>
				<h2>Info Wisatawan</h2>
				<div style="font-size: 15px; line-height: 150%;">
					<div style="float: left; width: 40px">1</div>
					<div style="float: left; width: 450px;">
						<div style="float: left; width: 150px;">Nama</div>
						<div style="float: left; width: 250px;"><b>Mr. Rudianto Sucipto</b></div>
						<div style="clear: both;"></div>
						<div style="float: left; width: 150px;">Tanggal Lahir</div>
						<div style="float: left; width: 250px;"><b>3 Januari 1992</b></div>
						<div style="clear: both;"></div>
						<div style="float: left; width: 150px;">ID Number</div>
						<div style="float: left; width: 250px;"><b>123123123</b></div>
						<div style="clear: both;"></div>
						<div style="float: left; width: 150px;">No. Tlp</div>
						<div style="float: left; width: 250px;"><b>0812932112</b></div>
					</div>
					<div style="clear: both; padding-top: 20px;"></div>
				</div>
				<div style="font-size: 15px; line-height: 150%;">
					<div style="float: left; width: 40px">2</div>
					<div style="float: left; width: 450px;">
						<div style="float: left; width: 150px;">Nama</div>
						<div style="float: left; width: 250px;"><b>Mr. Rudianto Sucipto</b></div>
						<div style="clear: both;"></div>
						<div style="float: left; width: 150px;">Tanggal Lahir</div>
						<div style="float: left; width: 250px;"><b>3 Januari 1992</b></div>
						<div style="clear: both;"></div>
						<div style="float: left; width: 150px;">ID Number</div>
						<div style="float: left; width: 250px;"><b>123123123</b></div>
						<div style="clear: both;"></div>
						<div style="float: left; width: 150px;">No. Tlp</div>
						<div style="float: left; width: 250px;"><b>0812932112</b></div>
					</div>
					<div style="clear: both; padding-top: 20px;"></div>
				</div>
				
			</div>
			<br>
			<div style="clear: both;"></div>
		</div>
	</div>
</div>
</body>
</html>