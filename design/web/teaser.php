<!doctype html>	
<html>
<?php include "includes/head.php";?>
<style>
	.counting {
		width: 430px;
		position: absolute;
		z-index: 2;
		left: calc(50% - 210px);
		top: 50%;
		color: #fff;
		font-size: 20px;
		text-align: center;
	}
	.defaultCountdown {
		text-align: center;
	}
	.countdown-section {
		float: left;
		margin: 10px 20px;
	}
	.countdown-section .countdown-amount,
	.countdown-section .countdown-period {
		display: block;
		text-align: center;
	}
	.countdown-section .countdown-amount {
		font-weight: bold;
		font-size: 48px;
		line-height: 120%;
	}
	.countdown-section .countdown-period {
		text-transform: ;
	}
	.home header .logo {
		left: calc(50% - 200px);
	}
	.home header.smaller .logo {
		left: calc(50% - 50px);
	}
	.list1 .more {
		background: grey !important;
	}
	@media screen and (max-width: 650px) {
		.counting {
			width: 300px;
			left: calc(50% - 135px);
			top: 40%;
			font-size: 16px;
		}
		.countdown-section {
			margin: 10px;
		}
		.countdown-section .countdown-amount {
			font-size: 32px;
		}
		
	}

</style>
<body class="home">
<header>
	<div class="container">
		<a href="index.php"><img src="assets/images/logo_ayo2_small.png" alt="" class="logo"></a>
		<img src="assets/images/logo_ayo2_small.png" alt="" class="logo_ayo">
		
	</div>
</header>
<!-- s:cover -->
<section id="cover">
	<div id="slide_cover">
		<div class="ratio16_9 box_img">
			<a href="#">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="assets/images/logo_ayo2.png" alt="">
				</div>
				<div class="img_con lqd">
					<img src="assets/images/c2.jpg" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="#">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="assets/images/logo_ayo2.png" alt="">
				</div>
				<div class="img_con lqd">
					<img src="assets/images/c1.jpg" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="#">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="assets/images/logo_ayo2.png" alt="">
				</div>
				<div class="img_con lqd">
					<img src="assets/images/c3.jpg" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="#">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="assets/images/logo_ayo2.png" alt="">
				</div>
				<div class="img_con lqd">
					<img src="assets/images/c4.jpg" alt="" class="bg">
				</div>
			</a>
		</div>
		<div class="ratio16_9 box_img">
			<a href="#">
				<div class="bgfloat2"></div>
				<div class="text">
					<img src="assets/images/logo_ayo2.png" alt="">
				</div>
				<div class="img_con lqd">
					<img src="assets/images/c5.jpg" alt="" class="bg">
				</div>
			</a>
		</div>
	</div>
	<img src="assets/images/logo_5star_color.png" alt="" class="bni_logo">
	<img src="assets/images/bni_pesona.png" class="bni_card box_modal_full" alt="bni_card.php">
	<div class="bgfloat2"></div>
	<a href="#intro" class="more">
		<!-- EXPLORE MORE<br> -->
		<img src="assets/images/expand more.png" alt="">
	</a>
	<div class="counting">
		<h3>COMMING SOON</h3>
		<div id="defaultCountdown"></div>
	</div>
	
</section>

<!-- e:cover -->
<div class="container_full">
<!-- s:intro -->
<section id="intro" class="section">
	<div class="container">
		<h2 class="title"><span style="color: #FCB03B !important;">#Ayo</span><span style="color: #008C9A !important;">Liburan</span> bersama Garuda Indonesia</h2>
Mau ngerasain asyiknya picnic sambil barbekyu-an di Pantai Pink (Komodo) atau di salah satu pulau cantik di Belitung? #AyoLiburan bareng Garuda Indonesia!
Selain bisa menikmati alam Indonesia yang nggak kalah keren sama  destinasi luar negeri, kamu bisa mencicipi sajian kuliner khas Nusantara dengan ditemani para Travel Influencers yang akan bikin liburan kamu tambah seru.
 <br><br>
Selain itu, kita juga punya aktivitas lain yang nggak kalah seru: Nyobain Mi Atep di Belitung, Minum Teh Talua Khas Minang, Berburu Blue Fire di  Kawah Ijen, Atau ke Museum Satwa di Batu Secret Zoo yang instagrammable banget buat kalian pecinta otomotif.

Tunggu apa lagi, yuk pesan paket #AyoLiburan pilihanmu sekarang!
</div>
</section>
<!-- e:intro -->
<!-- s:list -->

<div class="container">
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/e895301bcfa4b5b778f22f0077f1469e.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Chasing Sunrise in Bromo</h2>
			<div class="place">Malang & Bromo 3H2M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp.9.775.600,-</strong>
				<strong>Rp.4.365.000,- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a href="#" class="more">#AyoLiburan</a>
			
		</div>
	</article>
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/f6d6759821647a8f91d9e60bbf5e264d.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Starfish & The Powdery Sand</h2>
			<div class="place">Belitung 3H2M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp.8.525.000,-</strong>
				<strong>Rp.3.070.000,- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a href="#" class="more">#AyoLiburan</a>
			
		</div>
	</article>
	
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/a18e6b63be6bc5af9da56f11fa6d3ab5.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Sailing the Magnificent Komodo Islands</h2>
			<div class="place">Labuan Bajo & Kep. Komodo 4H3M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp.12.747.000,-</strong>
				<strong>Rp.5.625.000- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a href="#" class="more">#AyoLiburan</a>
			
		</div>
	</article>
	<article class="list1">
		<div class="box_img ratio_box ">
			<div class="img_con lqd">
				<img src="assets/images/img_padang.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Picnic Lunch in Pasumpahan Island</h2>
			<div class="place">Padang 3H2M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp.12.460.000,-</strong>
				<strong>Rp.3.880.000,- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a href="detail.php" class="more">#AyoLiburan</a>
			
		</div>
	</article>
	
	<article class="list1">
		<div class="box_img ratio_box ">
		
			<div class="img_con lqd">
				<img src="assets/images/img_banyuwangi.jpg" alt="">
			</div>
			<div class="bgfloat"></div>
		</div>
		<div class="text">
			<h2>Escape to The Blue Flames</h2>
			<div class="place">Banyuwangi 3H2M</div>
			<div class="clearfix"></div>
			<div class="price">
				<span>Harga mulai dari</span>
				<strong class="pc2">Rp.11.901.600,-</strong>
				<strong>Rp.5.805.000,- *</strong>
				<div class="note">*Syarat dan Ketentuan Berlaku</div>
			</div>
			<a href="detail.php" class="more">#AyoLiburan</a>
			
		</div>
	</article>
	
</div>
<div class="clearfix"></div>
</div>

<!-- e:list -->
<div class="clearfix"></div>
<div id="footer">
	<div class="container">
		<div class="foot_sosmed">
			<span>Find us on</span>
			<a href="https://www.facebook.com/garudaindonesia/" target="_blank"><img src="assets/images/ico-fb.png" alt=""></a>
			<a href="https://twitter.com/IndonesiaGaruda" target="_blank"><img src="assets/images/ico-tw.png" alt=""></a>
			<a href="https://www.instagram.com/garuda.indonesia/?hl=id" target="_blank"><img src="assets/images/ico-ig.png" alt=""></a>
			<a href="https://www.youtube.com/user/GarudaIndonesia1949" target="_blank"><img src="assets/images/ico-youtube.png" alt=""></a>
		</div>
		<div class="fl">
			<a href="#" class="box_modal_full" alt="bni_card.php"><img src="assets/images/logo_footer.png" alt=""></a>
			Copyright of Garuda Indonesia @2017 - Allright reserved
		</div>
		
		<div class="clearfix"></div>
	</div>
</div>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.carouFredSel-6.0.4-packed.js"></script>
<script type="text/javascript" src="assets/js/liquid.js"></script>
<script type="text/javascript" src="assets/js/header.js"></script>
<script type="text/javascript" src="assets/js/jquery.plugin.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.countdown.min.js"></script>
<script type="text/javascript" src="assets/js/controller.js"></script>
<script>

$('#defaultCountdown').countdown({until: new Date(2017, 7-1, 20)});
</script>


</body>
</html>