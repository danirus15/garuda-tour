<!doctype html>	
<html>
<?php include "includes/head.php";?>
<body>
<?php 
	include "includes/header.php";
?>
<section id="cover">
	<div id="slide_cover">
		<div class="ratio3_1 box_img">
			<div class="img_con lqd">
				<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/a18e6b63be6bc5af9da56f11fa6d3ab5.jpg" alt="">
			</div>
		</div>
		<div class="ratio3_1 box_img">
			<div class="img_con lqd">
				<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/46806afbe30f3abeb93fa437d264e84a.jpg" alt="">
			</div>
		</div>
		<div class="ratio3_1 box_img">
			<div class="img_con lqd">
				<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/4490fc158f1cde260b4bc62284638adc.jpg" alt="">
			</div>
		</div>
	</div>
	<img src="assets/images/arrow_left.png" alt="" class="nav_cover nav_left">
	<img src="assets/images/arrow_right.png" alt="" class="nav_cover nav_right">
</section>
<br><br>
<div class="container_full">
<div class="container" data-sticky_parent>
	<!-- s:detail_left -->
	<div class="detail_left">
		<h1>3 Days and 2 Nights at Labuan Bajo</h1>
		<h2>Ada Apa Saja Di Sana</h2>
		<div class="gal_img">
							<a class="box_img ratio4_3 " data-fancybox="images" href="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/0b86b1b255f96d3281223fc7d355f7e4.jpg">
					<div class="img_con lqd">
						<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/0b86b1b255f96d3281223fc7d355f7e4.jpg" alt="">
					</div>
				</a>
							<a class="box_img ratio4_3 " data-fancybox="images" href="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/a37ff45f38d4f9cdc1d9343135a391ea.jpg">
					<div class="img_con lqd">
						<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/a37ff45f38d4f9cdc1d9343135a391ea.jpg" alt="">
					</div>
				</a>
							<a class="box_img ratio4_3 " data-fancybox="images" href="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/1ebdddce3685db0e780adb20a7ab75f7.jpg">
					<div class="img_con lqd">
						<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/1ebdddce3685db0e780adb20a7ab75f7.jpg" alt="">
					</div>
				</a>
							<a class="box_img ratio4_3 " data-fancybox="images" href="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/bf954bea403cdd5543c1e19cc7e2ecf3.jpg">
					<div class="img_con lqd">
						<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/bf954bea403cdd5543c1e19cc7e2ecf3.jpg" alt="">
					</div>
				</a>
							<a class="box_img ratio4_3 " data-fancybox="images" href="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/4490fc158f1cde260b4bc62284638adc.jpg">
					<div class="img_con lqd">
						<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/4490fc158f1cde260b4bc62284638adc.jpg" alt="">
					</div>
				</a>
							<a class="box_img ratio4_3 " data-fancybox="images" href="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/46806afbe30f3abeb93fa437d264e84a.jpg">
					<div class="img_con lqd">
						<img src="http://ayoliburan.garuda-indonesia.com/uploads//2017/07/06/46806afbe30f3abeb93fa437d264e84a.jpg" alt="">
					</div>
				</a>
					</div>
		<div class="clearfix"></div>
		<h2>Yang Akan Kita Lakukan</h2>
		<div class="desc">
			<p>Di&nbsp;<strong>Hari Pertama</strong>, setelah sampai di Labuan Bajo kita akan melihat "perang" dalam bentuk tarian bernama&nbsp;<strong>Tari Caci di Desa Melo</strong>. Malam harinya kita akan menikmati sajian menu dari <strong>rumah makan Treetop</strong>&nbsp;di tengah kota Labuan Bajo, lalu beristirahat di&nbsp;<strong>Hotel La Prima</strong>.</p>
<p>Esok harinya, di&nbsp;<strong>Hari Kedua</strong>, setelah sarapan kita akan langsung naik&nbsp;<strong>kapal LOB</strong>&nbsp;di pelabuhan untuk memulai petualangan keliling Kepulauan Komodo. Jangan lupa pakai dan bawa sunblock ya! Oya, sebaiknya siapkan juga sepatu yg cukup baik untuk trekking. Petualangan island hopping hari ini kita mulai dari&nbsp;<strong>trekking di P. Rinca</strong>&nbsp;utk melihat Komodo, lalu&nbsp;<strong>snorkeling</strong>&nbsp;dan menikmati&nbsp;<strong>BBQ Seafood Lunch di Pink Beach</strong>, selanjutnya melihat ribuan kelelawar yang terbang saat&nbsp;<strong>senja di P. Kalong</strong>, dan malam harinya kita akan bersandar di teluk P. Padar utk&nbsp;<strong>bermalam di kapal. </strong></p>
<p>Di&nbsp;<strong>Hari Ketiga</strong>,&nbsp;kita akan merasakan serunya trekking di beberapa pulau. Tentu saja, sunblock is still a must! Pagi hari, kita akan&nbsp;<strong>trekking di P. Padar</strong>&nbsp;untuk menikmati view keren dari ketinggian, selanjutnya kita mengarungi lautan menuju&nbsp;<strong>Gili Lawa Darat</strong>&nbsp;dan lagi2 akan menikmati view dari ketinggian pulau ini, setelah itu kita akan merapat di<strong>&nbsp;Gili Lawa Laut</strong>&nbsp;untuk bermalam di kapal.</p>
<p><strong>Hari terakhir</strong>&nbsp;di Kapal LOB, kita akan menikmati indahnya coral dengan&nbsp;<strong>snorkeling di P. Kanawa&nbsp;</strong>dilanjutkan makan siang di kapal lalu kembali ke kota Labuan Bajo. Saatnya berkemas, tapi kita akan mampir belanja oleh-oleh sebelum tiba di Bandara Komodo dan terbang kembali ke Jakarta. Sampai ketemu di trip seru selanjutnya</p>
		</div>
		
		<h2>Yang Kami Sediakan</h2>
		<div class="desc">
			<p>
			Penerbangan 5-star bersama Garuda Indonesia dari Jakarta ke Labuan Bajo (pp). Penjemputan dari/ke Bandara Komodo dan juga tiket masuk Taman Nasional Komodo sudh termasuk dalam kegiatan ini.
			<br><br>
Menginap di Hotel La Prima 1 malam, lalu menikmati serunya berlayar 2 malam di Kepulauan Komodo bersama Kapal LOB xxxx.
<br><br>
Makan & minum sepanjang trip sesuai dengan itinerary pasti terjamin, dan termasuk Seafood BBQ Lunch di Pantai Merah (Pink Beach) yang menawan itu. Tidak lupa, kalian akan selalu didampingi oleh guide berpengalaman.</p>
		</div>
		
		
		<h2>Kenapa Harus Ikut Trip Ini</h2>
		<div class="desc">
			<p>Daerah paling Barat Pulau Flores ini memiliki sebuah tempat indah bernama Taman Nasional Komodo yang sudah terkenal di dunia. Nah, kota Labuan Bajo adalah gerbang menuju indahnya Taman Nasional Komodo tersebut. Keliling di pulau-pulau sekitar dengan Kapal Phinisi akan membuat pengalaman trip yang tak terlupakan.
Buat kamu pecinta pantai/laut, Kepulauan Komodo ini menjadi destinasi wajib di Indonesia. Semua sudut-nya sangat mengagumkan, tidak hanya di mata tapi juga seru dijadikan konten social media kamu. Tunggu apa lagi, the beach is calling!</p>
		</div>
		<!-- <div class="list_rp">
			<strong>Hari ke-1</strong>
			<ol>
				<li>Tiba di Bandara Internasional Minangkabau</li>
				<li>Pantai Air Manis</li>
				<li>Menuju Penginapan</li>
				<li>Malam di Bukittinggi</li>
			</ol>
			<strong>Hari ke-2</strong>
			<ol>
				<li>Tiba di Bandara Internasional Minangkabau</li>
				<li>Pantai Air Manis</li>
				<li>Menuju Penginapan</li>
				<li>Malam di Bukittinggi</li>
			</ol>
			<strong>Hari ke-3</strong>
			<ol>
				<li>Tiba di Bandara Internasional Minangkabau</li>
				<li>Pantai Air Manis</li>
				<li>Menuju Penginapan</li>
				<li>Malam di Bukittinggi</li>
			</ol>
		</div> -->
	</div>
	<!-- e:detail_left -->
	<!-- s:detail_right -->
	<div class="detail_right" data-sticky_column>
		<div class="box_">
			<form action="order.php" method="post">
			<div class="text">
				<div class="title">Paket 3D2N Labuan Bajo</div>
				<div class="group-input">
					<div class="ico">
						<img src="assets/images/ico_map.png" alt="">
					</div>
					<label class="input-date input-date_full">
						<span>Kota Keberangkatan</span>
						Jakarta
						<div class="select-style dnone">
							<select name="title" id="" required="">
								<option value="1">Jakarta</option>
							</select>
						</div>
					</label>
					
					<div class="clearfix"></div>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="assets/images/ico_time.png" alt="">
					</div>
					<!-- <label class="input-date input-date_full">
						<span>Tanggal</span>
						<div class="select-style">
							<select name="title" id="" required="">
								<option value="">Pilih Tanggal</option>
								<option value="">21 Juni 2017 - 24 Juni 2017</option>
								<option value="">25 Juli 2017 - 28 Juli 2017</option>
								<option value="">5 Agustus 2017 - 8 Agustus 2017</option>
							</select>
						</div>
					</label> -->
					<label class="input-date">
						<span>Berangkat</span>
						<input type="text" class="pilihtanggal" placeholder="Pilih tanggal" id="from" disabled="disabled" required>
					</label>
					<label class="input-date">
						<span>Kembali</span>
						<input type="text" disabled id="to">
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="assets/images/ico_dewasa.png" alt="">
					</div>
					<div class="info">Wisatawan</div>
					<!-- <div class="select-style select-style2">
						<select name="title" id="" required="">
							<option value="">0</option>
							<option value="">1</option>
							<option value="">2</option>
							<option value="">3</option>
							<option value="">4</option>
							<option value="">5</option>
							<option value="">6</option>
							<option value="">7</option>
							<option value="">8</option>
						</select>
					</div> -->
					<div class="input_num">
						 <input type='button' value='-' class='qtyminus' field='quantity' />
					    <input type='text' name='quantity' id="quantity" value='0' class='qty' disabled="" required />
					    <input type='button' value='+' class='qtyplus' field='quantity' />
					</div>
					<div class="clearfix"></div>
				</div>
				
			</div>
			
			<div class="price">
				HARGA
				<div class="price2">
					Harga Normal
					<div class="status">Sisa 15</div>
					<strong>Rp.5.000.000,-</strong>
				</div>
				<div class="bni_price">
					Cashback <b>Rp 1.000.000</b><br>
					untuk Kartu Kredit BNI Style Titanium, 
					dan cashback <b>Rp 500.000</b><br>
					untuk Kartu Kredit BNI lainnya (Mastercard, Visa, JCB) 
				</div>
				<div class="soldout_now">
					<span>SOLD OUT</span>
				</div>
			</div>
			<!-- <div class="total">
				<div class="num">
					TOTAL
					<strong class="total_cost">Rp.5.746.400,-</strong>
				</div>
				<div class="clearfix"></div>
			</div> -->
			<!-- <div align="center">
				<input type="submit" class="btn_submit" value="PESAN SEKARANG">
			</div> -->
			</form>
		</div>
	</div>
	<!-- e:detail_right -->
	<div class="clearfix"></div>
</div>
</div>
<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
<script type='text/javascript'>

$(function() {
	$('.qtyminus').hide();
    $('.qtyplus').hide();
	var array = ["2017-05-19","2017-07-14","2017-07-21","2017-07-28"]
    $( "#from, #to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        beforeShowDay: function(date){
	        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
	        return [ array.indexOf(string) >= 0 ]
	    },
        onSelect: function( selectedDate ) {
            if(this.id == 'from'){
              var dateMin = $('#from').datepicker("getDate");
              var rMin = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 1); 
              //set jumlah hari
              var rMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + 3); 
			$('#to').val($.datepicker.formatDate('mm-dd-yy', new Date(rMax)));                    
            }

            $('.qtyminus').show();
    		$('.qtyplus').show();
        }
    });

});


</script>
</body>
</html>