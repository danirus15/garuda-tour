<!doctype html>	
<html>
<?php include "includes/head.php";?>
<body>
<?php 
	include "includes/header.php";
?>
<div class="white_block">
	<div class="container container2">
		<h1>Aktivasi Akun Berhasil</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Negat esse eam, inquit, propter se expetendam. Hanc quoque iucunditatem, si vis, transfer in animum; Profectus in exilium Tubulus statim nec respondere ausus;</p>
		<p>Primum in nostrane potestate est, quid meminerimus? Istam voluptatem, inquit, Epicurus ignorat? Beatum, inquit. Utilitatis causa amicitia est quaesita. Ille incendat? Si quicquam extra virtutem habeatur in bonis. Quodcumque in mentem incideret, et quodcumque tamquam occurreret. Nos paucis ad haec additis finem faciamus aliquando; Cui Tubuli nomen odio non est?</p>

		<br>
		<a href="login.php" alt="login.php" class="btn box_modal_full">Login Sekarang</a>
	</div>
</div>

<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
<script type='text/javascript'>
$(window).load(function(){
	var array = ["2017-05-19","2017-05-26","2017-05-12","2017-05-05"]
	  $('.pilihtanggal').datepicker({
	    beforeShowDay: function(date){
	        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
	        return [ array.indexOf(string) >= 0 ]
	    }
	});
});
</script>
</body>
</html>