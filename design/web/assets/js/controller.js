
jQuery(document).ready(function(){
    // This button will increment the value
    $('.qtyplus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $('input[name='+fieldName+']').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });
    // This button will decrement the value till 0
    $(".qtyminus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('input[name='+fieldName+']').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });
});

$(document).ready(function(){
  function init() {
      window.addEventListener('scroll', function(e){
          var distanceY = window.pageYOffset || document.documentElement.scrollTop,
              shrinkOn = 150,
              header = document.querySelector("header");
          if (distanceY > shrinkOn) {
              classie.add(header,"smaller");
          } else {
              if (classie.has(header,"smaller")) {
                  classie.remove(header,"smaller");
              }
          }
      });
  }
  window.onload = init();
});

$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      target2 = $(this.hash);
      var hashtag = $(this).attr("href");
      
      if (target.length) {
          //alert(hashtag);
          $('html,body').animate({
            scrollTop: target.offset().top
          }, 700);
          /*$('html,body').animate({
            margin-
          }, 700);*/
          $("div" + hashtag).css("padding-top", "100px");
          return false;
      }
    }
  });
});

/*s:slide*/
$("#slide_cover").carouFredSel({
  circular: true,
  infinite: true,
  auto  : 50000,
  responsive: true,
  items: 1,
  scroll  : {
    fx : "crossfade"
  },
  next: '.nav_right',
  left: '.nav_left',
});

/*S:LIQUID IMAGE*/
//liquid image
$(document).ready(function() {
    $(".lqd").imgLiquid();
    
    var boxwis = $('.box_wisatawan').length;
    if(boxwis == 1) {
      //alert(boxwis);
      $(".box_wisatawan").addClass("box_wisatawan2");
    }
});
/*E:LIQUID IMAGE*/

//$.validate();
$.validate({
  modules : 'security'
});


//STICKY
if ($(window).width() > 560) {
$(window).load(function() {
    return $("[data-sticky_column]").stick_in_parent({
            parent: "[data-sticky_parent]",
            offset_top: 90,
            inner_scrolling: false
        })
        .on('sticky_kit:bottom', function(e) {
            $(this).parent().css('position', 'static');
        })
        .on('sticky_kit:unbottom', function(e) {
            $(this).parent().css('position', 'relative');
        });

      $(document).delay(500).find(".text_area").trigger("sticky_kit:recalc");
});
}
else {}
/*e:sticky*/


/*S:MODALBOX*/
if( $('div.pop_box').attr('id') == 'pop_box_now'){
    $("body").css('overflow','hidden');
}
else {
  $("body").css('overflow','scroll');
}

$(".box_modal").click(function (){
  if( $('div').attr('id') == 'pop_box_now'){}
  else{ 
    var src  = $(this).attr("alt");
    size   = src.split('|');
        url      = size[0],
        width    = size[1],
        height   = size[2],
        tops   = 'calc(50% - '+ (height/2) +'px)';
        tops2  = '-webkit-calc(50% - '+ (height/2) +'px)';

    $("body").append( "<div class='pop_box' id='pop_box_now'><iframe frameborder='0' id='framebox' src=''></iframe></div>" );
    $("#framebox").animate({
      height: height,
      width: width,
    },0).attr('src',url).css('top',tops).css('top',tops2);
    $("body").css('overflow','hidden');
  }
});

$(window).load(function(){
  $(".box_modal_full").click(function (){
    if( $('div').attr('id') == 'pop_box_now'){}
    else{ 
      $(this).removeAttr('href');
      var src  = $(this).attr("alt");
      size   = src.split('|');
      url      = size[0],
      width    = '100%',
      height   = '100%'
  
      $("body").append( "<div class='pop_box' id='pop_box_now'><iframe frameborder='0' id='framebox' src=''></iframe></div>" );
      $("#framebox").animate({
        height: height,
        width: width,
      },0).attr('src',url).css('position','fixed').css('top','0').css('left','0');
      $("body").css('overflow','hidden');
    }
    rescale();
  });
  $(function() {
    $(".pop_container").wrapInner( "<div id='pop_wrap'></div>" );
    $('#pop_wrap').css('height',$(window).height());
    $('#pop_wrap').css('width',$('#pop_wrap').parent('.pop_container').width());
  });
  function rescale(){
    var size = {width: $(window).width() , height: $(window).height() }
    $('#pop_wrap').css('height', size.height );
  }
  $(window).bind("resize", rescale);
  $(".box_modal2").click(function (){
    $("#pop_box2").show();
    $("body").css('overflow','hidden');
  });
  $(".box_modal3").click(function (){
    $("#pop_box3").show();
    $("body").css('overflow','hidden');
  });

  $(function() {
    var tinggipop = $(".container_pop").height();
    //alert(tinggipop);
    var tinggipop2 = tinggipop/2;
    $(".container_pop").css('top','calc(50% - '+ tinggipop2 +'px)');
    //alert(tinggipop2);

  });




  function pop_next(src){
    size   = src.split('|');
    url      = size[0],
    width    = size[1],
    height   = size[2],
    tops   = 'calc(50% - '+ (height/2) +'px)';
    tops2  = '-webkit-calc(50% - '+ (height/2) +'px)';
    $("#framebox").animate({
      height: height,
      width: width,
    },0).attr('src',url).css('top',tops).css('top',tops2);
  };
});

function closepop()
{ 
  $("#pop_box_now").remove();
  $("#pop_box2").hide();
  $("#pop_box3").hide();
  $("body").css('overflow','scroll');
};
$(".close_box").click(function (){
  closepop();
});
$(".close_box_in").click(function (){
  parent.closepop();
});
$(".pop_next").click(function (){
  var src = $(this).attr("alt");
  parent.pop_next(src);
});
/*S:MODALBOX*/

/**
 * Credit.js
 * Version: 1.0.0
 * Author: Ron Masas
 */

(function( $ ){

    $.fn.extend({
        credit: function ( args ) {

          $(this).each(function (){


          // Set defaults
      var defaults = {
        auto_select:true
      }

      // Init user arguments
      var args = $.extend(defaults,args);

          // global var for the orginal input
          var credit_org = $(this);

            // Hide input if css was not set
            credit_org.css("display","none");

            // Create credit control holder
          var credit_control = $('<div></div>',{
            class: "credit-input"
          });

          // Add credit cell inputs to the holder
          for ( i = 0; i < 4; i++ ) {
              credit_control.append(
                  $("<input />",{
                    class: "credit-cell",
                    placeholder: "0000",
                    maxlength: 4
                  })
                );
          }

          // Print the full credit input
          credit_org.after( credit_control );

          // Global var for credit cells
          var cells = credit_control.children(".credit-cell");

          /**
       * Set key press event for all credit inputs
       * this function will allow only to numbers to be inserted.
       * @access public
       * @return {bool} check if user input is only numbers
       */
      cells.keypress(function ( event ) {
        // Check if key code is a number
        if ( event.keyCode > 31 && (event.keyCode < 48 || event.keyCode > 57) ) {
              // Key code is a number, the `keydown` event will fire next
              return false;
          }
          // Key code is not a number return false, the `keydown` event will not fire
          return true;
      });

      /**
       * Set key down event for all credit inputs
       * @access public
       * @return {void}
       */
      cells.keydown(function ( event ) {
        // Check if key is backspace
        var backspace = ( event.keyCode == 8 );
        // Switch credit text length
        switch( $(this).val().length ) {
          case 4:
            // If key is backspace do nothing
            if ( backspace ) {
              return;
            }
            // Select next credit element
            var n = $(this).next(".credit-cell");
            // If found
            if (n.length) {
              // Focus on it
              n.focus();
            }
          break;
          case 0:
            // Check if key down is backspace
            if ( !backspace ) {
              // Key is not backspace, do nothing.
              return;
            }
            // Select previous credit element
            var n = $(this).prev(".credit-cell");
            // If found
            if (n.length) {
              // Focus on it
              n.focus();
            }
          break;
        }
      });

      // On cells focus
      cells.focus( function() {
          // Add focus class
          credit_control.addClass('c-focus');
      });

      // On focus out
      cells.blur( function() {
          // Remove focus class
          credit_control.removeClass('c-focus');
      });

      /**
       * Update orginal input value to the credit card number
       * @access public
       * @return {void}
       */
      cells.keyup(function (){
        // Init card number var
        var card_number = '';
        // For each of the credit card cells
        cells.each(function (){
          // Add current cell value
          card_number = card_number + $(this).val();
        });
        // Set orginal input value
        credit_org.val( card_number );
      });


      if ( args["auto_select"] === true ) {
        // Focus on the first credit cell input
        credit_control.children(".credit-cell:first").focus();
      }

      });
              
        }
    });

})(jQuery);

jQuery(function ( $ ){
  $(".credit").credit();
});


