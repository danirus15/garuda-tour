<!doctype html>	
<html>
<?php include "includes/head.php";?>
<body>
<?php 
	include "includes/header.php";
?>
<div class="nav_order">
	<div class="container">
		<div class="page">2. Informasi Wisatawan</div>
		<div class="order_pos">
			<span class="selected">1. Pilih & Pesan</span>
			<span class="selected">2. Data Wisatawan</span>
			<span class="selected">3. Konfirmasi & Bayar</span>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container" data-sticky_parent>
	<form action="payment_snap2.php" method="post">
	<!-- s:detail_left -->
	<div class="detail_left">
		<h3 class="title3">Informasi Kartu</h3>
		<div class="notif m10">
			Batas waktu pembayaran 20 menit, bila tidak melakukan pembayaran makan pesanan akan di anggap batal.
		</div>
		<div class="box_kontak">
			<div class="t_payment">
				<h1>Pembayaran hanya bisa melalui Kartu Kredit BNI Mastercard</h1>
			</div>
			<div class="text">
			<b>Pilih jenis Kartu Kredit BNI Mastercard Anda:</b>
			<div class="clearfix"></div>
			<label class="select_card">
				<input type="radio" name="card">
				<img src="assets/images/bni_other.png" alt="">
				BNI VISA or JCB
			</label>
			<label class="select_card">
				<input type="radio" name="card">
				<img src="assets/images/bni_mastercard.png" alt="">
				BNI Mastercard
			</label>
			<label class="select_card">
				<input type="radio" name="card">
				<img src="assets/images/bni_titanium.png" alt="">
				BNI Mastercard Titanium
			</label>
			</div>
			<div class="clearfix"></div>
			<div style="padding: 20px;">
			<div class="group-input">
				<label>Cicilan BNI</label>
				<div class="select-style">
					<select name="lahir-tgl" id="" class="" required="">
						<option value="">Full Payment</option>
						<option value="">6</option>
						<option value="">12</option>
					</select>
				</div>
			</div>
			</div>
		</div>
		<div class="clearfix"></div>

	</div>
	<!-- e:detail_left -->
	<!-- s:detail_right -->
	<div class="detail_right" data-sticky_column>
		<div class="box_">
			<div class="title2">Pesanan Anda</div>
			
			<div class="text">
				<div class="group-input">
					Tujuan
					<h2>Belitung 3H2D</h2>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="assets/images/ico_time.png" alt="">
					</div>
					<label class="input-date">
						<span>Berangkat</span>
						17-07-2017
					</label>
					<label class="input-date">
						<span>Kembali</span>
						19-07-2017
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="assets/images/ico_dewasa.png" alt="">
					</div>
					<div class="info">Wisatawan</div>
					<div class="input_num">
						<input type='text' name='quantity' value='3' class='qty' disabled/>
					</div>
					<div class="clearfix"></div>
					<ol class="list_nama">
						<li>Rudianto</li>
						<li>Budiono</li>
						<li>Sucipto</li>
					</ol>
				</div>
			</div>
			<div class="text simulasi_cicil">
				<h3>Simulasi Cicilan</h3>
				<div class="fl">6 Bulan Bunga 0%</div>
				<div class="fr">Rp 300.000</div>
				<div class="clearfix"></div>
				<div class="fl">12 Bulan Bunga 0%</div>
				<div class="fr">Rp 150.000</div>
				<div class="clearfix"></div>
			</div>
			<div class="total total2">
				<div class="num">
					TOTAL 
					<b>Rp.12.000.000,-</b>
					<!-- BNI Discount
					<b>Rp.500.000,-</b> -->
					TOTAL PAYMENT
					<strong>Rp.12.000.000,-</strong>
				</div>
				<div class="clearfix"></div>
			</div>
			<div align="center">
				<input type="submit" class="btn_submit" value="KONFIRMASI PEMBAYARAN">
			</div>
		</div>
	</div>
	<!-- e:detail_right -->
	</form>
	<div class="clearfix"></div>
</div>

<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
<script>
	$('.select_card').click(function(){
		$(".select_card").removeClass("selected");
		$(this).addClass("selected");
	});
	$('.select_card').click(function(){
		$(".select_card").removeClass("selected");
		$(this).addClass("selected");
	});
</script>

</body>
</html>