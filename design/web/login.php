<!doctype html>	
<html>
<?php include "includes/head.php";?></html>
<body class="body_pop">
<div class="container_pop">
	<img src="assets/images/btn_close.png" alt="" class="close_pop close_box_in">
	<div class="title_pop">LOGIN</div>
	<div class="notif">
		Login gagal, silahkan cek kembali email dan password
	</div>
	<form action="index.php?login=1" method="post" target="_parent">
		<div class="group-input">
			<label>ALAMAT EMAIL</label>
			<input type="text" placeholder="Email Address" data-validation="email" required>
		</div>
		<div class="group-input">
			<label>PASSWORD</label>
			<input type="password" placeholder="Password" required>
		</div>
		<div class="clearfix"></div>
		<div align="center">
			<input type="submit" value="LOGIN" class="btn">
			<div class="clearfix"></div>
			<a href="password.php">Lupa Password</a>
			<br><br>
			Belum Terdaftar?<br>
			<a href="register.php">Daftar Sekarang</a>
		</div>

	</form>
</div>

<?php include "includes/js.php";?>
</body>
</html>