<!doctype html>	
<html>
<?php include "includes/head.php";?>
<body class="body_pop">
<img src="assets/images/btn_close.png" alt="" class="close_pop close_box_in close_box_out">
<div class="container_pop container_pop2">
	<div class="box_ box_2">
		<div class="text">
			<div class="title">
				<img src="assets/images/logo_ayo2.png" alt="" height="80" class="logoinv">
				Paket 3D2N Padang
				<span class="code">
					KODE PESANAN
					<strong>B46DF</strong>
				</span>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
			<div>
				<div>
					<b>Pembayaran:</b> Full Payment / 6 bln cicilan<br>
					<b>Suplement:</b> Yes
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="info_box">
				<div class="group-input info_time">
					<div class="ico">
						<img src="assets/images/ico_time.png" alt="">
					</div>
					<label class="input-date">
						<span>Berangkat</span>
						17-07-2017
					</label>
					<label class="input-date">
						<span>Kembali</span>
						19-07-2017
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="group-input info_wis">
					<div class="ico">
						<img src="assets/images/ico_dewasa.png" alt="">
					</div>
					<div class="info"> 2 Wisatawan</div>
					<div class="clearfix"></div>
				</div>
				<div class="info_total">
					TOTAL
					<strong>Rp.12.000.000,-</strong>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="infowisatawan">
				<h3>Info Pemesan</h3>
				<div class="iw">
					<div class="isi">
						<div class="jdl">Nama</div>
						<div class="isi2">Mr. Rudianto Sucipto</div>
						<div class="clearfix"></div>
						<div class="jdl">Email</div>
						<div class="isi2">mail@mail.com</div>
						<div class="clearfix"></div>
						<div class="jdl">No. Telepon</div>
						<div class="isi2">081923222211</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<br><br>
				<h3>Info Wisatawan</h3>
				<div class="iw">
					<div class="no">1</div>
					<div class="isi">
						<div class="jdl">Nama</div>
						<div class="isi2">Mr. Rudianto Sucipto</div>
						<div class="clearfix"></div>
						<div class="jdl">Tanggal Lahir</div>
						<div class="isi2">3 Januari 1992</div>
						<div class="clearfix"></div>
						<div class="jdl">ID Number</div>
						<div class="isi2">12345552132132</div>
						<div class="clearfix"></div>
						<div class="jdl">No. Telepon</div>
						<div class="isi2">081923222211</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="iw">
					<div class="no">2</div>
					<div class="isi">
						<div class="jdl">Nama</div>
						<div class="isi2">Mr. Rudianto Sucipto</div>
						<div class="clearfix"></div>
						<div class="jdl">Tanggal Lahir</div>
						<div class="isi2">3 Januari 1992</div>
						<div class="clearfix"></div>
						<div class="jdl">ID Number</div>
						<div class="isi2">12345552132132</div>
						<div class="clearfix"></div>
						<div class="jdl">No. Telepon</div>
						<div class="isi2">081923222211</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<br>
			<div class="clearfix"></div>
			<div class="line"></div>
			<div align="center">
				<a href="#" class="btn">DOWNLOAD PDF</a>
				<a href="#" class="btn" onclick="window.print();">PRINT</a>
			</div>
		</div>
	</div>
</div>
<?php include "includes/js.php";?>
</script>
</body>
</html>