<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="This is an example of a meta description. This will often show up in search results.">
<link rel="shortcut icon" href="assets/images/favicon.png">
<link rel="apple-touch-icon" href="assets/images/favicon.png"/> 
<title>Garuda Indonesia-Ayo Liburan</title>
<link href="assets/css/style.css" rel="stylesheet" type="text/css" charset="utf-8"/>
<link href="https://fonts.googleapis.com/css?family=Cabin:400,700|Dosis:400,700|Raleway:400,700|Hind:400,700|Nunito:400,700|Open+Sans:400,700" rel="stylesheet">



<!-- s:tes font -->



<?php 
	if(isset($_GET['font']) && $_GET['font']=="cabin"){
		?>
	<style>
		body {
			font-family: 'Cabin', sans-serif;
		}
	</style>
<?php
}
elseif(isset($_GET['font']) && $_GET['font']=="raleway"){
	?>
	<style>
		body {
			font-family: 'Raleway', sans-serif;
		}
	</style>
	<?php
}
elseif(isset($_GET['font']) && $_GET['font']=="dosis"){
	?>
	<style>
		body {
			font-family: 'Dosis', sans-serif;
		}
	</style>
	<?php
}
elseif(isset($_GET['font']) && $_GET['font']=="hind"){
	?>
	<style>
		body {
			font-family: 'Hind', sans-serif;
		}
	</style>
	<?php
}
elseif(isset($_GET['font']) && $_GET['font']=="nunito"){
	?>
	<style>
		body {
			font-family: 'Nunito', sans-serif;
		}
	</style>
	<?php
}
elseif(isset($_GET['font']) && $_GET['font']=="opensans"){
	?>
	<style>
		body {
			font-family: 'Open Sans', sans-serif;
		}
	</style>
	<?php
}
else{

}
?>