<!doctype html>	
<html>
<?php include "includes/head.php";?>
<body>
<?php 
	include "includes/header.php";
?>
<div class="white_block">
	<div class="container container2">
		<h1>Maaf! Pembayaran Anda Gagal</h1>
		<p>Halo Rudianto, mohon maaf pembayaran yang kamu lakukan gagal. Mohon untuk melakukan pengecekan kembali dan mengulang proses pembayaran.</p>

		<br>
		<a href="index.php" class="btn">Kembali ke home</a>
	</div>
</div>

<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
<script type='text/javascript'>
$(window).load(function(){
	var array = ["2017-05-19","2017-05-26","2017-05-12","2017-05-05"]
	  $('.pilihtanggal').datepicker({
	    beforeShowDay: function(date){
	        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
	        return [ array.indexOf(string) >= 0 ]
	    }
	});
});
</script>
</body>
</html>