<!doctype html>	
<html>
<?php include "includes/head.php";?>
<body>
<?php 
	include "includes/header.php";
?>
<div class="nav_order">
	<div class="container">
		<div class="page">3. Konfirmasi & Bayar</div>
		<div class="order_pos">
			<span class="selected">1. Pilih & Pesan</span>
			<span class="selected">2. Data Wisatawan</span>
			<span class="selected">3. Konfirmasi & Bayar</span>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container">
	<form action="payment_success.php" method="post">
	<!-- s:detail_right -->
	<div class="detail_right detail_konfirmasi">
		<div class="box_">
			<div class="title2">Konfirmasi Pesanan Anda</div>
			<div class="text">
				<div class="group-input">
					Tujuan
					<h2>Belitung 3H2D</h2>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="assets/images/ico_time.png" alt="">
					</div>
					<label class="input-date">
						<span>Berangkat</span>
						17-07-2017
					</label>
					<label class="input-date">
						<span>Kembali</span>
						19-07-2017
					</label>
					<div class="clearfix"></div>
				</div>
				<div class="group-input">
					<div class="ico">
						<img src="assets/images/ico_dewasa.png" alt="">
					</div>
					<div class="info">Wisatawan</div>
					<div class="input_num">
						<input type='text' name='quantity' value='3' class='qty' disabled/>
					</div>
					<div class="clearfix"></div>
					<ol class="list_nama">
						<li>Rudianto</li>
						<li>Budiono</li>
						<li>Sucipto</li>
					</ol>
				</div>
			</div>
			<div class="text simulasi_cicil">
				<h3>Simulasi Cicilan</h3>
				<div class="fl">6 Bulan Bunga 0%</div>
				<div class="fr">Rp 300.000</div>
				<div class="clearfix"></div>
				<div class="fl">12 Bulan Bunga 0%</div>
				<div class="fr">Rp 150.000</div>
				<div class="clearfix"></div>
			</div>
			<div class="total total2">
				<div class="num">
					TOTAL 
					<b>Rp.12.000.000,-</b>
					<!-- BNI Discount
					<b>Rp.500.000,-</b> -->
					TOTAL PAYMENT
					<strong>Rp.12.000.000,-</strong>
				</div>
				<div class="clearfix"></div>
			</div>
			<div align="center">
				<input type="submit" class="btn_submit" value="KONFIRMASI PEMBAYARAN">
			</div>
		</div>
	</div>
	<!-- e:detail_right -->
	</form>
	<div class="clearfix"></div>
	<br>
</div>
<?php if($_GET['page']=="loading"){
	echo'
	<div class="loading_page">
		<div class="loader">
		    <span></span>
		    <span></span>
		    <span></span>
		    <br><br>
		    Please Wait
		</div>
	</div>';
}
?>

<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
<script>
	$('.select_card').click(function(){
		$(".select_card").removeClass("selected");
		$(this).addClass("selected");
	});
	$('.select_card').click(function(){
		$(".select_card").removeClass("selected");
		$(this).addClass("selected");
	});
</script>

</body>
</html>