
<!doctype html>	
<html>
<?php include "includes/head.php";?>
<body>
<?php 
	include "includes/header.php";
?>
<body>
<header>
	<div class="container">
		<a href="http://ayoliburan.garuda-indonesia.com/"><img src="http://ayoliburan.garuda-indonesia.com/assets/frontend/images/logo_ayo2_small.png" alt="" class="logo"></a>
		<img src="http://ayoliburan.garuda-indonesia.com/assets/frontend/images/logo_ayo2_small.png" alt="" class="logo_ayo">
		<div class="language">
			<img src="http://ayoliburan.garuda-indonesia.com/assets/frontend/images/ico_language.png" alt="">
			<span>
									ID
							</span>
			<img src="http://ayoliburan.garuda-indonesia.com/assets/frontend/images/ico_expand.png" alt="" class="arrow">
			<div class="box_bahasa">
				<a href="http://ayoliburan.garuda-indonesia.com/language/id?callback=http://ayoliburan.garuda-indonesia.com/helpcenter.php">Bahasa Indonesia</a>
				<a href="http://ayoliburan.garuda-indonesia.com/language/en?callback=http://ayoliburan.garuda-indonesia.com/helpcenter.php">English</a>
			</div>
		</div>
		<div class="user_login">
							<a href="http://ayoliburan.garuda-indonesia.com/login" alt="http://ayoliburan.garuda-indonesia.com/login?callback=http://ayoliburan.garuda-indonesia.com/destination/detail/11/belitung" class="box_modal_full">Masuk</a> /
				<a href="http://ayoliburan.garuda-indonesia.com/register" alt="http://ayoliburan.garuda-indonesia.com/register" class="box_modal_full">Daftar</a>
					</div>
		<!-- <div id="nav">
			<a href="index.php">Paket Ayo Liburan 2017</a>               
			<a href="#">Keuntungan tambahan</a>
		</div> -->

	</div>
</header>
<div class="container">
	<br><br>
	<div class="title_help"><h1>Help Center</h1></div>
	<div id="bahasa"></div>
	<div class="link_">
		<a href="#bahasa" class="selected">Bahasa Indonesia</a>
		<a href="#english">English</a>
	</div>
	
	<div class="title_help2">Hubungi Kami</div>
	<div class="text">
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam iste at quod tempora, mollitia earum ipsum voluptatibus facilis sit officia? Sed facilis consequuntur optio? Unde aliquid facilis, mollitia hic numquam.
	</div>
	<div class="title_help2">FAQ</div>
	<div class="text">
		<div class="q">1. Bagaimana Proses pemesanan paket wisata Ayo Liburan 2017?</div>
		<div class="a">Pemesanan dan pembelian paket wisata Ayo Liburan 2017 hanya dapat Anda lakukan pada (di) website resmi Ayo Liburan - Garuda Indonesia di ayoliburan.garuda-indonesia.com.</div>

		<div class="q">2. Kapan maksimal waktu sebelum keberangkatan,saya dapat melakukan proses pemesanan dan pembelian paket wisata Ayo Liburan 2017 ?</div>
		<div class="a">Pemesanan dan pembelian tiket paket wisata Ayo Liburan 2017, dapat dilakukan 1(satu) minggu atau 7 (tujuh) hari sebelum periode perjalanan.</div>

		<div class="q">3.	Bagaimana saya melakukan pembayaran untuk pesanan saya?</div>
		<div class="a">Pembayaran dapat Anda lakukan melalui website resmi ayoliburan.garuda-indonesia.com dengan menggunakan kartu kredit MasterCard BNI.</div>

		<div class="q">4.	Metode pembayaran apa saja yang diterima dalam pemesanan paket Ayo Liburan 2017?</div>
		<div class="a">Ayo Liburan 2017 hanya menerima metode pembayaran melalui kartu kredit BNI saja</div>

		<div class="q">5.	Mengapa transaksi saya tidak berhasil?</div>
		<div class="a">Jangan khawatir, hal ini biasa terjadi. Transaksi bisa saja tidak berhasil dikarenakan beberapa hal. Kegagalan transaksi dapat terjadi dikarenakan adanya masalah pada kartu kredit atau kesalahan teknis. Anda dapat memeriksa kembali kelengkapan dokumen pembayaran serta detail kartu kredit dan mengulang proses pembayaran Anda. Silahkan hubungi call center kami di........ atau mengirimkan email melalui ayoliburan@garuda-indonesia.com jika Anda membutuhkan bantuan lainnya. </div>

		<div class="q">6. Apakah terdapat jumlah minimum dan maksimum pembelian paket Ayo Liburan 2017 dalam satu reservasi menggunakan 1 (satu) Kartu Kredit?</div>
		<div class="a">Tidak ada jumlah maksimum pembelian paket Ayo Liburan 2017 dalam satu reservasi menggunakan satu kartu.</div>

		<div class="q">7.	Apakah pembelian paket wisata untuk anak-anak atau bayi akan mendapatkan harga khusus?</div>
		<div class="a">Tidak. Pembelian paket wisata untuk anak-anak akan dikenakan harga normal.</div>

		<div class="q">8.	Apakah pembelian paket wisata untuk usia lanjut (60 tahun keatas) akan mendapatkan harga khusus?</div>
		<div class="a">Tidak. Pembelian paket wisata untuk usia lanjut akan dikenakan harga normal.</div>

		<div class="q">9.	Dari mana sajakah titik keberangkatan perjalanan Ayo Liburan 2017?</div>
		<div class="a">Untuk saat ini titik keberangkatan Ayo Liburan 2017 hanya akan diadakan dari kota Jakarta saja.</div>

		<div class="q">10.	Apakah harga yang tertera pada website sudah termasuk tax dan biaya lain-lain?</div>
		<div class="a">Ya, harga yang tertera pada website ayoliburan.garuda-indonesia.com sudah termasuk harga tax, tiket pesawat, hotel, dan akomodasi selama perjalanan.</div>

		<div class="q">11.	Bagaimana saya dapat melihat status pemesanan dan pembelian saya?</div>
		<div class="a">Anda dapat melihat status pemesanan atau pembelian Anda dengan melihat pada halaman dashboard Anda.</div>

		<div class="q">12.	Apakah saya dapat mencetak bukti pembayaran saya?</div>
		<div class="a">Ya, Anda dapat mencetak bukti pembelian dan pembayaran setelah seluruh proses transaksi berhasil dilakukan. Anda hanya perlu menekan tombol cetak pada halaman dashboard Anda.</div>

		<div class="q">13.	Bagaimana jika terjadi kesalahan sistem ketika melakukan pembelian atau pembayaran?</div>
		<div class="a">Anda dapat menghubungi call center kami di.....atau mengirimkan email melalui ayoliburan@garuda-indonesia.com . </div>

		<div class="q">14.	Apa yang harus saya lakukan jika saya melakukan pemesanan namun tidak dapat melakukan pembayaran?</div>
		<div class="a">Jangan khawatir, hal ini biasa terjadi. Transaksi bisa saja tidak berhasil dikarenakan beberapa hal. Kegagalan transaksi dapat terjadi dikarenakan adanya masalah pada kartu kredit atau kesalahan teknis. Anda dapat memeriksa kembali kelengkapan dokumen pembayaran serta detail kartu kredit dan mengulang proses pembayaran Anda. Silahkan hubungi call center kami di........ atau mengirimkan email melalui ayoliburan@garuda-indonesia.com jika Anda membutuhkan bantuan lainnya. </div>

		<div class="q">15.	Bagaimana jika saya belum melakukan pembayaran akan tetapi time limit telah habis? (Bagaimana jika saya belum melakukan pembayaran namun batas waktu yang disediakan telah habis ?)</div>
		<div class="a">Anda akan diberikan waktu session hold selama 8 (delapan) menit. Jika anda telah melewati batas waktu pembayaran, maka pesanan Anda dianggap batal dan Anda dapat melakukan pemesanan ulang (Anda akan diberikan waktu yang terus berjalan selama 8 menit. Jika anda telah melewati batas waktu pembayaran, maka pesanan anda dianggap batal. Anda Dapat kembali melakukan pemesanan ulang)</div>

		<div class="q">16.	Apakah terdapat minimum kuota keberangkatan pada setiap paket Ayo Liburan 2017?</div>
		<div class="a">Ya, minimum kuota keberangkatan untuk destinasi wisata Malang, Belitung, Labuan Bajo, dan Banyuwangi adalah 10 pax. Untuk destinasi Padang, minimum kuota keberangkatan adalah 20 pax</div>
	</div>
	<div class="title_help2">Syarat & Ketentuan</div>
	<div class="text">
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam iste at quod tempora, mollitia earum ipsum voluptatibus facilis sit officia? Sed facilis consequuntur optio? Unde aliquid facilis, mollitia hic numquam.
	</div>
	<br><br><br><br>
	<div class="link_">
		<a href="#bahasa">Bahasa Indonesia</a>
		<a href="#english" class="selected">English</a>
	</div>
	<div id="english"></div>
	<div class="title_help2">Contact Us</div>
	<div class="title_help2">FAQ</div>
	<div class="title_help2">Terms & Conditions</div>
	
	
</div>
<script type="text/javascript">
var url_schedules = 'http://ayoliburan.garuda-indonesia.com/destination/schedules';
var max_tourist = 25</script><div class="clearfix"></div>
<div id="footer">
	<div class="container">
		<div class="foot_sosmed">
			<span>Find us on</span>
			<a href="#"><img src="http://ayoliburan.garuda-indonesia.com/assets/frontend/images/ico-fb.png" alt=""></a>
			<a href="#"><img src="http://ayoliburan.garuda-indonesia.com/assets/frontend/images/ico-tw.png" alt=""></a>
			<a href="#"><img src="http://ayoliburan.garuda-indonesia.com/assets/frontend/images/ico-ig.png" alt=""></a>
			<a href="#"><img src="http://ayoliburan.garuda-indonesia.com/assets/frontend/images/ico-youtube.png" alt=""></a>
		</div>
		<div class="fl">
			<a href="#" class="box_modal_full" alt="bni_card.php"><img src="http://ayoliburan.garuda-indonesia.com/assets/frontend/images/logo_footer.png" alt=""></a>
			Copyright of Garuda Indonesia @2017 - Allright reserved
		</div>
		
		<div class="clearfix"></div>
	</div>
</div>
<?php include "includes/footer.php";?>
<?php include "includes/js.php";?>
</body>
</html>