<!DOCTYPE html>
<html lang="en" class="app">
<?php include "includes/head.php"; ?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php"; ?>
    <section>
      <section class="hbox stretch">
        <?php include "includes/menu.php"; ?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="col-sm-6">
                      <h3 class="m-b-xs text-black">Cover Images</h3>
                    </div>
                    <a href="home_cover_edit.php" class="btn btn-success fr m15">Tambah</a>
                  </section>
                  
                  <div class="clearfix"></div>
                  <table class="table table-striped m-b-none">
                    <thead>
                      <tr>
                        <th width="20%">Gambar</th>
                        <th width="40%">Link</th>
                        <th width="15%">Logo Ayo Liburan</th>
                        <th width="10%">Status</th>
                        <th width="10%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <img src="images/p1.jpg" alt="" height="150">
                        </td>
                        <td><a href="https://www.garuda-indonesia.com/id/en/index.page">https://www.garuda-indonesia.com/id/en/index.page</a></td>
                        <td>Ada</td>
                        <td>Aktif</td>
                        <td class="action">
                          <a href="home_cover_edit.php" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <img src="images/p1.jpg" alt="" height="150">
                        </td>
                        <td><a href="https://www.garuda-indonesia.com/id/en/index.page">https://www.garuda-indonesia.com/id/en/index.page</a></td>
                        <td>-</td>
                        <td>Aktif</td>
                        <td class="action">
                          <a href="home_cover_edit.php" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>     
                </section>
              </section>
            </section>
          </section>
        </section>
      </section>
    </section>
  </section>
<?php include "includes/js.php"; ?>
</body>
</html>