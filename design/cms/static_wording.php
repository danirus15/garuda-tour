<!DOCTYPE html>
<html lang="en" class="app">
<?php include "includes/head.php"; ?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php"; ?>
    <section>
      <section class="hbox stretch">
        <?php include "includes/menu.php"; ?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="col-sm-6">
                      <h3 class="m-b-xs text-black">Static Wording Edit</h3>
                    </div>
                  </section>
                  <!-- s:content --> 
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">
                      
                    </header>
                    <div class="panel-body">
                      <form class="form-horizontal" method="post" action="admin.php">
                        <section class="panel panel-default">
                          <header class="panel-heading bg-light">
                            <ul class="nav nav-tabs nav-justified">
                              <li class="active"><a href="#indonesia" data-toggle="tab">Indonesia</a></li>
                              <li><a href="#english" data-toggle="tab">English</a></li>
                            </ul>
                          </header>
                          <div class="panel-body">
                            <div class="tab-content">
                              <!-- s:english -->
                              <div class="tab-pane active" id="indonesia">
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Judul Intro</label>
                                  <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                                <div class="line line-dashed b-b line-lg pull-in"></div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Deskripsi</label>
                                  <div class="col-md-8">
                                    <textarea class="input-sm  form-control" name="" id="" cols="30" rows="10"></textarea>
                                    
                                  </div>
                                </div>
                              </div>
                              <!-- s:english -->
                              <!-- s:arab -->
                              <div class="tab-pane" id="english">
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Intro Title</label>
                                  <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                                <div class="line line-dashed b-b line-lg pull-in"></div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">description</label>
                                  <div class="col-md-8">
                                    <textarea class="input-sm  form-control" name="" id="" cols="30" rows="10"></textarea>
                                    
                                  </div>
                                </div>
                              </div>
                              <!-- s:arab -->
                            </div>
                          </div>
                        </section>

                      </form>
                    </div>
                  </section>
                  <!-- e:content -->
                  <div class="clearfix"></div>
                  

                </section>
              </section>
            </section>
          </section>
        </section>
      </section>
    </section>
  </section>
<?php include "includes/js.php"; ?>
</body>
</html>