<!DOCTYPE html>
<html lang="en" class="app">
<?php include "includes/head.php"; ?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php"; ?>
    <section>
      <section class="hbox stretch">
        <?php include "includes/menu.php"; ?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="col-sm-6">
                      <h3 class="m-b-xs text-black">Tanggal Paket Liburan Add/Edit</h3>
                    </div>
                  </section>
                  <!-- s:content --> 
                  <form class="form-horizontal" method="post" action="paket.php">

                  <section class="panel panel-default">
                    
                    <header class="panel-heading font-bold">
                      Paket
                    </header>
                    <div class="panel-body">
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Kota Keberangkatan</label>
                          <div class="col-sm-10 no-padding">
                            <div class="col-sm-10">
                              <div class="checkbox i-checks col-sm-10">
                                <label>
                                  <input type="checkbox" value="">
                                  <i></i>
                                  Jakarta
                                </label>
                              </div>
                            </div>
                            <div class="clearfix" style="padding-top: 5px;"></div>
                            <div class="col-sm-4">
                              Harga Luar Paket<br>
                              <input class="input-sm form-control" size="16" type="text">
                            </div>
                            <div class="col-sm-4">
                              Jumlah Kursi Potongan<br>
                              <input class="input-sm form-control" size="5" type="text">
                            </div>
                            <div class="col-sm-4">
                              Harga Potongan<br>
                              <input class="input-sm form-control" size="16" type="text">
                            </div>
                            <div class="col-sm-4">
                              Harga Paket<br>
                              <input class="input-sm form-control" size="16" type="text">
                            </div>
                            <div class="col-sm-4">
                              Suplement Harga<br>
                              <input class="input-sm form-control" size="16" type="text">
                            </div>
                            <div class="clearfix" style="padding-top: 10px;"></div>

                            

                          </div>


                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Jumlah Hari</label>
                          <div class="col-sm-10">
                            <input class="input-sm input-s  form-control" size="16" type="text" value="">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Tanggal Berangkat</label>
                          <div class="col-sm-10">
                            <input class="input-sm input-s datepicker-input form-control" size="16" type="text" value="" data-date-format="dd-mm-yyyy">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Tanggal Pulang</label>
                          <div class="col-sm-10">
                            <input class="input-sm input-s datepicker-input form-control" size="16" type="text" value="" data-date-format="dd-mm-yyyy">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Tanggal Batas Pesan Akhir</label>
                          <div class="col-sm-10">
                            <input class="input-sm input-s datepicker-input form-control" size="16" type="text" value="" data-date-format="dd-mm-yyyy">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Jumlah Kursi</label>
                          <div class="col-sm-10">
                            <input class="input-sm input-s form-control" size="16" type="text">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Status <a class="mandatory tip" title="" data-original-title="Wajib diisi">*</a></label>
                          <div class="col-md-3">
                            <select name="account" class="form-control" required="">
                              <option>Aktif</option>
                              <option>Tidak Aktif</option>
                            </select>
                          </div>
                        </div>
                    </div> 
                    <div class="panel-body">
                     <div class="form-group">
                          <label class="col-sm-2 control-label">&nbsp;</label>
                          <div class="col-sm-4">
                            <a href="home_cover.php" class="btn btn-default">Batal</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                        </div>
                      </div>
                  </section>
                 
                  </form>
                  <!-- e:content -->
                  <div class="clearfix"></div>
                  

                </section>
              </section>
            </section>
          </section>
        </section>
      </section>
    </section>
  </section>
<?php include "includes/js.php"; ?>
</body>
</html>