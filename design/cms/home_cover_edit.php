<!DOCTYPE html>
<html lang="en" class="app">
<?php include "includes/head.php"; ?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php"; ?>
    <section>
      <section class="hbox stretch">
        <?php include "includes/menu.php"; ?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="col-sm-6">
                      <h3 class="m-b-xs text-black">Cover Image Add/Edit</h3>
                    </div>
                  </section>
                  <!-- s:content --> 
                  <section class="panel panel-default">
                    <header class="panel-heading font-bold">
                      
                    </header>
                    <div class="panel-body">
                      <form class="form-horizontal" method="post" action="home_cover.php">
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Foto</label>
                          <div class="col-sm-10">
                            <label class="foto-form">
                              <div id="imagePreview"></div>
                              <input id="uploadFile" type="file" name="image" class="img" />
                              <span>Upload Foto</span>
                            </label>
                            *Ukuran 1400x800px
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Link</label>
                          <div class="col-md-10">
                            <input type="text" class="input-sm form-control">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Logo Ayo Liburan</label>
                          <div class="col-md-10">
                            <div class="checkbox i-checks col-sm-3">
                              <label>
                                <input type="checkbox" value="">
                                <i></i>
                                Ada
                              </label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Status <a class="mandatory tip" title="" data-original-title="Wajib diisi">*</a></label>
                          <div class="col-md-3">
                            <select name="account" class="form-control" required="">
                              <option>Aktif</option>
                              <option>Tidak Aktif</option>
                            </select>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <div class="col-sm-4 col-sm-offset-2">
                            <a href="home_cover.php" class="btn btn-default">Batal</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </section>
                  <!-- e:content -->
                  <div class="clearfix"></div>
                  

                </section>
              </section>
            </section>
          </section>
        </section>
      </section>
    </section>
  </section>
<?php include "includes/js.php"; ?>
</body>
</html>