<!DOCTYPE html>
<html lang="en" class="app">
<?php include "includes/head.php"; ?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php"; ?>
    <section>
      <section class="hbox stretch">
        <?php include "includes/menu.php"; ?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="col-sm-6">
                      <h3 class="m-b-xs text-black">User Member</h3>
                    </div>
                  </section>
                  
                  <div class="clearfix"></div>
                  
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                        <th width="40%">Nama</th>
                        <th width="20%">Email</th>
                        <th width="20%">Phone</th>
                        <th width="20%">Status</th>
                        <th width="10%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Rudianto</td>
                        <td>rudianto@gamil.com</td>
                        <td>0819238392</td>
                        <td>Aktif</td>
                        <td class="action">
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete"  data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>

                </section>
              </section>
            </section>
          </section>
        </section>
      </section>
    </section>
  </section>
<?php include "includes/js.php"; ?>
</body>
</html>