<!-- .aside -->
        <aside class="bg-black aside-md hidden-print" id="nav">          
          <section class="vbox">
            <section class="w-f scrollable">
              <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
                         


                <!-- nav -->                 
                <nav class="nav-primary hidden-xs">
                  <ul class="nav">
                    <li class="active">
                      <a href="home.php">
                        <i class="i i-circle-sm text-info-dk"></i>
                        <span>Dashboard</span>
                      </a>
                    </li>
                    <li>
                      <a href="home_cover.php">
                        <i class="i i-circle-sm text-info-dk"></i>
                        <span>Cover Images</span>
                      </a>
                    </li>
                    <li>
                      <a href="static_wording.php">
                        <i class="i i-circle-sm text-info-dk"></i>
                        <span>Static Wording</span>
                      </a>
                    </li>
                    <li>
                      <a href="paket.php">
                        <i class="i i-circle-sm text-info-dk"></i>
                        <span>Paket Liburan</span>
                      </a>
                    </li>
                    <li>
                      <a href="pemesanan.php">
                        <i class="i i-circle-sm text-info-dk"></i>
                        <span>Daftar Pemesanan</span>
                      </a>
                    </li>
                    <li>
                      <a href="user.php">
                        <i class="i i-circle-sm text-info-dk"></i>
                        <span>User</span>
                      </a>
                    </li>
                    <li>
                      <a href="admin.php">
                        <i class="i i-circle-sm text-info-dk"></i>
                        <span>Admin Member</span>
                      </a>
                    </li>
                    <li>
                      <a href="index.php">
                        <i class="i i-circle-sm text-info-dk"></i>
                        <span>Logout</span>
                      </a>
                    </li>
                   
                  </ul>
                  
                </nav>
                <!-- / nav -->
              </div>
            </section>
            
          </section>
        </aside>