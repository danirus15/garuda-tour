<!DOCTYPE html>
<html lang="en" class="app">
<?php include "includes/head.php"; ?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php"; ?>
    <section>
      <section class="hbox stretch">
        <?php include "includes/menu.php"; ?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="col-sm-6">
                      <h3 class="m-b-xs text-black">Pemesanan</h3>
                    </div>
                  </section>
                  
                  <div class="clearfix"></div>
                  
                  <table class="table table-striped m-b-none">
                    <thead>
                      <tr>
                        <th>Kode Pesanan</th>
                        <th>Destinasi</th>
                        <th>Kota Berangkat</th>
                        <th>Tanggal</th>
                        <th>Orang</th>
                        <th width="150">Nama Pemesan</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>EP1234</td>
                        <td>Belitung</td>
                        <td>Jakarta</td>
                        <td>12 Juni 2017 - 15 Juni 2017</td>
                        <td>2</td>
                        <td>
                          <b>Rudianto</b><br>
                          Rudianto@gmail.com<br>
                          08193229299
                        </td>
                        <td>Rp.10.000.000,-</td>
                        <td>Paid</td>
                        <td><a href="pemesanan_detail.php" class="btn_small">Lihat Rincian</a></td>
                      </tr>
                      
                      
                    </tbody>
                  </table>

                </section>
              </section>
            </section>
          </section>
        </section>
      </section>
    </section>
  </section>
<?php include "includes/js.php"; ?>
</body>
</html>