<!DOCTYPE html>
<html lang="en" class="app">
<?php include "includes/head.php"; ?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php"; ?>
    <section>
      <section class="hbox stretch">
        <?php include "includes/menu.php"; ?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="col-sm-6">
                      <h3 class="m-b-xs text-black">Destinasi Paket Liburan Add/Edit</h3>
                    </div>
                  </section>
                  <!-- s:content --> 
                  <form class="form-horizontal" method="post" action="paket.php">

                  <section class="panel panel-default">
                    
                    <header class="panel-heading font-bold">
                      Content
                    </header>
                    <div class="panel-body">
                      <div class="panel-body">
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Status</label>
                          <div class="col-sm-10">
                            <div class="checkbox i-checks col-sm-10">
                                <label>
                                  <input type="checkbox" value="">
                                  <i></i>
                                  Sold out
                                </label>
                              </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="panel-body">
                          <div class="form-group">
                              <label class="col-sm-2 control-label">Status Tampil</label>
                              <div class="col-sm-10">
                                <div class="checkbox i-checks col-sm-10">
                                    <label>
                                      <input type="checkbox" value="">
                                      <i></i>
                                      Tidak Tampil
                                    </label>
                                  </div>
                            </div>
                          <div class="clearfix"></div>
                    </div>


                        
                        <section class="panel panel-default">
                          <header class="panel-heading bg-light">
                            <ul class="nav nav-tabs nav-justified">
                              <li class="active"><a href="#indonesia" data-toggle="tab">Indonesia</a></li>
                              <li><a href="#english" data-toggle="tab">English</a></li>
                            </ul>
                          </header>
                          <div class="panel-body">
                            <div class="tab-content">
                              <!-- s:english -->
                              <div class="tab-pane active" id="indonesia">
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Judul</label>
                                  <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Destinasi Kota</label>
                                  <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                                <div class="line line-dashed b-b line-lg pull-in"></div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Ringkasan</label>
                                  <div class="col-md-10">
                                    <textarea class="input-sm  form-control" name="" id="" cols="30" rows="10"></textarea>
                                    
                                  </div>
                                </div>
                                <div class="line line-dashed b-b line-lg pull-in"></div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Deskripsi</label>
                                  <div class="col-md-10">
                                    <textarea class="input-sm  form-control" name="" id="" cols="30" rows="10"></textarea>
                                    
                                  </div>
                                </div>
                                <div class="line line-dashed b-b line-lg pull-in"></div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Rencana Perjalanan</label>
                                  <div class="col-md-10">
                                    <textarea class="input-sm  form-control" name="" id="" cols="30" rows="10"></textarea>
                                    
                                  </div>
                                </div>
                              </div>
                              <!-- s:english -->
                              <!-- s:arab -->
                              <div class="tab-pane" id="english">
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Title</label>
                                  <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Destination</label>
                                  <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                  </div>
                                </div>
                                <div class="line line-dashed b-b line-lg pull-in"></div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Highlight</label>
                                  <div class="col-md-10">
                                    <textarea class="input-sm  form-control" name="" id="" cols="30" rows="10"></textarea>
                                    
                                  </div>
                                </div>
                                <div class="line line-dashed b-b line-lg pull-in"></div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">Decription</label>
                                  <div class="col-md-10">
                                    <textarea class="input-sm  form-control" name="" id="" cols="30" rows="10"></textarea>
                                    
                                  </div>
                                </div>
                                <div class="line line-dashed b-b line-lg pull-in"></div>
                                <div class="form-group">
                                  <label class="col-sm-2 control-label">itenerary</label>
                                  <div class="col-md-10">
                                    <textarea class="input-sm  form-control" name="" id="" cols="30" rows="10"></textarea>
                                    
                                  </div>
                                </div>
                              </div>
                              <!-- s:arab -->
                            </div>
                          </div>
                        <header class="panel-heading font-bold">
                      Image Cover
                    </header>
                    <div class="panel-body">
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Foto</label>
                          <div class="col-sm-10">
                            <label class="foto-form">
                              <div id="imagePreview"></div>
                              <input id="uploadFile" type="file" name="image" class="img" />
                              <span>Upload Foto</span>
                            </label>
                            *Ukuran 1400x800px
                          </div>
                        </div>
                    </div>
                        <header class="panel-heading font-bold">
                          Galeri
                        </header>
                        <div class="panel-body">
                            <div class="form-group group-img">
                              <div class="img-area">
                                  <div class="img-wrap">
                                      <div class="img-loc">
                                          <div class="style-input">
                                              <input type="file" name="file1" id="file1" class="inputfile" onchange="PreviewImage(1);" />
                                              <label class="file" for="file1"><span>Upload Foto</span></label>
                                          </div>
                                          <div class="clearfix"></div>
                                          <div id="appendImg1" class="appImg"></div>
                                      </div>
                                      <div class="img-cap">
                                          <h5>Caption</h5>
                                          <input type="text" name="img-text1">
                                      </div>
                                      <div class="clearfix"></div>
                                  </div>
                               <div class="clearfix"></div>
                                <div class="img-area">
                                    <div class="img-wrap">
                                        <div class="img-loc">
                                            <div class="style-input">
                                                <input type="file" name="file2" id="file2" class="inputfile" onchange="PreviewImage(2);" />
                                                <label class="file" for="file2"><span>Upload Foto</span></label>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div id="appendImg2" class="appImg"></div>
                                        </div>
                                        <div class="img-cap">
                                            <h5>Caption</h5>
                                            <input type="text" name="img-text2">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="img-area">
                                    <div class="img-wrap">
                                        <div class="img-loc">
                                            <div class="style-input">
                                                <input type="file" name="file3" id="file3" class="inputfile" onchange="PreviewImage(3);" />
                                                <label class="file" for="file3"><span>Upload Foto</span></label>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div id="appendImg3" class="appImg"></div>
                                        </div>
                                        <div class="img-cap">
                                            <h5>Caption</h5>
                                            <input type="text" name="img-text3">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="img-area">
                                    <div class="img-wrap">
                                        <div class="img-loc">
                                            <div class="style-input">
                                                <input type="file" name="file4" id="file4" class="inputfile" onchange="PreviewImage(4);" />
                                                <label class="file" for="file4"><span>Upload Foto</span></label>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div id="appendImg4" class="appImg"></div>
                                        </div>
                                        <div class="img-cap">
                                            <h5>Caption</h5>
                                            <input type="text" name="img-text4">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="img-area">
                                    <div class="img-wrap">
                                        <div class="img-loc">
                                            <div class="style-input">
                                                <input type="file" name="file5" id="file5" class="inputfile" onchange="PreviewImage(5);" />
                                                <label class="file" for="file5"><span>Upload Foto</span></label>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div id="appendImg5" class="appImg"></div>
                                        </div>
                                        <div class="img-cap">
                                            <h5>Caption</h5>
                                            <input type="text" name="img-text5">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="img-area">
                                    <div class="img-wrap">
                                        <div class="img-loc">
                                            <div class="style-input">
                                                <input type="file" name="file6" id="file6" class="inputfile" onchange="PreviewImage(6);" />
                                                <label class="file" for="file6"><span>Upload Foto</span></label>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div id="appendImg6" class="appImg"></div>
                                        </div>
                                        <div class="img-cap">
                                            <h5>Caption</h5>
                                            <input type="text" name="img-text6">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="img-area">
                                    <div class="img-wrap">
                                        <div class="img-loc">
                                            <div class="style-input">
                                                <input type="file" name="file7" id="file7" class="inputfile" onchange="PreviewImage(7);" />
                                                <label class="file" for="file7"><span>Upload Foto</span></label>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div id="appendImg7" class="appImg"></div>
                                        </div>
                                        <div class="img-cap">
                                            <h5>Caption</h5>
                                            <input type="text" name="img-text7">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="img-area">
                                    <div class="img-wrap">
                                        <div class="img-loc">
                                            <div class="style-input">
                                                <input type="file" name="file8" id="file8" class="inputfile" onchange="PreviewImage(8);" />
                                                <label class="file" for="file8"><span>Upload Foto</span></label>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div id="appendImg8" class="appImg"></div>
                                        </div>
                                        <div class="img-cap">
                                            <h5>Caption</h5>
                                            <input type="text" name="img-text8">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                
                              </div>
                        
                              <div class="clearfix"></div>
                              <!-- <input type="button" id="addImg" class="btn_add" value="Tambah Foto"> -->
                            </div>
                        </div>

                      </section>
                    </div>
                    
                    <section class="panel panel-default">
                    <div class="panel-body">
                     <div class="form-group">
                          <div class="col-sm-4">
                            <a href="home_cover.php" class="btn btn-default">Batal</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                        </div>
                      </div>
                  </section>
                 
                  </form>
                  <!-- e:content -->
                  <div class="clearfix"></div>
                  

                </section>
              </section>
            </section>
          </section>
        </section>
      </section>
    </section>
  </section>
<?php include "includes/js.php"; ?>
</body>
</html>