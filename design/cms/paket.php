<!DOCTYPE html>
<html lang="en" class="app">
<?php include "includes/head.php"; ?>
<body class="">
  <section class="vbox">
    <?php include "includes/header.php"; ?>
    <section>
      <section class="hbox stretch">
        <?php include "includes/menu.php"; ?>
        <!-- /.aside -->
        <section id="content">
          <section class="hbox stretch">
            <section>
              <section class="vbox">
                <section class="scrollable padder">              
                  <section class="row m-b-md">
                    <div class="col-sm-6">
                      <h3 class="m-b-xs text-black">Paket Liburan</h3>
                    </div>
                    <a href="paket_form.php" class="btn btn-success fr m15">Tambah</a>
                  </section>
                  
                  <div class="clearfix"></div>
                  
                  <table class="table table-striped m-b-none">
                    <thead>
                      <tr>
                        <th width="90%" colspan="6">Destinasi</th>
                        <th width="10%">Urutan</th>
                        <th width="10%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="destinasi">
                        <td colspan="5">
                          <b>Malang </b>
                        </td>
                        <td>
                          <a href="paket_tanggal.php" class="btn_small">Tambah Tanggal</a>
                        </td>
                        <td>
                          <select name="" id="">
                            <option value="">1</option>
                            <option value="">2</option>
                          </select>
                        </td>
                        <td class="action">
                          
                          <a href="paket_form.php" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete"  data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      <tr class="tanggal">
                        <td>
                          12 Juni 2017 - 15 Juni 2017<br>
                          Batas pesan:  5 Juni 2017
                        </td>
                        <td colspan="2">
                          <span class="kota">Jakarta</span> Rp. 2.500.000,-<br>
                          <span class="kota">Bali</span> Rp. 2.000.000,-<br>
                          <span class="kota">Makasar</span> Rp. 3.500.000,-<br>
                        </td>
                        <td>25 Kursi</td>
                        <td>Sisa 20 Kursi</td>
                        <td colspan="2">Status: Aktif</td>
                        <td class="action">
                          <a href="paket_tanggal.php" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete"  data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      <tr class="tanggal">
                        <td>
                          4 Juli 2017 - 7 Juli 2017<br>
                          Batas pesan:  20 Juni 2017
                        </td>
                        <td colspan="2">
                          <span class="kota">Jakarta</span> Rp. 2.500.000,-<br>
                          <span class="kota">Bali</span> Rp. 2.000.000,-<br>
                          <span class="kota">Makasar</span> Rp. 3.500.000,-<br>
                        </td>
                        <td>25 Kursi</td>
                        <td>Sisa 24 Kursi</td>
                        <td colspan="2">Status: Aktif</td>
                        <td class="action">
                          <a href="paket_tanggal.php" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete"  data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>

                      <tr class="destinasi">
                        <td colspan="5">
                          <b>Belitung </b>
                        </td>
                        <td>
                          <a href="paket_tanggal.php" class="btn_small">Tambah Tanggal</a>
                        </td>
                        <td>
                          <select name="" id="">
                            <option value="">1</option>
                            <option value="" selected>2</option>
                          </select>
                        </td>
                        <td class="action">
                          
                          <a href="paket_form.php" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete"  data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      <tr class="tanggal">
                        <td>
                          12 Juni 2017 - 15 Juni 2017<br>
                          Batas pesan:  5 Juni 2017
                        </td>
                        <td colspan="2">
                          <span class="kota">Jakarta</span> Rp. 2.500.000,-<br>
                          <span class="kota">Bali</span> Rp. 2.000.000,-<br>
                          <span class="kota">Makasar</span> Rp. 3.500.000,-<br>
                        </td>
                        <td>25 Kursi</td>
                        <td>Sisa 20 Kursi</td>
                        <td colspan="2">Status: Aktif</td>
                        <td class="action">
                          <a href="paket_tanggal.php" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete"  data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      <tr class="tanggal">
                        <td>
                          4 Juli 2017 - 7 Juli 2017<br>
                          Batas pesan:  27 Juni 2017
                        </td>
                        <td colspan="2">
                          <span class="kota">Jakarta</span> Rp. 2.500.000,-<br>
                          <span class="kota">Bali</span> Rp. 2.000.000,-<br>
                          <span class="kota">Makasar</span> Rp. 3.500.000,-<br>
                        </td>
                        <td>25 Kursi</td>
                        <td>Sisa 24 Kursi</td>
                        <td colspan="2">Status: Aktif</td>
                        <td class="action">
                          <a href="paket_tanggal.php" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-original-title="Edit"><img src="images/ico_edit.png" alt=""></a>
                          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" class="delete"  data-original-title="Delete" class="delete" ><img src="images/ico_del.png" alt=""></a>
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>

                </section>
              </section>
            </section>
          </section>
        </section>
      </section>
    </section>
  </section>
<?php include "includes/js.php"; ?>
</body>
</html>